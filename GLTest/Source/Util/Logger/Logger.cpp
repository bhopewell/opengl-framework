#include "Util/Logger/Logger.h"

#include <string>
#include <Windows.h>

//Init the file pointer to nullptr
FILE* Logger::m_pfLogFile = nullptr;

void Logger::Init()
{
	//Init the log file to a nullptr
	m_pfLogFile = nullptr;

	//Open the log file for writing
	if (fopen_s(&m_pfLogFile, "Logs.txt", "w") != 0)
	{
		//If the file failed to open, set the log file to nullptr
		m_pfLogFile = nullptr;
	}
}

void Logger::Log(const char* log, LogLevel logLevel)
{
	//If this log level is less than our static log level, just return.
	if (logLevel < m_eLogLevel)
		return;

	std::string s = std::string("LOG: ");
	s.append(log);
	s.append("\n");
	
	//Print the log to the file
	fprintf(m_pfLogFile, s.c_str());
	//Print the error to the output window
	OutputDebugStringA(s.c_str());
}

void Logger::Log(std::string log, LogLevel logLevel)
{
	//If this log level is less than our static log level, just return.
	if (logLevel < m_eLogLevel)
		return;

	std::string s = std::string("LOG: ");
	s.append(log);
	s.append("\n");

	//Print the log to the file
	fprintf(m_pfLogFile, s.c_str());
	//Print the error to the output window
	OutputDebugStringA(s.c_str());
}

void Logger::LogWarning(const char* log, LogLevel logLevel)
{
	//If this log level is less than our static log level, just return.
	if (logLevel < m_eLogLevel)
		return;

	std::string s = std::string("WARNING: ");
	s.append(log);
	s.append("\n");

	//Print the log to the file
	fprintf(m_pfLogFile, s.c_str());
	//Print the error to the output window
	OutputDebugStringA(s.c_str());
}

void Logger::LogWarning(std::string log, LogLevel logLevel)
{
	//If this log level is less than our static log level, just return.
	if (logLevel < m_eLogLevel)
		return;

	std::string s = std::string("WARNING: ");
	s.append(log);
	s.append("\n");

	//Print the log to the file
	fprintf(m_pfLogFile, s.c_str());
	//Print the error to the output window
	OutputDebugStringA(s.c_str());
}

void Logger::LogError(const char* log)
{
	std::string s = std::string("ERROR: ");
	s.append(log);
	s.append("\n");

	//Print the log to the file
	fprintf(m_pfLogFile, s.c_str());
	//Print the error to the output window
	OutputDebugStringA(s.c_str());
}

void Logger::LogError(std::string log)
{
	std::string s = std::string("ERROR: ");
	s.append(log);
	s.append("\n");

	//Print the log to the file
	fprintf(m_pfLogFile, s.c_str());
	//Print the error to the output window
	OutputDebugStringA(s.c_str());
}

//Flush and close the log file
void Logger::Close()
{
	fflush(m_pfLogFile);
	fclose(m_pfLogFile);
}