#include "GameTime\GameTime.h"

#include "Util/Logger/Logger.h"

#include <GLFW\glfw3.h>

GameTime* GameTime::m_poInstance = nullptr;

GameTime::GameTime()
{
	m_fCurrentTime = 0.0;
	m_fPrevTime = 0.0;
	m_fDeltaTime = 0.0;
	m_fTotalElapsedTime = 0.0;
}

GameTime::~GameTime()
{

}

void GameTime::Destroy()
{
	if (m_poInstance != nullptr)
	{
		delete m_poInstance;
		m_poInstance = nullptr;
	}
}

void GameTime::Initialize()
{
	Instance()->m_fCurrentTime = glfwGetTime();
	Instance()->m_fPrevTime = Instance()->m_fCurrentTime;
	Instance()->m_fDeltaTime = 0.0;
	Instance()->m_fTotalElapsedTime = 0.0;

	Logger::Log("GameTime Initialized.");
}

void GameTime::Update()
{
	Instance()->m_fCurrentTime = glfwGetTime();
	Instance()->m_fDeltaTime = (Instance()->m_fCurrentTime - Instance()->m_fPrevTime);
	//Update the previous time value
	Instance()->m_fPrevTime = Instance()->m_fCurrentTime;

	Instance()->m_fTotalElapsedTime += Instance()->m_fDeltaTime;
}