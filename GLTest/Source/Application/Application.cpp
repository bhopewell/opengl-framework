#include <GL/glew.h>

#define ILUT_USE_OPENGL
#include <IL/il.h>
#include <IL/ilu.h>
#include <IL/ilut.h>

#include <GLFW\glfw3.h>

#include "Application/Application.h"
#include "Input/InputManager.h"

#include "Graphics/Font/FontManager.h"
#include "AssetManagement/Texture/TextureManager.h"
#include "AssetManagement/Shader/ShaderManager.h"
#include "Scene/TestScene.h"

#include "GameTime\GameTime.h"

#include "Renderer\ForwardRenderer.h"
#include "Renderer\DeferredRenderer.h"

#include "Util/Logger/Logger.h"

#include "Debug.h"

Application* Application::m_poInstance = nullptr;

//Callback for GLFW to resize our frame buffer
void Resize(GLFWwindow* window, int x, int y)
{
	Application::Instance()->FrameBufferResize(x, y);
}

//Function pointer to our resize callback
GLFWframebuffersizefun(ResizeFrameBuffer) = &Resize;

Application::Application()
{
	m_bIsRunning = true;

	//Init the logger
	Logger::Init();

	//Create the renderer
//#define DEFERRED
#ifdef DEFERRED
	m_poRenderer = new DeferredRenderer();
#else
	m_poRenderer = new ForwardRenderer();
#endif DEFERRED

	//Create the input manager
	m_poInputManager = new InputManager();

	//Create the scene manager
	m_poSceneManager = new SceneManager();
}

Application::~Application()
{
	//Destroy the font manager
	FontManager::Instance()->Destroy();

	//Destroy the texture manager
	TextureManager::Instance()->Destroy();
	//Destroy the shader manager
	ShaderManager::Instance()->Destroy();

	//Destroy the scene manager
	if (m_poSceneManager != nullptr)
	{
		delete m_poSceneManager;
		m_poSceneManager = nullptr;
	}

	//Delete the input manager
	if (m_poInputManager != nullptr)
	{
		delete m_poInputManager;
		m_poInputManager = nullptr;
	}

	//Destroy the GameTime object
	GameTime::Destroy();

	//Destroy the renderer
	if (m_poRenderer != nullptr)
	{
		delete m_poRenderer;
		m_poRenderer = nullptr;
	}

	//Close the logger
	Logger::Close();
}

//Destroy the application instance
void Application::Destroy()
{
	if (m_poInstance != nullptr)
	{
		delete m_poInstance;
		m_poInstance = nullptr;
	}
}

//Initialize the application instance
bool Application::Init()
{
	//Window size, OpenGL Major Version, OpenGL Minor Version, AntiAlias level
	if (!m_poRenderer->Initialize(glm::vec2(1200, 900), 4, 2, 4))
		return false;

	//Set up the method we want to call when the framebuffer resizes
	glfwSetFramebufferSizeCallback(GetWindow(), ResizeFrameBuffer);

	//Initialize the TextureManager
	TextureManager::Instance()->Initialize();

	//Initialise the InputManager
	m_poInputManager->Init();

	//Initialize the SceneManager
	m_poSceneManager->Init();

	//Push the menu scene to the scene manager
	m_poSceneManager->PushScene(new TestScene());

	//Initialize the FontManager
	FontManager::Instance()->InitFontManager("Resources/Textures/font.png");

	return true;
}

//Handle the resizing of the widow/framebuffer
void Application::FrameBufferResize(int newX, int newY)
{
	//Handle the frame buffer resizing
	m_poRenderer->FrameBufferResize(newX, newY);

	//Set the new projection
	GetActiveCamera()->SetProjection(60.0f, newX / (float)newY, 0.1f, 100.0f);
}

//Run the application
void Application::Run()
{
	GameTime::Initialize();

	do
	{
		//Update the gametime
		GameTime::Update();

		float dt = (float)GameTime::DeltaTime();

		//Update the input manager
		m_poInputManager->Update();

		//Update the scene manager
		m_poSceneManager->Update(dt);

		//Render the screen
		Render();

		//Poll for glfw events
		glfwPollEvents();

	} while (m_bIsRunning && glfwWindowShouldClose(m_poRenderer->GetWindow()) == 0);
}

//Render the application
void Application::Render()
{
#ifdef DEFERRED
	//Start our rendering
	m_poRenderer->StartRender(); //Start Geometry Pass

	//Render the scenes
	m_poSceneManager->Render();

	//Start rendering our GUI step
	m_poRenderer->StartRenderGUI();

	//End our rendering, swapping the back buffers
	m_poRenderer->EndRender(); //End Geometry Pass. Start Lighting Pass

#else
	//Start our rendering
	m_poRenderer->StartRender();

	//Render the scenes
	m_poSceneManager->Render();

	//Start rendering our GUI step
	m_poRenderer->StartRenderGUI();

	//Render our GUI
	m_poSceneManager->RenderGUI();

	//End our rendering, swapping the back buffers
	m_poRenderer->EndRender();
#endif DEFERRED
}

void Application::Quit()
{
	//((DeferredRenderer*)m_poRenderer)->SaveBuffers();

	m_bIsRunning = false;

	Logger::Log("Application Quit");
}