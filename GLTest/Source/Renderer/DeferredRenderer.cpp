#include "Renderer/DeferredRenderer.h"
#include "Renderer/DeferredQuad.h"

#include "Graphics/Font/FontManager.h"

#include "AssetManagement/Shader/ShaderManager.h"
#include "Application/Application.h"

#include "Object\DrawableObject\DrawableObject3D.h"

#include "Graphics/ShaderTechnique/TechniqueFactory/ETechnique.h"

#include "Util/Logger/Logger.h"

#include <glm/glm.hpp>

#include <GLFW\glfw3.h>
#include <stdio.h>

#include <vector>

#include "Debug.h"

#include <IL/il.h>
#include <IL/ilu.h>
#include <IL/ilut.h>

DeferredRenderer::DeferredRenderer()
{
	m_poScreenQuad = nullptr;
	m_poWindow = nullptr;
}

DeferredRenderer::~DeferredRenderer()
{
	if (m_poScreenQuad != nullptr)
	{
		delete m_poScreenQuad;
		m_poScreenQuad = nullptr;
	}

	if (m_poWindow != nullptr)
	{
		glfwDestroyWindow(m_poWindow);
		m_poWindow = nullptr;
	}
}

bool DeferredRenderer::Initialize(glm::vec2& screenSize, int openGLVersionMajor, int openGLVersionMinor, int antiAliasLevel)
{
	Application::Instance()->SetIsDeferred(true);

	//Initialize the vars we'll be using for OpenGL
	m_v2ScreenSize = screenSize;
	m_iOpenGLVersionMajor = openGLVersionMajor;
	m_iOpenGLVersionMinor = openGLVersionMinor;
	m_iAntiAliasLevel = antiAliasLevel;

	//Initialize Open GL
	if (!InitializeOpenGL())
		return false;

	//Initialize the framebuffer
	if (!InitializeFrameBuffer())
		return false;

	//Create the screen quad
	m_poScreenQuad = new DeferredQuad(m_v2ScreenSize);
	m_poScreenQuad->SetTextures(m_uiColorBuffer, m_uiDepthBuffer, m_uiNormalBuffer, m_uiSpecBuffer, m_uiUVBuffer);

	Logger::Log("Deferred Renderer Initialized.");

	return true;
}

bool DeferredRenderer::InitializeOpenGL()
{
	//Initialise GLFW
	if (!glfwInit())
	{
		Logger::LogError("Failed to initialize GLFW.");
		return false;
	}

	glfwWindowHint(GLFW_SAMPLES, m_iAntiAliasLevel); //4x anti-aliasing
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, m_iOpenGLVersionMajor); //We want to use OpenGL 4.5
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, m_iOpenGLVersionMinor);
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE); //Used to make MacOS happy. Otherwise not needed.
#ifdef _SHADERDEBUG 
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_COMPAT_PROFILE);
#else
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE); //We don't want the old OpenGL
#endif

	//Open a window and create its OpenGL context
	m_poWindow = glfwCreateWindow((int)m_v2ScreenSize.x, (int)m_v2ScreenSize.y, "GL Test", NULL, NULL);
	if (m_poWindow == NULL)
	{
		Logger::LogError("Failed to open GLFW window. If you have an Intel GPU, they aren't 3.3 compatible.");
		glfwTerminate();
		return false;
	}

	//Make the new window the current context
	glfwMakeContextCurrent(m_poWindow);
	glewExperimental = true; //Needed in core profile
	if (glewInit() != GLEW_OK)
	{
		Logger::LogError("Failed to initialize GLEW.");
		return false;
	}

	//Set the clear color
	glClearColor(0.25f, 0.25f, 0.25f, 1.0f);

	//Accept frag if it's closer to the camera than the former one
	glDepthFunc(GL_LEQUAL);
	glDepthMask(GL_TRUE);
	// Enable blending
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	glDepthRange(0, 1);

	//Ensure we can capture the key events
	glfwSetInputMode(m_poWindow, GLFW_STICKY_KEYS, GL_TRUE);
	glfwSetInputMode(m_poWindow, GLFW_CURSOR, GLFW_CURSOR_HIDDEN);

	return true;
}

//Initialize the frame buffer
bool DeferredRenderer::InitializeFrameBuffer()
{
	//Create the FrameBuffer object
	glGenFramebuffers(1, &m_uiFrameBuffer);
	glBindFramebuffer(GL_FRAMEBUFFER, m_uiFrameBuffer);

	//Create the G-buffer textures
	CreateGBufferTextures();

	GLenum status = glCheckFramebufferStatus(GL_FRAMEBUFFER);
	if (status != GL_FRAMEBUFFER_COMPLETE)
	{
		return false;
	}

	//Restore the default frame buffer and unbind the textures
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	glBindTexture(GL_TEXTURE_2D, 0);

	return true;
}

//Create the G-buffer textures
void DeferredRenderer::CreateGBufferTextures()
{
	//Create the color buffer
	CreateBuffer(m_uiColorBuffer);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, m_uiColorBuffer, 0);
	//Create the position buffer
	CreateBuffer(m_uiPositionBuffer);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT1, GL_TEXTURE_2D, m_uiPositionBuffer, 0);
	//Create the normal buffer
	CreateBuffer(m_uiNormalBuffer);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT2, GL_TEXTURE_2D, m_uiNormalBuffer, 0);
	//Create the UV buffer
	CreateBuffer(m_uiSpecBuffer);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT3, GL_TEXTURE_2D, m_uiSpecBuffer, 0);
	//Create the UV buffer
	CreateBuffer(m_uiUVBuffer);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT4, GL_TEXTURE_2D, m_uiUVBuffer, 0);

	//Create the depth buffer
	CreateBuffer(m_uiDepthBuffer, true);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, m_uiDepthBuffer, 0);
}

void DeferredRenderer::CreateBuffer(GLuint& textureID, bool isDepthTex)
{
	glm::vec2 screenSize = Application::Instance()->GetScreenSize();

	//Create the render texture
	glGenTextures(1, &textureID);
	//Bind the newly created texture that we will be rendering with
	glBindTexture(GL_TEXTURE_2D, textureID);

	//Create the appropriate glTexImage2D
	if (isDepthTex)
	{
		//Give an empty image to OpenGL. Note: We pass 0 through as the last param hence the empty tex
		glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT32, (int)m_v2ScreenSize.x, (int)m_v2ScreenSize.y, 0, GL_DEPTH_COMPONENT, GL_FLOAT, 0);
	}
	else
	{
		//Give an empty image to OpenGL. Note: We pass 0 through as the last param hence the empty tex
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, (int)m_v2ScreenSize.x, (int)m_v2ScreenSize.y, 0, GL_RGB, GL_FLOAT, 0);
	}

	//Poor filtering which is apparently required
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
}

void DeferredRenderer::FrameBufferResize(int newX, int newY)
{
	glViewport(0, 0, newX, newY);
	m_v2ScreenSize = glm::vec2(newX, newY);
}

void DeferredRenderer::StartRender()
{
	//Bind the frame buffer before we start writing to it
	glBindFramebuffer(GL_FRAMEBUFFER, m_uiFrameBuffer);

	//Clear the framebuffer
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Create the draw buffers
	GLenum DrawBuffers[5] = { GL_COLOR_ATTACHMENT0, GL_COLOR_ATTACHMENT1, GL_COLOR_ATTACHMENT2, GL_COLOR_ATTACHMENT3, GL_COLOR_ATTACHMENT4 };
	glDrawBuffers(5, DrawBuffers); //5 being the size of DrawBuffers
}

void DeferredRenderer::StartRenderGUI()
{
#pragma warning("Why does this not work?")
	//Do the lighting pass after everything has been drawn
	glBindFramebuffer(GL_FRAMEBUFFER, 0);

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Disable depth testing
	glDisable(GL_DEPTH_TEST);
	//Enable backface culling
	glEnable(GL_CULL_FACE);

	//Render the screen quad
	m_poScreenQuad->Render();

	glBindTexture(GL_TEXTURE_2D, 0);
}

void DeferredRenderer::EndRender()
{
	//Do the lighting pass after everything has been drawn
	glBindFramebuffer(GL_FRAMEBUFFER, 0);

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Disable depth testing
	glDisable(GL_DEPTH_TEST);
	//Enable backface culling
	glEnable(GL_CULL_FACE);

	//Render the screen quad
	m_poScreenQuad->Render();

	glBindTexture(GL_TEXTURE_2D, 0);

	//Push the framebuffer to the screen
	glfwSwapBuffers(m_poWindow);
}