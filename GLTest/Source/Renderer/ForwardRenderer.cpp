#include "Renderer\ForwardRenderer.h"

#include "Application/Application.h"

#include <stdio.h>
#include <GL/glew.h>
#include <GLFW\glfw3.h>

#include "Util/Logger/Logger.h"

#include "Debug.h"

ForwardRenderer::ForwardRenderer()
{
}

ForwardRenderer::~ForwardRenderer()
{
	if (m_poWindow != nullptr)
	{
		glfwDestroyWindow(m_poWindow);
		m_poWindow = nullptr;
	}
}

//Initialize our renderer
bool ForwardRenderer::Initialize(glm::vec2& screenSize, int openGLVersionMajor, int openGLVersionMinor, int antiAliasLevel)
{
	Application::Instance()->SetIsDeferred(false);

	//Initialize the vars we'll be using for OpenGL
	m_v2ScreenSize = screenSize;
	m_iOpenGLVersionMajor = openGLVersionMajor;
	m_iOpenGLVersionMinor = openGLVersionMinor;
	m_iAntiAliasLevel = antiAliasLevel;

	//Initialise GLFW
	if (!glfwInit())
	{
		fprintf(stderr, "Failed to initialize GLFW\n");
		return false;
	}
	
	glfwWindowHint(GLFW_SAMPLES, m_iAntiAliasLevel); //4x anti-aliasing
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, m_iOpenGLVersionMajor); //We want to use OpenGL 3.3
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, m_iOpenGLVersionMinor);
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE); //Used to make MacOS happy. Otherwise not needed.
#ifdef _SHADERDEBUG 
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_COMPAT_PROFILE);
#else
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE); //We don't want the old OpenGL
#endif

	//Open a window and create its OpenGL context
	m_poWindow = glfwCreateWindow((int)m_v2ScreenSize.x, (int)m_v2ScreenSize.y, "GL Test", NULL, NULL);
	if (m_poWindow == NULL)
	{
		fprintf(stderr, "Failed to open GLFW window. If yopu have an Intel GPU, they aren't 3.3 compatible.\n");
		glfwTerminate();
		return false;
	}
	
	//Make the new window the current context
	glfwMakeContextCurrent(m_poWindow);
	glewExperimental = true; //Needed in core profile
	if (glewInit() != GLEW_OK)
	{
		fprintf(stderr, "Failed to initialize GLEW.\n");
		return false;
	}

	//Set the clear color
	glClearColor(0.25f, 0.25f, 0.25f, 1.0f);

	//Accept frag if it's closer to the camera than the former one
	//glDepthFunc(GL_LEQUAL);
	glDepthFunc(GL_LESS);
	glDepthMask(GL_TRUE);
	//Enable alpha in opengl
	glEnable(GL_ALPHA);
	// Enable blending
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	//Ensure we can capture the key events
	glfwSetInputMode(m_poWindow, GLFW_STICKY_KEYS, GL_TRUE);
	glfwSetInputMode(m_poWindow, GLFW_CURSOR, GLFW_CURSOR_HIDDEN);

	Logger::Log("Forward Renderer Initialized.");

	return true;
}

//Handle a frame buffer resize
void ForwardRenderer::FrameBufferResize(int newX, int newY)
{
	glViewport(0, 0, newX, newY);
	m_v2ScreenSize = glm::vec2(newX, newY);
}

//Start the rendering
void ForwardRenderer::StartRender()
{
	//Clear the window
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
}

void ForwardRenderer::StartRenderGUI()
{
	//Do nothing since there's no special steps we need to take to render our GUI
}

//End the rendering
void ForwardRenderer::EndRender()
{
	//Swap the back buffer
	glfwSwapBuffers(m_poWindow);
}