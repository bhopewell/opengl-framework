#include "Renderer/DeferredQuad.h"

#include "Graphics/ShaderTechnique/ITechnique.h"
#include "Graphics/ShaderTechnique/TechniqueFactory/TechniqueFactory.h"
#include "Graphics/ShaderTechnique/TechniqueFactory/ETechnique.h"

#include "Application/Application.h"
#include "Object/Camera/ICamera.h"

#include <vector>

#include "Util/Logger/Logger.h"

#include "Debug.h"

DeferredQuad::DeferredQuad(glm::vec2& screenSize)
{
	//Initialize the Vert and UV buffers
	InitBuffers(screenSize);

	//Initialize the shaders
	InitShaders();
}

DeferredQuad::~DeferredQuad()
{
	if (m_poShaderTechnique != nullptr)
	{
		delete m_poShaderTechnique;
		m_poShaderTechnique = nullptr;
	}
}

void DeferredQuad::InitBuffers(glm::vec2& screenSize)
{
	//Create the verts
	CreateVerts(screenSize);

	//Create the UVs
	CreateUVs();
}

void DeferredQuad::CreateVerts(glm::vec2& screenSize)
{
	//Generate Vert Buffer
	glGenBuffers(1, &m_uiVertexBufferID);

	std::vector<glm::vec2> verts;

	//Generate the verts
	glm::vec2 topLeftVert = glm::vec2(-1, 1);
	glm::vec2 topRightVert = glm::vec2(1, 1);
	glm::vec2 bottomRightVert = glm::vec2(1, -1);
	glm::vec2 bottomLeftVert = glm::vec2(-1, -1);

	//Add the verts to the vert buffer
	verts.push_back(topLeftVert);
	verts.push_back(bottomLeftVert);
	verts.push_back(topRightVert);

	verts.push_back(bottomRightVert);
	verts.push_back(topRightVert);
	verts.push_back(bottomLeftVert);

	//Store the number of verts we have
	m_uiVertCount = verts.size();

	glBindBuffer(GL_ARRAY_BUFFER, m_uiVertexBufferID);
	glBufferData(GL_ARRAY_BUFFER, m_uiVertCount * sizeof(glm::vec2), &verts[0], GL_STATIC_DRAW);
}

void DeferredQuad::CreateUVs()
{
	//Generate the UV buffer
	glGenBuffers(1, &m_uiUVBufferID);

	std::vector<glm::vec2> uvs;

	//Generate the UVS
	glm::vec2 topLeftUV = glm::vec2(0, 0);
	glm::vec2 topRightUV = glm::vec2(1, 0);
	glm::vec2 bottomRightUV = glm::vec2(1, 1);
	glm::vec2 bottomLeftUV = glm::vec2(0, 1);

	//Add the uvs to the uv buffer
	uvs.push_back(topLeftUV);
	uvs.push_back(bottomLeftUV);
	uvs.push_back(topRightUV);

	uvs.push_back(bottomRightUV);
	uvs.push_back(topRightUV);
	uvs.push_back(bottomLeftUV);

	glBindBuffer(GL_ARRAY_BUFFER, m_uiUVBufferID);
	glBufferData(GL_ARRAY_BUFFER, uvs.size() * sizeof(glm::vec2), &uvs[0], GL_STATIC_DRAW);
}

void DeferredQuad::InitShaders()
{
	//Create the technique and initialize it
	m_poShaderTechnique = TechniqueFactory::CreateTechnique(ETechnique::EDeferred);	
	if (!m_poShaderTechnique->Initialize())
	{
		//Log the error
		Logger::LogError("Failed to initialize shader technique!");
		return;
	}

	// Initialize uniforms' IDs
	m_iDiffuseUniform = m_poShaderTechnique->GetUniformLocation("diffuseTexture");
	m_iDepthUniform = m_poShaderTechnique->GetUniformLocation("depthTexture");
	m_iNormalUniform = m_poShaderTechnique->GetUniformLocation("normalTexture");
	m_iSpecUniform = m_poShaderTechnique->GetUniformLocation("specTexture");
	m_iTangentUniform = m_poShaderTechnique->GetUniformLocation("tangentTexture");

	m_uiVertAttribID = m_poShaderTechnique->GetAttribLocation("vertPosIn");
	m_uiUVAttribID = m_poShaderTechnique->GetAttribLocation("vertUVIn");
}

void DeferredQuad::SetTextures(GLint diffuse, GLint depth, GLint normal, GLint spec, GLint tangent)
{
	m_iDiffuseBuffer = diffuse;
	m_iDepthBuffer = depth;
	m_iNormalBuffer = normal;
	m_iSpecBuffer = spec;
	m_iTangentBuffer = tangent;
}

void DeferredQuad::BindTextures()
{
	//Bind the diffuse buffer
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, m_iDiffuseBuffer);
	glUniform1i(m_iDiffuseUniform, 0);

	//Bind the position buffer
	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, m_iDepthBuffer);
	glUniform1i(m_iDepthUniform, 1);

	//Bind the normal buffer
	glActiveTexture(GL_TEXTURE2);
	glBindTexture(GL_TEXTURE_2D, m_iNormalBuffer);
	glUniform1i(m_iNormalUniform, 2);

	//Bind the spec buffer
	glActiveTexture(GL_TEXTURE3);
	glBindTexture(GL_TEXTURE_2D, m_iSpecBuffer);
	glUniform1i(m_iSpecUniform, 3);

	//Bind the tangent buffer
	glActiveTexture(GL_TEXTURE4);
	glBindTexture(GL_TEXTURE_2D, m_iTangentBuffer);
	glUniform1i(m_iTangentUniform, 4);
}

void DeferredQuad::UnbindTextures()
{
	//Unbind the diffuse texture
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, 0);

	//Unbind the position texture
	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, 0);

	//Unbind the normal texture
	glActiveTexture(GL_TEXTURE2);
	glBindTexture(GL_TEXTURE_2D, 0);

	//Unbind the spec texture
	glActiveTexture(GL_TEXTURE3);
	glBindTexture(GL_TEXTURE_2D, 0);

	//Unbind the spec texture
	glActiveTexture(GL_TEXTURE4);
	glBindTexture(GL_TEXTURE_2D, 0);
}

void DeferredQuad::Render()
{
	//Use the shader technique
	m_poShaderTechnique->Activate();

	ICamera* cam = Application::Instance()->GetActiveCamera();

	//Pass our camPos to the shader
	glUniform3fv(m_poShaderTechnique->GetUniformLocation("CameraPosition"), 1, &cam->GetPosition()[0]);

	//Pass our near plane to the shader
	glUniform1f(m_poShaderTechnique->GetUniformLocation("zNearPlane"), cam->GetNearPlane());
	//Pass our far plane to the shader
	glUniform1f(m_poShaderTechnique->GetUniformLocation("zFarPlane"), cam->GetFarPlane());

	//Pass through the screenSize
	glUniform2f(m_poShaderTechnique->GetUniformLocation("screenSize"), Application::Instance()->GetScreenSize().x, Application::Instance()->GetScreenSize().y);

	//Send our view and projection matrices to the shader
	glUniformMatrix4fv(m_poShaderTechnique->GetUniformLocation("V"), 1, GL_FALSE, &(Application::Instance()->GetActiveCamera()->GetViewMatrix()[0][0]));
	glUniformMatrix4fv(m_poShaderTechnique->GetUniformLocation("P"), 1, GL_FALSE, &(Application::Instance()->GetActiveCamera()->GetProjectionMatrix()[0][0]));

	//Render the lights here because that's a thing we need to do!
	Application::Instance()->GetSceneManager()->ActiveScene()->GetLightManager()->RenderLights(m_poShaderTechnique);

	//Bind the textures for the final image
	BindTextures();

	// 1st attribute buffer : vertices
	glEnableVertexAttribArray(m_uiVertAttribID);
	glBindBuffer(GL_ARRAY_BUFFER, m_uiVertexBufferID);
	glVertexAttribPointer(m_uiVertAttribID, 2, GL_FLOAT, GL_FALSE, 0, (void*)0);

	// 2nd attribute buffer : UVs
	glEnableVertexAttribArray(m_uiUVAttribID);
	glBindBuffer(GL_ARRAY_BUFFER, m_uiUVBufferID);
	glVertexAttribPointer(m_uiUVAttribID, 2, GL_FLOAT, GL_FALSE, 0, (void*)0);

	// Draw call
	glDrawArrays(GL_TRIANGLES, 0, m_uiVertCount);

	glDisableVertexAttribArray(m_uiVertAttribID);
	glDisableVertexAttribArray(m_uiUVAttribID);

	//Unbind the textures
	UnbindTextures();
	//Stop using the shader technique
	m_poShaderTechnique->Deactivate();
}