#include "AssetManagement/Light/LightManager.h"
#include "Object/Light/ILight.h"
#include "Object/Light/AmbientLight.h"

#include "Graphics/ShaderTechnique/ITechnique.h"

#include <string>

#include <gl/glew.h>

LightManager::LightManager()
{
	//Create the default ambient light
	m_poAmbientLight = new AmbientLight(glm::vec3(0.25f, 0.25f, 0.25f));
}

LightManager::~LightManager()
{
	//Destroy each of the lights
	for (unsigned int i = 0; i < m_lLights.size(); ++i)
	{
		if (m_lLights[i] != nullptr)
		{
			delete m_lLights[i];
			m_lLights[i] = nullptr;
		}
	}
	//Clear the light list
	m_lLights.clear();

	//Destroy the ambient light
	if (m_poAmbientLight != nullptr)
	{
		delete m_poAmbientLight;
		m_poAmbientLight = nullptr;
	}
}

//Add the new light to the light list
void LightManager::AddLight(ILight* newLight)
{
	m_lLights.push_back(newLight);
}

//Update all the lights
void LightManager::Update(float deltaTime)
{
	//Update the ambient light
	m_poAmbientLight->Update(deltaTime);

	//Update all the lights in the light vector
	for (unsigned int i = 0; i < m_lLights.size(); ++i)
	{
		m_lLights[i]->Update(deltaTime);
	}
}

//Render all the lights
void LightManager::RenderLights(ITechnique* shaderTechnique)
{
	//Pass the ambient light through to the shader
	GLint ID = shaderTechnique->GetUniformLocation("ambientLight.AmbientColor");
	glUniform3fv(ID, 1, &m_poAmbientLight->GetColor()[0]);

	//Render each of the lights
	for (unsigned int i = 0; i < m_lLights.size(); ++i)
	{
		//Ignore any light that is off... Just don't do it.
		if (!m_lLights[i]->IsOn())
			continue;

		std::string positionString = "lights[" + std::to_string(i) + "].Position";
		std::string colorString = "lights[" + std::to_string(i) + "].Color";
		std::string exString = "lights[" + std::to_string(i) + "].Exponent";
		std::string cutoffString = "lights[" + std::to_string(i) + "].Cutoff";
		std::string rangeString = "lights[" + std::to_string(i) + "].Range";
		std::string constAttenStr = "lights[" + std::to_string(i) + "].ConstantAttenuation";
		std::string linAttenString = "lights[" + std::to_string(i) + "].LinearAttenuation";
		std::string quadAttenString = "lights[" + std::to_string(i) + "].QuadraticAttenuation";
		std::string dirString = "lights[" + std::to_string(i) + "].Direction";

		//Get the transformed position via GetPosition and pass it through to the shader.
		glm::vec4 pos = m_lLights[i]->GetPosition();
		ID = shaderTechnique->GetUniformLocation(positionString.c_str());
		glUniform4fv(ID, 1, &(pos[0]));
		
		//Store the direction in a temp variable because we return a normalized version of it from GetDirection which doesn't play nice with referencing.
		glm::vec3 dir = m_lLights[i]->GetDirection();
		ID = shaderTechnique->GetUniformLocation(dirString.c_str());
		glUniform3fv(ID, 1, &(dir[0]));

		//Pass the color through to the shader
		ID = shaderTechnique->GetUniformLocation(colorString.c_str());
		glUniform3fv(ID, 1, &(m_lLights[i]->GetColor()[0]));
		
		ID = shaderTechnique->GetUniformLocation(exString.c_str());
		glUniform1f(ID, m_lLights[i]->GetExponent());
		
		ID = shaderTechnique->GetUniformLocation(cutoffString.c_str());
		glUniform1f(ID, m_lLights[i]->GetCutoff());

		ID = shaderTechnique->GetUniformLocation(rangeString.c_str());
		glUniform1f(ID, m_lLights[i]->GetRange());

		Attenuation att = m_lLights[i]->GetAttenuation();
		ID = shaderTechnique->GetUniformLocation(constAttenStr.c_str());
		glUniform1f(ID, att.m_fConstant);

		ID = shaderTechnique->GetUniformLocation(linAttenString.c_str());
		glUniform1f(ID, att.m_fLinear);

		ID = shaderTechnique->GetUniformLocation(quadAttenString.c_str());
		glUniform1f(ID, att.m_fQuadratic);
	}
}