#include "AssetManagement/Shader/ShaderManager.h"

#include <fstream>
#include <vector>

#include "Debug.h"
#include "Util/Logger/Logger.h"

ShaderManager* ShaderManager::m_poInstance = nullptr;

ShaderManager::ShaderManager()
{

}

ShaderManager::~ShaderManager()
{
	for (auto it = m_oShaderMap.begin(); it != m_oShaderMap.end(); ++it)
	{
		glDeleteShader(it._Ptr->_Myval.second);
	}

	m_oShaderMap.clear();
}

void ShaderManager::Destroy()
{
	if (m_poInstance != nullptr)
	{
		delete m_poInstance;
		m_poInstance = nullptr;
	}
}

GLuint ShaderManager::LoadShader(const char* filepath, GLenum shaderType)
{
	//If the shader has already been loaded, return the loaded shader
	if (m_oShaderMap.find(filepath) != m_oShaderMap.end())
	{
		return m_oShaderMap[filepath];
	}

	GLuint shader = 0;
	std::string shaderString;
	std::ifstream sourceFile(filepath);

	if (!sourceFile)
	{
		std::string s = std::string("Unable to open file: " + std::string(filepath));
		Logger::LogError(s.c_str());
		return 0;
	}

	//Read the shader source. Read from the start of the file until a null character
	shaderString.assign((std::istreambuf_iterator<char>(sourceFile)), std::istreambuf_iterator<char>());

	//Create the shaderID
	shader = glCreateShader(shaderType);

	//Set the shader source
	const GLchar* shaderSource = shaderString.c_str();
	glShaderSource(shader, 1, (const GLchar**)&shaderSource, NULL);

	//Compile the shader
	glCompileShader(shader);

	//Check the shader for errors
	GLint shaderCompiled = GL_FALSE;
	glGetShaderiv(shader, GL_COMPILE_STATUS, &shaderCompiled);
	if (shaderCompiled != GL_TRUE)
	{
		std::string s = std::string("Shader failed to compile! " + std::string(filepath));
		Logger::LogError(s.c_str());
		PrintShaderLog(shader, filepath);
		glDeleteShader(shader);
		shader = 0;
	}

	//Place the new shader into the shader map
	m_oShaderMap.emplace(filepath, shader);

	return shader;
}

void ShaderManager::PrintShaderLog(GLuint shader, const char* filepath)
{
	//Ensure name is shader
	if (glIsShader(shader))
	{
		//Program log length
		int infoLogLen = 0;
		int maxLen = infoLogLen;

		//Get info str len
		glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &maxLen);

		//Allocate string
		char* infoLog = new char[maxLen];

		//Get info log
		glGetShaderInfoLog(shader, maxLen, &infoLogLen, infoLog);
		if (infoLogLen > 0)
		{
			//Print the error
			std::string s = std::string("Shader - " + std::string(filepath) + "; Error: " + std::string(&infoLog[0]));
			Logger::LogError(s.c_str());
		}

		delete[] infoLog;
	}
	else
	{
		//Print the error
		std::string s = std::string("Shader ID " + std::to_string(shader) + " is not a valid shader ID");
		Logger::LogError(s.c_str());
	}
}