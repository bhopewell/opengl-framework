#include "AssetManagement/Scene/SceneManager.h"
#include "Input/InputManager.h"
#include "Application/Application.h"
#include "Scene/IScene.h"

#include "Util/Logger/Logger.h"

#include "Debug.h"

SceneManager::SceneManager()
{
}

SceneManager::~SceneManager()
{
	//Unhook from the input events
	__unhook(&InputManager::OnKeyPressed, Application::Instance()->GetInputManager(), &SceneManager::OnKeyPressed);
	__unhook(&InputManager::OnKeyReleased, Application::Instance()->GetInputManager(), &SceneManager::OnKeyReleased);

	//While the stack is not empty, loop through and delete all the scenes
	while (!m_stSceneStack.empty())
	{
		delete m_stSceneStack.top();
		m_stSceneStack.pop();
	}
}

void SceneManager::Init()
{
	//Hook into the input events
	__hook(&InputManager::OnKeyPressed, Application::Instance()->GetInputManager(), &SceneManager::OnKeyPressed);
	__hook(&InputManager::OnKeyReleased, Application::Instance()->GetInputManager(), &SceneManager::OnKeyReleased);

	Logger::Log("Scene Manager Initialized.");
}

void SceneManager::Update(float deltaTime)
{
	//If the scene stack is empty, don't update anything
	if (m_stSceneStack.empty())
		return;

	m_stSceneStack.top()->Update(deltaTime);
}

void SceneManager::Render()
{
	//If the scene stack is empty, don't render anything
	if (m_stSceneStack.empty())
		return;

	m_stSceneStack.top()->Render();
}

void SceneManager::RenderGUI()
{
	if (m_stSceneStack.empty())
		return;

	m_stSceneStack.top()->RenderGUI();
}

void SceneManager::PushScene(IScene* newScene)
{
	//Push a new scene onto the scene stack
	m_stSceneStack.push(newScene);

	Logger::Log(std::string("Push Scene: " + newScene->GetName()));
}

void SceneManager::PopScene()
{
	//Get the scene we are popping
	IScene* oldTop = m_stSceneStack.top();

	//Pop the top scene from the scene stack
	m_stSceneStack.pop();
	
	if (!m_stSceneStack.empty())
	{
		IScene* newTop = m_stSceneStack.top();
		Logger::Log(std::string("Pop Scene: " + oldTop->GetName() + ". New Top: " + newTop->GetName()));
	}
	else
	{
		Logger::Log(std::string("Pop Scene: " + oldTop->GetName() + ". Scene Stack empty."));

		//Quit the game as there are no scenes to update
		Application::Instance()->Quit();
	}

	delete oldTop;
}

void SceneManager::ReplaceCurrentScene(IScene* newScene)
{
	//Pop the top scene from the stack and push the new scene to the stack
	m_stSceneStack.pop();

	m_stSceneStack.push(newScene);
}

void SceneManager::OnKeyPressed(int key)
{
	//If the scene stack is empty, don't go calling the event
	if (m_stSceneStack.empty())
	{
		return;
	}

	//Allow the current scene to handle the key press event
	m_stSceneStack.top()->OnKeyPressed(key);
}

void SceneManager::OnKeyReleased(int key)
{
	//If the scene stack is empty, don't go calling the event
	if (m_stSceneStack.empty())
	{
		return;
	}

	//Allow the current scene to handle the key press event
	m_stSceneStack.top()->OnKeyReleased(key);
}