#include "AssetManagement/Texture/TextureManager.h"

#define ILUT_USE_OPENGL
#include <IL/il.h>
#include <IL/ilu.h>
#include <IL/ilut.h>

#include <SOIL.h>
#include <vector>

#include "Util/Logger/Logger.h"

#include "Debug.h"

TextureManager* TextureManager::m_poInstance = nullptr;

TextureManager::TextureManager()
{
	m_uiDefaultTexID = LoadTexture("Resources/Textures/default.dds");
}

TextureManager::~TextureManager()
{
	for (auto it = m_oTextureMap.begin(); it != m_oTextureMap.end(); ++it)
	{
		glDeleteTextures(1, &(it._Ptr->_Myval.second));
	}

	m_oTextureMap.clear();
}

void TextureManager::Destroy()
{
	if (m_poInstance != nullptr)
	{
		delete m_poInstance;
		m_poInstance = nullptr;
	}
}

void TextureManager::Initialize()
{
	//Initialise DevIL
	ilInit();
	iluInit();
	ilutInit();
	ilutRenderer(ILUT_OPENGL);

	Logger::Log("Texture Manager Initialized.");
}

GLint TextureManager::LoadTexture(const char* filepath)
{
	//If this texture already exists, return the ID linked with the texture
	if (m_oTextureMap.find(filepath) != m_oTextureMap.end())
	{
		return m_oTextureMap[filepath];
	}

	ILuint imageID;

	ilInit();

	ilGenImages(1, &imageID);
	ilBindImage(imageID);

	ILboolean success = ilLoadImage((ILstring)filepath);
	if (!success)
	{
		ilBindImage(0);
		ilDeleteImages(1, &imageID);
		return -1;
	}

	//Convert the image to RGBA
	ilConvertImage(IL_RGBA, IL_UNSIGNED_BYTE);

	void* data = ilGetData();
	if (!data)
	{
		ilBindImage(0);
		ilDeleteImages(1, &imageID);
		return -1;
	}

	const int width = ilGetInteger(IL_IMAGE_WIDTH);
	const int height = ilGetInteger(IL_IMAGE_HEIGHT);
	const int type = ilGetInteger(IL_IMAGE_TYPE); //Matches OpenGL
	const int format = ilGetInteger(IL_IMAGE_FORMAT); //Matches OpenGL

	//Create an OpenGL texture
	GLuint texID;
	glGenTextures(1, &texID);
	//"Bind" the newly created texture
	glBindTexture(GL_TEXTURE_2D, texID);

	glPixelStorei(GL_UNPACK_SWAP_BYTES, GL_FALSE);
	glPixelStorei(GL_UNPACK_ROW_LENGTH, 0); //Rows are tightly packed
	glPixelStorei(GL_UNPACK_SKIP_PIXELS, 0);
	glPixelStorei(GL_UNPACK_SKIP_ROWS, 0);
	glPixelStorei(GL_UNPACK_ALIGNMENT, 1); //Pixels are tightly packed

	//Give the image to OpenGL
	glTexImage2D(GL_TEXTURE_2D, 0, format, width, height, 0, format, type, data);

	//Nice trilinear filtering
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
	//When magnifying the image, use linear filtering
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	//When 'minifying' the image, use a linear blend of 2 mipmaps. Each filtered linearly
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	//Generate the mipmaps
	glGenerateMipmap(GL_TEXTURE_2D);

	//Put the texture ID into the map
	m_oTextureMap.emplace(filepath, texID);

	return texID;
}

GLint TextureManager::LoadTextureCubeMap(const char* forwardTexture, const char* backTexture, const char* leftTexture,
	const char* rightTexture, const char* bottomTexture, const char* topTexture)
{
	//If this texture already exists, return the ID linked with the texture
	if (m_oTextureMap.find(forwardTexture) != m_oTextureMap.end())
	{
		return m_oTextureMap[forwardTexture];
	}

	const char* cubeMapFilePaths[] = {
		forwardTexture, backTexture,
		leftTexture, rightTexture,
		bottomTexture, topTexture
	};

	ILuint imageID;
	ilInit();

	ILubyte* textures[6];

	for (int i = 0; i < 6; ++i)
	{
		ilGenImages(1, &imageID);
		ilBindImage(imageID);

		ILboolean success = ilLoadImage((ILstring)cubeMapFilePaths[i]);
		if (!success)
		{
			ilBindImage(0);
			ilDeleteImages(1, &imageID);
			fprintf(stderr, "Failed to load cubemap image!");
			return 0;
		}

		//Convert the image to RGBA
		ilConvertImage(IL_RGBA, IL_UNSIGNED_BYTE);

		textures[i] = ilGetData();
		if (!textures[i])
		{
			ilBindImage(0);
			ilDeleteImages(1, &imageID);
			fprintf(stderr, "Failed to load cubemap image data!");
			return 0;
		}
	}

	const int width = ilGetInteger(IL_IMAGE_WIDTH);
	const int height = ilGetInteger(IL_IMAGE_HEIGHT);
	const int type = ilGetInteger(IL_IMAGE_TYPE); //Matches OpenGL
	const int format = ilGetInteger(IL_IMAGE_FORMAT); //Matches OpenGL

	glPixelStorei(GL_UNPACK_SWAP_BYTES, GL_FALSE);
	glPixelStorei(GL_UNPACK_ROW_LENGTH, 0); //Rows are tightly packed
	glPixelStorei(GL_UNPACK_SKIP_PIXELS, 0);
	glPixelStorei(GL_UNPACK_SKIP_ROWS, 0);
	glPixelStorei(GL_UNPACK_ALIGNMENT, 1); //Pixels are tightly packed

	//Create an OpenGL texture
	GLuint texID;
	glGenTextures(1, &texID);
	//"Bind" the newly created texture
	glBindTexture(GL_TEXTURE_CUBE_MAP, texID);

	//Give the images to OpenGL
	glTexImage2D(GL_TEXTURE_CUBE_MAP_NEGATIVE_Z, 0, format, width, height, 0, format, type, textures[0]);	//Front
	glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_Z, 0, format, width, height, 0, format, type, textures[1]);	//Back
	glTexImage2D(GL_TEXTURE_CUBE_MAP_NEGATIVE_X, 0, format, width, height, 0, format, type, textures[2]);	//Left
	glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X, 0, format, width, height, 0, format, type, textures[3]);	//Right
	glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_Y, 0, format, width, height, 0, format, type, textures[4]);	//Bottom
	glTexImage2D(GL_TEXTURE_CUBE_MAP_NEGATIVE_Y, 0, format, width, height, 0, format, type, textures[5]);	//Top

	//Nice trilinear filtering
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	//When magnifying the image, use linear filtering
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	//When 'minifying' the image, use a linear blend of 2 mipmaps. Each filtered linearly
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	//Generate the mipmaps
	glGenerateMipmap(GL_TEXTURE_CUBE_MAP);

	glBindTexture(GL_TEXTURE_CUBE_MAP, 0);
	glBindTexture(GL_TEXTURE_2D, 0);

	//Put the cubemap texture ID into the map
	m_oTextureMap.emplace(forwardTexture, texID);

	return texID;
}

/*bool TextureManager::SaveTextureSOIL(GLint textureID, const char* filename)
{
	int width;
	int height;

	glBindTexture(GL_TEXTURE_2D, textureID);

	glGetTexLevelParameteriv(GL_TEXTURE_2D, 0, GL_TEXTURE_WIDTH, &width);
	glGetTexLevelParameteriv(GL_TEXTURE_2D, 0, GL_TEXTURE_HEIGHT, &height);

	std::vector<unsigned char> buf(width * height * 3);

	//glm::vec3* buffer = new glm::vec3[width * height];

	auto error = glGetError();
	glReadPixels(0, 0, width, height, GL_RGB, GL_UNSIGNED_BYTE, &buf[0]);
	error = glGetError();

	int success = SOIL_save_image(filename, SOIL_SAVE_TYPE_BMP, width, height, 3, &buf[0]);

	return success != 0;
}*/