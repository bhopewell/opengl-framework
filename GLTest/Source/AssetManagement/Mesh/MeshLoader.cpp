#include "Graphics\Mesh\Mesh.h"
#include "AssetManagement\Mesh\MeshLoader.h"

#include "assimp\Importer.hpp"
#include "assimp\scene.h"
#include "assimp\postprocess.h"

#include "Debug.h"

//Load a mesh using the Assimp library
bool MeshLoader::LoadMeshAssimp(const char* filepath, std::vector<GLuint>& indices, std::vector<glm::vec3>& vertices,
	std::vector<glm::vec2>& uvs, std::vector<glm::vec3>& normals)
{
	Assimp::Importer importer;

	const aiScene* scene = importer.ReadFile(filepath, aiProcess_JoinIdenticalVertices | aiProcess_SortByPType);
	if (!scene)
	{
		fprintf(stderr, importer.GetErrorString());
		getchar();
		return false;
	}

	const aiMesh* mesh = scene->mMeshes[0]; // Only use the first mesh... For now...

	// Fill the verts vector
	vertices.reserve(mesh->mNumVertices);
	for (unsigned int i = 0; i < mesh->mNumVertices; ++i)
	{
		aiVector3D vert = mesh->mVertices[i];
		vertices.push_back(glm::vec3(vert.x, vert.y, vert.z));
	}

	//Fill in the UVs if they exist
	if ((*mesh->mTextureCoords) != NULL)
	{
		uvs.reserve(mesh->mNumVertices);
		for (unsigned int i = 0; i < mesh->mNumVertices; ++i)
		{
			aiVector3D uv = mesh->mTextureCoords[0][i]; // Assume only 1 set of UV coords. Assimp supports 8 UV sets
			uvs.push_back(glm::vec2(uv.x, uv.y));
		}
	}

	//Fill in the normals if they exist
	if (mesh->mNormals != NULL)
	{
		normals.reserve(mesh->mNumVertices);
		for (unsigned int i = 0; i < mesh->mNumVertices; ++i)
		{
			aiVector3D normal = mesh->mNormals[i];
			normals.push_back(glm::vec3(normal.x, normal.y, normal.z));
		}
	}

	//Fill face indices
	indices.reserve(3 * mesh->mNumFaces);
	for (unsigned int i = 0; i < mesh->mNumFaces; ++i)
	{
		indices.push_back(mesh->mFaces[i].mIndices[0]);
		indices.push_back(mesh->mFaces[i].mIndices[1]);
		indices.push_back(mesh->mFaces[i].mIndices[2]);
	}

	return true;
}

bool MeshLoader::LoadMeshAssimp(const char* filepath, Mesh** poMesh)
{
	Assimp::Importer importer;

	const aiScene* scene = importer.ReadFile(filepath, aiProcess_JoinIdenticalVertices | aiProcess_SortByPType);
	if (!scene)
	{
		fprintf(stderr, importer.GetErrorString());
		getchar();
		return false;
	}

	const aiMesh* mesh = scene->mMeshes[0]; // Only use the first mesh... For now...

	// Fill the verts vector
	for (unsigned int i = 0; i < mesh->mNumVertices; ++i)
	{
		aiVector3D vert = mesh->mVertices[i];
		(*poMesh)->AddVert(glm::vec3(vert.x, vert.y, vert.z));
	}

	//Fill in the UVs
	for (unsigned int i = 0; i < mesh->mNumVertices; ++i)
	{
		aiVector3D uv = mesh->mTextureCoords[0][i]; // Assume only 1 set of UV coords. Assimp supports 8 UV sets
		(*poMesh)->AddUV(glm::vec2(uv.x, uv.y));
	}

	//Fill in the normals
	for (unsigned int i = 0; i < mesh->mNumVertices; ++i)
	{
		aiVector3D normal = mesh->mNormals[i];
		(*poMesh)->AddNormal(glm::vec3(normal.x, normal.y, normal.z));
	}

	//Fill face indices
	for (unsigned int i = 0; i < mesh->mNumFaces; ++i)
	{
		(*poMesh)->AddIndices(glm::vec3(mesh->mFaces[i].mIndices[0], 
										mesh->mFaces[i].mIndices[1], 
										mesh->mFaces[i].mIndices[2]));
	}

	return true;
}