#include <Windows.h>

#include "Application/Application.h"

#include <GLFW\glfw3.h>

#include "Debug.h"

int CALLBACK WinMain(_In_  HINSTANCE hInstance, _In_  HINSTANCE hPrevInstance,
	_In_  LPSTR lpCmdLine, _In_  int nCmdShow)
{
	if (Application::Instance()->Init() == false)
	{
		return -1;
	}

	//Run the application
	Application::Instance()->Run();

	//Destroy the application
	Application::Instance()->Destroy();

	//Terminate the glfw library before we close the program
	glfwTerminate();

#ifdef _DEBUG
	_CrtDumpMemoryLeaks();
#endif _DEBUG

	return 0;
}