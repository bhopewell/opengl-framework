#include "Object/DrawableObject/DrawableObject3D.h"
#include "Graphics/Mesh/Mesh.h"
#include "AssetManagement/Texture/TextureManager.h"

#include "Application/Application.h"
#include "Object/Camera/ICamera.h"
#include "Object/Light/PointLight.h"

#include "Graphics/ShaderTechnique/ITechnique.h"
#include "Graphics/ShaderTechnique/TechniqueFactory/TechniqueFactory.h"

#include <glm/gtc/quaternion.hpp>
#include <glm/gtx/quaternion.hpp>

#include "Debug.h"

DrawableObject3D::DrawableObject3D()
	: Object()
{
	//Create the mesh object
	m_poMesh = new Mesh("Resources/Models/suzanne.obj");

	//Set the diffuse texture to be our default texture so we know when we have forgotten to load the correct texture
	//Also, set the normal and spec textures to be -1 so we don't get any weird results
	m_iDiffuseTextureID = TextureManager::Instance()->DefaultTexture();
	m_iNormalTextureID = -1;
	m_iSpecTextureID = -1;
}

DrawableObject3D::DrawableObject3D(const char* meshFilePath)
	: Object()
{
	//Create the mesh object
	m_poMesh = new Mesh(meshFilePath);

	//Set the diffuse texture to be our default texture so we know when we have forgotten to load the correct texture
	//Also, set the normal and spec textures to be -1 so we don't get any weird results
	m_iDiffuseTextureID = TextureManager::Instance()->DefaultTexture();
	m_iNormalTextureID = -1;
	m_iSpecTextureID = -1;
}

DrawableObject3D::~DrawableObject3D()
{
	//Delete the mesh
	if (m_poMesh != nullptr)
	{
		delete m_poMesh;
		m_poMesh = nullptr;
	}

	if (m_poShaderTechnique != nullptr)
	{
		delete m_poShaderTechnique;
		m_poShaderTechnique = nullptr;
	}
}

void DrawableObject3D::SetTextures(GLint diffuseTexID, GLint bumpTexID, GLint specTexID)
{
	//Load the textures
	m_iDiffuseTextureID = diffuseTexID;

	//If there is a bump map ID, set the bump map
	if (bumpTexID != -1)
		m_iNormalTextureID = bumpTexID;

	//If there is a spec map ID, set the spec map
	if (specTexID != -1)
		m_iSpecTextureID = specTexID;
}


void DrawableObject3D::SetTextures(const char* diffuseTexturePath, const char* bumpMapPath, const char* specMapPath)
{
	//Load the textures
	m_iDiffuseTextureID = TextureManager::Instance()->LoadTexture(diffuseTexturePath);
	
	//If there is a bump map path, load the bump map
	if (bumpMapPath != nullptr)
		m_iNormalTextureID = TextureManager::Instance()->LoadTexture(bumpMapPath);

	//If there is a spec map path, load the spec map
	if (specMapPath != nullptr)
		m_iSpecTextureID = TextureManager::Instance()->LoadTexture(specMapPath);
}

bool DrawableObject3D::AttachShader(ETechnique shaderTechnique)
{
	//Create the default lighting technique for this object
	m_poShaderTechnique = TechniqueFactory::CreateTechnique(shaderTechnique);
	if (!m_poShaderTechnique->Initialize())
		return false;

	//Set the texture IDs according to the shader
	m_iDiffuseTextureAttribID = m_poShaderTechnique->GetUniformLocation("diffuseTexture");
	m_iNormalTextureAttribID = m_poShaderTechnique->GetUniformLocation("normalTexture");
	m_iSpecTextureAttribID = m_poShaderTechnique->GetUniformLocation("specTexture");

	//Attach the shader to the mesh object
	m_poMesh->AttachShader(m_poShaderTechnique);

	return true;
}

void DrawableObject3D::BindTextures()
{
	//Bind the diffuse texture
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, m_iDiffuseTextureID);
	glUniform1i(m_iDiffuseTextureAttribID, 0);

	//Bind the normal map
	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, m_iNormalTextureID);
	glUniform1i(m_iNormalTextureAttribID, 1);

	//Bind the spec map
	glActiveTexture(GL_TEXTURE2);
	glBindTexture(GL_TEXTURE_2D, m_iSpecTextureID);
	glUniform1i(m_iSpecTextureAttribID, 2);
}

void DrawableObject3D::UnbindTextures()
{
	//Unbind the diffuse texture
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, 0);

	//Unbind the diffuse texture
	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, 0);

	//Unbind the diffuse texture
	glActiveTexture(GL_TEXTURE2);
	glBindTexture(GL_TEXTURE_2D, 0);
}

void DrawableObject3D::Update(float deltaTime)
{
	if (m_bRotate)
	{
		m_v3Rotation.y += glm::sin(deltaTime) * (glm::quarter_pi<float>() * 0.25f);
	}

	Object::Update(deltaTime);
}

void DrawableObject3D::Render()
{
	//Render the mesh
	//m_poMesh->Render();
	//return;

#ifdef _SHADERDEBUG
	glMatrixMode(GL_PROJECTION);
	glLoadMatrixf((const GLfloat*)&Application::Instance()->GetActiveCamera()->GetProjectionMatrix()[0]);
	glMatrixMode(GL_MODELVIEW);
	glm::mat4 modelView = Application::Instance()->GetActiveCamera()->GetViewMatrix() * m_m4Transform;
	glLoadMatrixf((const GLfloat*)&modelView[0]);

	glUseProgram(0);

	m_poMesh->Render();

#else
	//Start using the shader technique
	m_poShaderTechnique->Activate();

	//Get the currently active camera
	ICamera* cam = Application::Instance()->GetActiveCamera();

	//Send our view matrix to the shader
	glUniformMatrix4fv(m_poShaderTechnique->GetUniformLocation("V"), 1, GL_FALSE, &(cam->GetViewMatrix()[0][0]));
	//Send our projection matrix to the shader
	glUniformMatrix4fv(m_poShaderTechnique->GetUniformLocation("P"), 1, GL_FALSE, &(cam->GetProjectionMatrix()[0][0]));
	//Send our model matrix to the shader
	glUniformMatrix4fv(m_poShaderTechnique->GetUniformLocation("M"), 1, GL_FALSE, &(m_m4Transform[0][0]));

	//Send our camera position to the shader
	glUniform3fv(m_poShaderTechnique->GetUniformLocation("CameraPosition"), 1, &(cam->GetPosition()[0]));

	//Send flags to our shader to say our spec and bump maps are valid textures
	glUniform1i(m_poShaderTechnique->GetUniformLocation("NormalTexIsValid"), m_iNormalTextureID != -1);
	glUniform1i(m_poShaderTechnique->GetUniformLocation("SpecTexIsValid"), m_iSpecTextureID != -1);

	//Pass through the screenSize
	glUniform2f(m_poShaderTechnique->GetUniformLocation("screenSize"), Application::Instance()->GetScreenSize().x, Application::Instance()->GetScreenSize().y);
	
	//Render the lights here because that's a thing we need to do!
	Application::Instance()->GetSceneManager()->ActiveScene()->GetLightManager()->RenderLights(m_poShaderTechnique);

	//Bind the textures used for rendering
	BindTextures();

	//Render the mesh
	m_poMesh->Render();

	//Unbind the textures and stop using the current shader program
	UnbindTextures();
	
	//Stop using the shader technique
	m_poShaderTechnique->Deactivate();
#endif
}