#include "Object/DrawableObject/DrawableObject2D.h"

#include "Graphics/Sprite/Quad.h"
#include "AssetManagement/Texture/TextureManager.h"
#include "AssetManagement/Shader/ShaderManager.h"

#include "Application/Application.h"
#include "Object/Camera/ICamera.h"

#include "Graphics/ShaderTechnique/ITechnique.h"
#include "Graphics/ShaderTechnique/TechniqueFactory/TechniqueFactory.h"

#include <glm/gtx/quaternion.hpp>

#include "Debug.h"

DrawableObject2D::DrawableObject2D()
	: Object()
{
	//Create the quad object
	m_poQuad = new Quad();

	//Set the diffuse texture to be our default texture so we know when we have forgotten to load the correct texture
	//Also, set the normal and spec textures to be -1 so we don't get any weird results
	m_iDiffuseTextureID = TextureManager::Instance()->DefaultTexture();
	m_iNormalTextureID = -1;
	m_iSpecTextureID = -1;
}

DrawableObject2D::DrawableObject2D(const glm::vec3& position)
	: Object(position)
{
	//Create the quad object
	m_poQuad = new Quad();

	//Set the diffuse texture to be our default texture so we know when we have forgotten to load the correct texture
	//Also, set the normal and spec textures to be -1 so we don't get any weird results
	m_iDiffuseTextureID = TextureManager::Instance()->DefaultTexture();
	m_iNormalTextureID = -1;
	m_iSpecTextureID = -1;
}

DrawableObject2D::DrawableObject2D(const glm::vec3& position, const glm::vec3& scale)
	: Object(position, scale)
{
	//Create the quad object
	m_poQuad = new Quad();

	//Set the diffuse texture to be our default texture so we know when we have forgotten to load the correct texture
	//Also, set the normal and spec textures to be -1 so we don't get any weird results
	m_iDiffuseTextureID = TextureManager::Instance()->DefaultTexture();
	m_iNormalTextureID = -1;
	m_iSpecTextureID = -1;
}

DrawableObject2D::DrawableObject2D(const glm::vec3& position, const glm::vec3& scale, const glm::vec3& rotationRadians)
	: Object(position, scale, rotationRadians)
{
	//Create the quad object
	m_poQuad = new Quad();

	//Set the diffuse texture to be our default texture so we know when we have forgotten to load the correct texture
	//Also, set the normal and spec textures to be -1 so we don't get any weird results
	m_iDiffuseTextureID = TextureManager::Instance()->DefaultTexture();
	m_iNormalTextureID = -1;
	m_iSpecTextureID = -1;
}

DrawableObject2D::~DrawableObject2D()
{
	if (m_poQuad != nullptr)
	{
		delete m_poQuad;
		m_poQuad = nullptr;
	}

	//Destroy the shader technique
	if (m_poShaderTechnique != nullptr)
	{
		delete m_poShaderTechnique;
		m_poShaderTechnique = nullptr;
	}
}

void DrawableObject2D::SetTextures(GLint diffuseTexID, GLint bumpTexID, GLint specTexID)
{
	//Load the textures
	m_iDiffuseTextureID = diffuseTexID;

	//If there is a bump map ID, set the bump map
	if (bumpTexID != -1)
		m_iNormalTextureID = bumpTexID;

	//If there is a spec map ID, set the spec map
	if (specTexID != -1)
		m_iSpecTextureID = specTexID;
}

void DrawableObject2D::SetTextures(const char* diffuseTexturePath, const char* bumpMapPath, const char* specMapPath)
{
	//Load the textures
	m_iDiffuseTextureID = TextureManager::Instance()->LoadTexture(diffuseTexturePath);

	//If there is a bump map path, load the bump map
	if (bumpMapPath != nullptr)
		m_iNormalTextureID = TextureManager::Instance()->LoadTexture(bumpMapPath);

	//If there is a spec map path, load the spec map
	if (specMapPath != nullptr)
		m_iSpecTextureID = TextureManager::Instance()->LoadTexture(specMapPath);
}

bool DrawableObject2D::AttachShader(ETechnique shaderTechnique)
{
	//Create the default lighting technique for this object
	m_poShaderTechnique = TechniqueFactory::CreateTechnique(shaderTechnique);
	if (!m_poShaderTechnique->Initialize())
		return false;

	//Set the texture IDs according to the shader
	m_iDiffuseTextureAttribID = m_poShaderTechnique->GetUniformLocation("diffuseTexture");
	m_iNormalTextureAttribID = m_poShaderTechnique->GetUniformLocation("normalTexture");
	m_iSpecTextureAttribID = m_poShaderTechnique->GetUniformLocation("specTexture");

	//Attach the shader to the mesh object
	m_poQuad->AttachShader(m_poShaderTechnique);

	return true;
}

void DrawableObject2D::BindTextures()
{
	//Bind the diffuse texture
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, m_iDiffuseTextureID);
	glUniform1i(m_iDiffuseTextureAttribID, 0);

	//Bind the normal map
	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, m_iNormalTextureID);
	glUniform1i(m_iNormalTextureAttribID, 1);

	//Bind the spec map
	glActiveTexture(GL_TEXTURE2);
	glBindTexture(GL_TEXTURE_2D, m_iSpecTextureID);
	glUniform1i(m_iSpecTextureAttribID, 2);
}

void DrawableObject2D::UnbindTextures()
{
	glBindTexture(GL_TEXTURE_2D, 0);
}

void DrawableObject2D::Update(float deltaTime)
{
	if (m_bOrbit)
	{
		//directionToRot = CurrentPos - PointToRotateAround
		glm::vec3 dir = m_v3LocalPosition - glm::vec3(0);
		//Calculate the angle to rotate
		float rot = glm::sin(deltaTime) * glm::pi<float>();
		//Create a rotation matrix with the given angle
		glm::mat4 yRotMat = glm::toMat4(glm::quat(glm::vec3(0, rot, 0)));
		//Multiply directionToRot by the rotation matrix
		glm::vec4 outDir = yRotMat * glm::vec4(dir, 0.0f);
		//Set the position
		m_v3LocalPosition = glm::vec3(outDir.x, outDir.y, outDir.z);
	}

	//Update the base object
	Object::Update(deltaTime);
}

void DrawableObject2D::Render()
{
	glDisable(GL_CULL_FACE);
	m_poShaderTechnique->Activate();

	//Get the currently active camera
	ICamera* cam = Application::Instance()->GetActiveCamera();

	//Send our view matrix to the shader
	glUniformMatrix4fv(m_poShaderTechnique->GetUniformLocation("V"), 1, GL_FALSE, &(cam->GetViewMatrix()[0][0]));
	//Send our projection matrix to the shader
	glUniformMatrix4fv(m_poShaderTechnique->GetUniformLocation("P"), 1, GL_FALSE, &(cam->GetProjectionMatrix()[0][0]));
	//Send our model matrix to the shader
	glUniformMatrix4fv(m_poShaderTechnique->GetUniformLocation("M"), 1, GL_FALSE, &m_m4Transform[0][0]);

	//Send our camera position to the shader
	glUniform3fv(m_poShaderTechnique->GetUniformLocation("CameraPosition"), 1, &cam->GetPosition()[0]);

	//Send flags to our shader to say our spec and bump maps are valid textures
	glUniform1i(m_poShaderTechnique->GetUniformLocation("NormalTexIsValid"), m_iNormalTextureID != -1);
	glUniform1i(m_poShaderTechnique->GetUniformLocation("SpecTexIsValid"), m_iSpecTextureID != -1);

	//Render the lights here because that's a thing we need to do!
	Application::Instance()->GetSceneManager()->ActiveScene()->GetLightManager()->RenderLights(m_poShaderTechnique);

	//Bind the textures used for rendering
	BindTextures();

	//Render the mesh
	m_poQuad->Render();

	//Unbind the textures and stop using the current shader program
	UnbindTextures();
	m_poShaderTechnique->Deactivate();

	glEnable(GL_CULL_FACE);
}