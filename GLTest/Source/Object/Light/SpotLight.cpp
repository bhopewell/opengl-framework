#include "Object/Light/SpotLight.h"

#include <glm/matrix.hpp>
#include <glm/gtc/quaternion.hpp>
#include <glm/gtx/quaternion.hpp>

SpotLight::SpotLight()
{
	m_fExponent = 10.0f; //Default Exponent of 10
	m_fCutoff = 45.0f; //Default cutoff of 45*
	m_fRange = 20.0f; //Default range of 20
	m_v3Color = glm::vec3(1.0f, 1.0f, 1.0f); //Default color of white
	m_v3LocalPosition = glm::vec3(0.0f, 0.0f, 0.0f); //Default position of world center
	m_v3Direction = glm::vec3(0.0f, 0.0f, 1.0f); //Default direction along the forward Z axis
	
	//Default attenuation
	m_oAttenuation = Attenuation(0.0f, 1.0f, 0.0f);

	m_bIsOn = true; //Default the light to being on
}

SpotLight::SpotLight(float exponent)
{
	m_fExponent = exponent;
	m_fCutoff = 45.0f; //Default cutoff of 45*
	m_fRange = 20.0f; //Default range of 20
	m_v3Color = glm::vec3(1.0f, 1.0f, 1.0f); //Default color of white
	m_v3LocalPosition = glm::vec3(0.0f, 0.0f, 0.0f); //Default position of world center
	m_v3Direction = glm::vec3(0.0f, 0.0f, 1.0f); //Default direction along the forward Z axis

	//Default attenuation
	m_oAttenuation = Attenuation(0.0f, 1.0f, 0.0f);

	m_bIsOn = true; //Default the light to being on
}

SpotLight::SpotLight(float exponent, float cutoffDegrees)
{
	m_fExponent = exponent;
	m_fCutoff = cutoffDegrees;
	m_fRange = 20.0f; //Default range of 20
	m_v3Color = glm::vec3(1.0f, 1.0f, 1.0f); //Default color of white
	m_v3LocalPosition = glm::vec3(0.0f, 0.0f, 0.0f); //Default position of world center
	m_v3Direction = glm::vec3(0.0f, 0.0f, 1.0f); //Default direction along the forward Z axis

	//Default attenuation
	m_oAttenuation = Attenuation(0.0f, 1.0f, 0.0f);

	m_bIsOn = true; //Default the light to being on
}

SpotLight::SpotLight(float exponent, float cutoffDegrees, float range)
{
	m_fExponent = exponent;
	m_fCutoff = cutoffDegrees;
	m_fRange = range;
	m_v3Color = glm::vec3(1.0f, 1.0f, 1.0f); //Default color of white
	m_v3LocalPosition = glm::vec3(0.0f, 0.0f, 0.0f); //Default position of world center
	m_v3Direction = glm::vec3(0.0f, 0.0f, 1.0f); //Default direction along the forward Z axis

	//Default attenuation
	m_oAttenuation = Attenuation(0.0f, 1.0f, 0.0f);

	m_bIsOn = true; //Default the light to being on
}

SpotLight::SpotLight(float exponent, float cutoffDegrees, float range, glm::vec3& color)
{
	m_fExponent = exponent;
	m_fCutoff = cutoffDegrees;
	m_fRange = range;
	m_v3Color = color;
	m_v3LocalPosition = glm::vec3(0.0f, 0.0f, 0.0f); //Default position of world center
	m_v3Direction = glm::vec3(0.0f, 0.0f, 1.0f); //Default direction along the forward Z axis

	//Default attenuation
	m_oAttenuation = Attenuation(0.0f, 1.0f, 0.0f);

	m_bIsOn = true; //Default the light to being on
}

SpotLight::SpotLight(float exponent, float cutoffDegrees, float range, glm::vec3& color, glm::vec3& position)
{
	m_fExponent = exponent;
	m_fCutoff = cutoffDegrees;
	m_fRange = range;
	m_v3Color = color;
	m_v3LocalPosition = position;
	m_v3Direction = glm::vec3(0.0f, 0.0f, 1.0f); //Default direction along the forward Z axis

	//Default attenuation
	m_oAttenuation = Attenuation(0.0f, 1.0f, 0.0f);

	m_bIsOn = true; //Default the light to being on
}

SpotLight::SpotLight(float exponent, float cutoffDegrees, float range, glm::vec3& color, glm::vec3& position, glm::vec3& direction)
{
	m_fExponent = exponent;
	m_fCutoff = cutoffDegrees;
	m_fRange = range;
	m_v3Color = color;
	m_v3LocalPosition = position;
	m_v3Direction = direction;

	//Default attenuation
	m_oAttenuation = Attenuation(0.0f, 1.0f, 0.0f);

	m_bIsOn = true; //Default the light to being on
}

SpotLight::SpotLight(float exponent, float cutoffDegrees, float range, glm::vec3& color, glm::vec3& position, glm::vec3& direction, Attenuation& attenuation, bool isOn)
{
	m_fExponent = exponent;
	m_fCutoff = cutoffDegrees;
	m_fRange = range;
	m_v3Color = color;
	m_v3LocalPosition = position;
	m_v3Direction = direction;

	m_oAttenuation = attenuation;

	m_bIsOn = isOn;
}

SpotLight::~SpotLight()
{
}

void SpotLight::Update(float deltaTime)
{
	//Update the base object
	Object::Update(deltaTime);

	//Set the position of the light according to the updated transform
	m_v4Position = m_m4Transform[3];
}