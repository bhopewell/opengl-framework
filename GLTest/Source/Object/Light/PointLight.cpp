#include "Object/Light/PointLight.h"

PointLight::PointLight()
{
	m_fExponent = 10.0f; //Default Exponent of 10
	m_fCutoff = 180.0f; //Point Lights always have a cutoff of 180*
	m_v3Color = glm::vec3(1.0f, 1.0f, 1.0f); //Default color of white
	m_v3LocalPosition = glm::vec3(0.0f, 0.0f, 0.0f); //Default position of world center
	m_v3Direction = glm::vec3(0.0f, 0.0f, 0.0f); //Point light. Direction doesn't matter!

	//Default attenuation
	m_oAttenuation.m_fConstant = 0.0f;
	m_oAttenuation.m_fLinear = 1.0f;
	m_oAttenuation.m_fQuadratic = 0.0f;

	m_bIsOn = true; //Default the light to being on
}

PointLight::PointLight(float exponent)
{
	m_fExponent = exponent;
	m_fCutoff = 180.0f; //Point Lights always have a cutoff of 180*
	m_fRange = 20.0f;
	m_v3Color = glm::vec3(1.0f, 1.0f, 1.0f); //Default color of white
	m_v3LocalPosition = glm::vec3(0.0f, 0.0f, 0.0f); //Default position of world center
	m_v3Direction = glm::vec3(0.0f, 0.0f, 0.0f); //Point light. Direction doesn't matter!

	//Default attenuation
	m_oAttenuation.m_fConstant = 0.0f;
	m_oAttenuation.m_fLinear = 1.0f;
	m_oAttenuation.m_fQuadratic = 0.0f;

	m_bIsOn = true; //Default the light to being on
}

PointLight::PointLight(float exponent, float range)
{
	m_fExponent = exponent;
	m_fCutoff = 180.0f; //Point Lights always have a cutoff of 180*
	m_fRange = range;
	m_v3Color = glm::vec3(1.0f, 1.0f, 1.0f); //Default color of white
	m_v3LocalPosition = glm::vec3(0.0f, 0.0f, 0.0f); //Default position of world center
	m_v3Direction = glm::vec3(0.0f, 0.0f, 0.0f); //Point light. Direction doesn't matter!

	//Default attenuation
	m_oAttenuation.m_fConstant = 0.0f;
	m_oAttenuation.m_fLinear = 1.0f;
	m_oAttenuation.m_fQuadratic = 0.0f;

	m_bIsOn = true; //Default the light to being on
}

PointLight::PointLight(float exponent, float range, glm::vec3& color)
{
	m_fExponent = exponent;
	m_fCutoff = 180.0f; //Point Lights always have a cutoff of 180*
	m_fRange = range;
	m_v3Color = color;
	m_v3LocalPosition = glm::vec3(0.0f, 0.0f, 0.0f); //Default position of world center
	m_v3Direction = glm::vec3(0.0f, 0.0f, 0.0f); //Point light. Direction doesn't matter!

	//Default attenuation
	m_oAttenuation.m_fConstant = 0.0f;
	m_oAttenuation.m_fLinear = 1.0f;
	m_oAttenuation.m_fQuadratic = 0.0f;

	m_bIsOn = true; //Default the light to being on
}

PointLight::PointLight(float exponent, float range, glm::vec3& color, glm::vec3& position)
{
	m_fExponent = exponent;
	m_fCutoff = 180.0f; //Point Lights always have a cutoff of 180*
	m_fRange = range;
	m_v3Color = color;
	m_v3LocalPosition = position;
	m_v3Direction = glm::vec3(0.0f, 0.0f, 0.0f); //Point light. Direction doesn't matter!

	//Default attenuation
	m_oAttenuation.m_fConstant = 0.0f;
	m_oAttenuation.m_fLinear = 1.0f;
	m_oAttenuation.m_fQuadratic = 0.0f;

	m_bIsOn = true; //Default the light to being on
}

PointLight::PointLight(float exponent, float range, glm::vec3& color, glm::vec3& position, Attenuation& attenuation, bool isOn)
{
	m_fExponent = exponent;
	m_fCutoff = 180.0f; //Point Lights always have a cutoff of 180*
	m_fRange = range;
	m_v3Color = color;
	m_v3LocalPosition = position;
	m_v3Direction = glm::vec3(0.0f, 0.0f, 0.0f); //Point light. Direction doesn't matter!

	m_oAttenuation = attenuation;

	m_bIsOn = isOn;
}

PointLight::~PointLight()
{
}

void PointLight::Update(float deltaTime)
{
	//Update the base object
	Object::Update(deltaTime);

	//Set the position of the light according to the updated transform
	m_v4Position = m_m4Transform[3];
}