#include "Object/Light/AmbientLight.h"

AmbientLight::AmbientLight()
{
	m_v3Color = glm::vec3(0.1f, 0.1f, 0.1f);
}

AmbientLight::AmbientLight(glm::vec3& color)
{
	m_v3Color = color;
}

AmbientLight::~AmbientLight()
{

}

void AmbientLight::Update(float deltaTime)
{

}