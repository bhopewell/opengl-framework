#include "Object/Light/DirectionalLight.h"

DirectionalLight::DirectionalLight()
{
	m_v3Color = glm::vec3(1.0f, 1.0f, 1.0f); //Default color of white
	m_v4Position = glm::vec4(0.0f, 0.0f, 1.0f, 0.0f); //Default position/direction along the forward Z

	m_bIsOn = true;
}

DirectionalLight::DirectionalLight(glm::vec3& color)
{
	m_v3Color = color;
	m_v4Position = glm::vec4(0.0f, 0.0f, 1.0f, 0.0f); //Default position/direction along the forward Z

	m_bIsOn = true;
}

DirectionalLight::DirectionalLight(glm::vec3& color, glm::vec3& direction, bool isOn)
{
	m_v3Color = color;
	m_v4Position = glm::vec4(-direction, 0.0f);

	m_bIsOn = isOn;
}

DirectionalLight::~DirectionalLight()
{

}

void DirectionalLight::Update(float deltaTime)
{

}