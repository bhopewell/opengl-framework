#include "Object/Object.h"

#include "glm/gtc/matrix_transform.hpp"
#include <glm/gtc/quaternion.hpp>
#include <glm/gtx/quaternion.hpp>

#include "Debug.h"

Object::Object()
{
	//Null the parent and clear the children
	m_poParent = nullptr;
	m_lChildren.clear();

	m_v3LocalPosition = glm::vec3(0.f);
	m_v3Scale = glm::vec3(1.f);
	m_v3Rotation = glm::vec3(0.f);

	//Create the model matrix from the position, rotation and scale
	glm::mat4 scaleMat = glm::scale(glm::mat4(1.0f), m_v3Scale);
	glm::mat4 rotationMat = glm::toMat4(glm::quat(m_v3Rotation));
	glm::mat4 translateMat = glm::translate(glm::mat4(1.0f), m_v3LocalPosition);
	m_m4Transform = translateMat * rotationMat * scaleMat;
}

Object::Object(const glm::vec3& position)
{
	//Null the parent and clear the children
	m_poParent = nullptr;
	m_lChildren.clear();

	m_v3LocalPosition = position;
	m_v3Scale = glm::vec3(1.f);
	m_v3Rotation = glm::vec3(0.f);

	//Create the model matrix from the position, rotation and scale
	glm::mat4 scaleMat = glm::scale(glm::mat4(1.0f), m_v3Scale);
	glm::mat4 rotationMat = glm::toMat4(glm::quat(m_v3Rotation));
	glm::mat4 translateMat = glm::translate(glm::mat4(1.0f), m_v3LocalPosition);
	m_m4Transform = translateMat * rotationMat * scaleMat;
}

Object::Object(const glm::vec3& position, const glm::vec3& scale)
{
	//Null the parent and clear the children
	m_poParent = nullptr;
	m_lChildren.clear();

	m_v3LocalPosition = position;
	m_v3Scale = scale;
	m_v3Rotation = glm::vec3(0.f);

	//Create the model matrix from the position, rotation and scale
	glm::mat4 scaleMat = glm::scale(glm::mat4(1.0f), m_v3Scale);
	glm::mat4 rotationMat = glm::toMat4(glm::quat(m_v3Rotation));
	glm::mat4 translateMat = glm::translate(glm::mat4(1.0f), m_v3LocalPosition);
	m_m4Transform = translateMat * rotationMat * scaleMat;
}

Object::Object(const glm::vec3& position, const glm::vec3& scale, const glm::vec3& rotationRadians)
{
	//Null the parent and clear the children
	m_poParent = nullptr;
	m_lChildren.clear();

	m_v3LocalPosition = position;
	m_v3Scale = scale;
	m_v3Rotation = rotationRadians;

	//Create the model matrix from the position, rotation and scale
	glm::mat4 scaleMat = glm::scale(glm::mat4(1.0f), m_v3Scale);
	glm::mat4 rotationMat = glm::toMat4(glm::quat(m_v3Rotation));
	glm::mat4 translateMat = glm::translate(glm::mat4(1.0f), m_v3LocalPosition);
	m_m4Transform = translateMat * rotationMat * scaleMat;
}

Object::~Object()
{
	//Remove this from the parent object and null out the parent object ptr.
	if (m_poParent != nullptr)
		m_poParent->RemoveChild(this);

	m_poParent = nullptr;

	//Set the parent of all the child objects to null and clear the children.
	for (auto it = m_lChildren.begin(); it != m_lChildren.end(); ++it)
	{
		(*it._Ptr)->SetParent(nullptr);
	}
	m_lChildren.clear();
}

void Object::Update(float deltaTime)
{
	if (m_poParent == nullptr)
	{
		//Create the model matrix from the position, rotation and scale
		glm::mat4 scale = glm::scale(glm::mat4(1.0f), m_v3Scale);
		glm::mat4 rotation = glm::toMat4(glm::quat(m_v3Rotation));
		glm::mat4 translate = glm::translate(glm::mat4(1.0f), m_v3LocalPosition);
		m_m4Transform = translate * rotation * scale;
	}
	else
	{
		//Create the model matrix from the position, rotation and scale
		glm::mat4 scale = glm::scale(glm::mat4(1.0f), m_v3Scale);
		glm::mat4 rotation = glm::toMat4(glm::quat(m_v3Rotation));
		glm::mat4 translate = glm::translate(m_poParent->GetTransform(), m_v3LocalPosition);
		m_m4Transform = translate * rotation * scale;
	}
}