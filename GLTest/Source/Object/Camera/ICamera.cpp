#include "Object/Camera/ICamera.h"
#include "Application/Application.h"

#include "glm/gtc/matrix_transform.hpp"

#include "Debug.h"

ICamera::ICamera()
	: Object()
{
	m_v3LocalPosition = glm::vec3(0, 1, 15);
}

ICamera::~ICamera()
{

}

void ICamera::Init()
{
	m_fFOV = glm::radians(45.0f);
	m_fAspectRatio = Application::Instance()->GetScreenSize().x / Application::Instance()->GetScreenSize().y;
	m_fNearPlane = 0.1f;
	m_fFarPlane = 1000.0f;

	//Set up the projection matrix
	m_m4Projection = glm::perspective(m_fFOV, m_fAspectRatio, m_fNearPlane, m_fFarPlane);

	//Camera matrix
	m_m4View = glm::lookAt(
		m_v3LocalPosition, //Camera location
		glm::vec3(0, 0, 0), //Point to look at
		glm::vec3(0, 1, 0)); //Up vector
}

void ICamera::Init(float fov, float aspectRatio, float nearPlane, float farPlane)
{
	m_fFOV = glm::radians(fov);
	m_fAspectRatio = aspectRatio;
	m_fNearPlane = nearPlane;
	m_fFarPlane = farPlane;

	//Set up the projection matrix
	m_m4Projection = glm::perspective(m_fFOV, m_fAspectRatio, m_fNearPlane, m_fFarPlane);
	
	//Camera matrix
	m_m4View = glm::lookAt(
		m_v3LocalPosition, //Camera location
		glm::vec3(0, 0, 0), //Point to look at
		glm::vec3(0, 1, 0)); //Up vector
}