#include "Object/Camera/FreeCam.h"

#include "Application/Application.h"

#include <GLFW/glfw3.h>

FreeCam::FreeCam()
{

}

FreeCam::~FreeCam()
{

}

void FreeCam::Update(float deltaTime)
{
	double xPos;
	double yPos;

	//Get the mouse position
	glfwGetCursorPos(Application::Instance()->GetWindow(), &xPos, &yPos);

	//Compute the orientation
	m_fHorizontalAngle += m_fMouseSpeed * deltaTime * ((Application::Instance()->GetScreenSize().x * 0.5f) - (float)xPos);
	m_fVerticalAngle += m_fMouseSpeed * deltaTime * ((Application::Instance()->GetScreenSize().y * 0.5f) - (float)yPos);

	//Get the direction matrix
	glm::vec3 dir = glm::vec3(cos(m_fVerticalAngle) * sin(m_fHorizontalAngle),
		sin(m_fVerticalAngle),
		cos(m_fVerticalAngle) * cos(m_fHorizontalAngle));

	//Define the right vector
	glm::vec3 right = glm::vec3(sin(m_fHorizontalAngle - glm::half_pi<double>()),
		0,
		cos(m_fHorizontalAngle - glm::half_pi<double>()));

	//Get the up vector from a cross prod of the right and dir
	glm::vec3 up = glm::cross(right, dir);
	//Put restrictions on the camera so we can't view anything upside down... Upside down is just wrong.
	if (up.y <= 0)
		up.y = 0;

	if (glfwGetKey(Application::Instance()->GetWindow(), GLFW_KEY_LEFT_SHIFT) == GLFW_PRESS)
		m_fSpeedMod = 2.0f;
	else
		m_fSpeedMod = 1.0f;

	//Move forward
	if (glfwGetKey(Application::Instance()->GetWindow(), GLFW_KEY_UP) == GLFW_PRESS ||
		glfwGetKey(Application::Instance()->GetWindow(), GLFW_KEY_W) == GLFW_PRESS)
	{
		m_v3LocalPosition += dir * deltaTime * m_fSpeed * m_fSpeedMod;
	}

	//Move backwards
	if (glfwGetKey(Application::Instance()->GetWindow(), GLFW_KEY_DOWN) == GLFW_PRESS ||
		glfwGetKey(Application::Instance()->GetWindow(), GLFW_KEY_S) == GLFW_PRESS)
	{
		m_v3LocalPosition -= dir * deltaTime * m_fSpeed * m_fSpeedMod;
	}

	//Move left
	if (glfwGetKey(Application::Instance()->GetWindow(), GLFW_KEY_LEFT) == GLFW_PRESS ||
		glfwGetKey(Application::Instance()->GetWindow(), GLFW_KEY_A) == GLFW_PRESS)
	{
		m_v3LocalPosition -= right * deltaTime * m_fSpeed * m_fSpeedMod;
	}

	//Move right
	if (glfwGetKey(Application::Instance()->GetWindow(), GLFW_KEY_RIGHT) == GLFW_PRESS ||
		glfwGetKey(Application::Instance()->GetWindow(), GLFW_KEY_D) == GLFW_PRESS)
	{
		m_v3LocalPosition += right * deltaTime * m_fSpeed * m_fSpeedMod;
	}

	//Recalculate the matrices
	m_m4Projection = glm::perspective(m_fFOV, m_fAspectRatio, m_fNearPlane, m_fFarPlane);
	m_m4View = glm::lookAt(m_v3LocalPosition, m_v3LocalPosition + dir, up);

	//Reset the mouse position to the center of the screen so we don't go out of the window bounds
	glfwSetCursorPos(Application::Instance()->GetWindow(),
		Application::Instance()->GetScreenSize().x * 0.5f,
		Application::Instance()->GetScreenSize().y * 0.5f);

	//Update the base object
	Object::Update(deltaTime);
}