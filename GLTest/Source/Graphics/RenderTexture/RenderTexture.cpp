#include "Graphics\RenderTexture\RenderTexture.h"
#include "Application\Application.h"

#include <IL/il.h>
#include <IL/ilu.h>
#include <IL/ilut.h>

#include "Debug.h"
#include "Util/Logger/Logger.h"

RenderTexture::RenderTexture(bool useDepthBuffer)
	: Object()
{
	m_bUseDepthBuffer = useDepthBuffer;

	CreateFrameBuffer();
}

RenderTexture::~RenderTexture()
{
	glDeleteTextures(1, &m_uiDepthTextureID);
	glDeleteTextures(1, &m_uiColorTextureID);

	glDeleteFramebuffers(1, &m_uiFrameBufferID);
}

void RenderTexture::CreateFrameBuffer()
{
	glm::vec2 screenSize = Application::Instance()->GetScreenSize();

	//Create the frame buffer
	glGenFramebuffers(1, &m_uiFrameBufferID);
	glBindFramebuffer(GL_FRAMEBUFFER, m_uiFrameBufferID);

	//Create the color texture
	CreateTexture(m_uiColorTextureID);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, m_uiColorTextureID, 0);

	//Create the depth buffer if we plan on using it
	if (m_bUseDepthBuffer)
	{
		//Create the depth texture
		CreateTexture(m_uiDepthTextureID, true);
		glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, m_uiDepthTextureID, 0);
	}

	//Ensure our framebuffer is okay
	GLenum result = glCheckFramebufferStatus(GL_FRAMEBUFFER);
	if (result != GL_FRAMEBUFFER_COMPLETE)
	{
		//Print the error to the output window
		Logger::LogError("Framebuffer not complete!");
		return;
	}

	//Unbind the framebuffer and textures.
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	glBindTexture(GL_TEXTURE_2D, 0);
}

void RenderTexture::CreateTexture(GLuint& textureID, bool isDepthTex)
{
	glm::vec2 screenSize = Application::Instance()->GetScreenSize();

	//Create the render texture
	glGenTextures(1, &textureID);
	//Bind the newly created texture that we will be rendering with
	glBindTexture(GL_TEXTURE_2D, textureID);

	//Create the appropriate glTexImage2D
	if (isDepthTex)
	{
		//Give an empty image to OpenGL. Note: We pass 0 through as the last param hence the empty tex
		glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT24, (int)screenSize.x, (int)screenSize.y, 0, GL_DEPTH_COMPONENT, GL_UNSIGNED_BYTE, 0);
	}
	else
	{
		//Give an empty image to OpenGL. Note: We pass 0 through as the last param hence the empty tex
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, (int)screenSize.x, (int)screenSize.y, 0, GL_RGB, GL_UNSIGNED_BYTE, 0);
	}

	//Poor filtering which is apparently required
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
}

void RenderTexture::Update(float deltaTime)
{
	Object::Update(deltaTime);
}

void RenderTexture::StartRender()
{
	// Render to our framebuffer
	glBindFramebuffer(GL_FRAMEBUFFER, m_uiFrameBufferID);

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Set the list of draw buffers
	GLenum DrawBuffers[1] = { GL_COLOR_ATTACHMENT0 };
	glDrawBuffers(1, DrawBuffers); //1 is the size of DrawBuffers
}

void RenderTexture::EndRender()
{
	// Render to the screen
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
}