#include "Graphics\Sprite\BillboardSprite.h"
#include "Application/Application.h"
#include "Object/Camera/ICamera.h"

#include "Graphics/ShaderTechnique/ITechnique.h"

#include "Debug.h"

BillboardSprite::BillboardSprite(BillboardType billboardType)
	: Quad()
{
	m_eBillboardType = billboardType;
}

BillboardSprite::~BillboardSprite()
{

}

void BillboardSprite::Update(float deltaTime)
{

}