#include "Graphics/Font/FontManager.h"

#include <vector>

#include "AssetManagement/Shader/ShaderManager.h"
#include "AssetManagement/Texture/TextureManager.h"

#include "Graphics/ShaderTechnique/ITechnique.h"
#include "Graphics/ShaderTechnique/TechniqueFactory/TechniqueFactory.h"
#include "Graphics/ShaderTechnique/TechniqueFactory/ETechnique.h"

#include "Application\Application.h"

#include "Util/Logger/Logger.h"

#include "Debug.h"

FontManager* FontManager::m_poInstance = nullptr;

FontManager::FontManager()
{
	m_poShaderTechnique = TechniqueFactory::CreateTechnique(ETechnique::EFont);
}

FontManager::~FontManager()
{
	//Delete buffers
	glDeleteBuffers(1, &m_uiText2DVertexBufferID);
	glDeleteBuffers(1, &m_uiText2DUVBufferID);

	//Destroy the shader technique
	if (m_poShaderTechnique != nullptr)
	{
		delete m_poShaderTechnique;
		m_poShaderTechnique = nullptr;
	}
}

void FontManager::Destroy()
{
	if (m_poInstance != nullptr)
	{
		delete m_poInstance;
		m_poInstance = nullptr;
	}
}

void FontManager::InitFontManager(const char* texturePath)
{
	// Initialize texture
	m_uiText2DTextureID = TextureManager::Instance()->LoadTexture(texturePath);

	// Initialize VBO
	glGenBuffers(1, &m_uiText2DVertexBufferID);
	glGenBuffers(1, &m_uiText2DUVBufferID);

	if (!m_poShaderTechnique->Initialize())
	{
		//Log the error
		Logger::LogError("Failed to initialize shader technique!");
		return;
	}

	// Initialize uniforms' IDs
	m_uiTextUniformID = m_poShaderTechnique->GetUniformLocation("diffuseTex");
	m_uiScreenSizeUniformID = m_poShaderTechnique->GetUniformLocation("screenSize");
	m_uiVertAttribID = m_poShaderTechnique->GetAttribLocation("vertPosIn");
	m_uiUVAttribID = m_poShaderTechnique->GetAttribLocation("vertUVIn");

	Logger::Log("Font Manager Initialized.");
}

void FontManager::RenderText(const char* text, glm::vec2& pos, float size)
{
	std::vector<glm::vec2> verts;
	std::vector<glm::vec2> uvs;

	for (int i = 0; ; ++i)
	{
		if (text[i] == '\0')
		{
			break;
		}

		//Generate the verts
		glm::vec2 topLeftVert = glm::vec2(pos.x + (i * size), pos.y + size);
		glm::vec2 topRightVert = glm::vec2(pos.x + (i * size) + size, pos.y + size);
		glm::vec2 bottomRightVert = glm::vec2(pos.x + (i * size) + size, pos.y);
		glm::vec2 bottomLeftVert = glm::vec2(pos.x + (i * size), pos.y);

		//Add the verts to the vert buffer
		verts.push_back(topLeftVert);
		verts.push_back(bottomLeftVert);
		verts.push_back(topRightVert);

		verts.push_back(bottomRightVert);
		verts.push_back(topRightVert);
		verts.push_back(bottomLeftVert);

		char character = text[i];
		float uv_x = (character % 16) / 16.0f;
		float uv_y = (character / 16) / 16.0f;

		//Generate the UVS
		glm::vec2 topLeftUV = glm::vec2(uv_x, uv_y);
		glm::vec2 topRightUV = glm::vec2(uv_x + 1.0f / 16.0f, uv_y);
		glm::vec2 bottomRightUV = glm::vec2(uv_x + 1.0f / 16.0f, (uv_y + 1.0f / 16.0f));
		glm::vec2 bottomLeftUV = glm::vec2(uv_x, (uv_y + 1.0f / 16.0f));

		//Add the uvs to the uv buffer
		uvs.push_back(topLeftUV);
		uvs.push_back(bottomLeftUV);
		uvs.push_back(topRightUV);

		uvs.push_back(bottomRightUV);
		uvs.push_back(topRightUV);
		uvs.push_back(bottomLeftUV);
	}

	glBindBuffer(GL_ARRAY_BUFFER, m_uiText2DVertexBufferID);
	glBufferData(GL_ARRAY_BUFFER, verts.size() * sizeof(glm::vec2), &verts[0], GL_STATIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, m_uiText2DUVBufferID);
	glBufferData(GL_ARRAY_BUFFER, uvs.size() * sizeof(glm::vec2), &uvs[0], GL_STATIC_DRAW);

	//Use the shader technique
	m_poShaderTechnique->Activate();

	//Bind the texture
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, m_uiText2DTextureID);
	//Set our sampler to user Texture Unit 0
	glUniform1i(m_uiTextUniformID, 0);

	glUniform2f(m_uiScreenSizeUniformID, Application::Instance()->GetScreenSize().x, Application::Instance()->GetScreenSize().y);

	// 1st attribute buffer : vertices
	glEnableVertexAttribArray(m_uiVertAttribID);
	glBindBuffer(GL_ARRAY_BUFFER, m_uiText2DVertexBufferID);
	glVertexAttribPointer(m_uiVertAttribID, 2, GL_FLOAT, GL_FALSE, 0, (void*)0);

	// 2nd attribute buffer : UVs
	glEnableVertexAttribArray(m_uiUVAttribID);
	glBindBuffer(GL_ARRAY_BUFFER, m_uiText2DUVBufferID);
	glVertexAttribPointer(m_uiUVAttribID, 2, GL_FLOAT, GL_FALSE, 0, (void*)0);

	// Draw call
	glDrawArrays(GL_TRIANGLES, 0, verts.size());

	glDisableVertexAttribArray(m_uiVertAttribID);
	glDisableVertexAttribArray(m_uiUVAttribID);

	//Unbind the font texture and stop using the current shader program
	glBindTexture(GL_TEXTURE_2D, 0);
	m_poShaderTechnique->Deactivate();
}