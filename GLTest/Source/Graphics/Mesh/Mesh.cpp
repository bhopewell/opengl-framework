#include "Graphics\Mesh\Mesh.h"
#include "AssetManagement\Mesh\MeshLoader.h"

#include "Graphics/ShaderTechnique/ITechnique.h"

#include "Debug.h"

Mesh::Mesh(const char* filepath)
{
	//Load the mesh
	LoadMesh(filepath);
}

Mesh::~Mesh()
{
	glDeleteBuffers(1, &m_uiVertBufferID);
	glDeleteBuffers(1, &m_uiUVBufferID);
	glDeleteBuffers(1, &m_uiNormalBufferID);
	glDeleteBuffers(1, &m_uiTangentBufferID);
	glDeleteBuffers(1, &m_uiBiTangentBufferID);
	glDeleteBuffers(1, &m_uiIndexBufferID);
}

void Mesh::LoadMesh(const char* filepath)
{
	std::vector<glm::vec3> verts;
	std::vector<glm::vec2> uvs;
	std::vector<glm::vec3> normals;
	std::vector<glm::vec3> tangents;
	std::vector<glm::vec3> bitangents;

	if (!MeshLoader::LoadMeshAssimp(filepath, m_auiIndexBuffer, m_v3Verts, m_v2UVs, m_v3Normals))
	{
		printf("Failed to load mesh!\n");
		return;
	}

	//Create the vertex buffer data
	CreateVertexData();

	//Create the index buffer data
	CreateIndexData();

	//If there are UV's, create the UV data
	if (!m_v2UVs.empty())
	{
		//Create the uv buffer data
		CreateTextureData();
	}

	if (!m_v3Normals.empty())
	{
		//Create normal buffer data
		CreateNormalData();
	}
}

//Create the VertexArrayObject
void Mesh::CreateVertexData()
{
	//Generate the vert array
	GLuint VertexArrayID;
	glGenVertexArrays(1, &VertexArrayID);
	glBindVertexArray(VertexArrayID);

	//Set the vertex and index counts
	m_uiVertCount = m_v3Verts.size();

	//Generate 1 buffer, put the result ID in m_uiVertBuffer
	glGenBuffersARB(1, &m_uiVertBufferID);
	glBindBufferARB(GL_ARRAY_BUFFER, m_uiVertBufferID);
	//Bind our buffer data with the actual buffer
	glBufferDataARB(GL_ARRAY_BUFFER, m_uiVertCount * sizeof(glm::vec3), &m_v3Verts[0], GL_STATIC_DRAW);
}

//Create the VertexArrayObject
void Mesh::CreateIndexData()
{
	//Set the index count
	m_uiIndexCount = m_auiIndexBuffer.size();

	//Generate the index buffer, placing the result in m_uiIndexBufferID
	glGenBuffersARB(1, &m_uiIndexBufferID);
	glBindBufferARB(GL_ELEMENT_ARRAY_BUFFER, m_uiIndexBufferID);
	glBufferDataARB(GL_ELEMENT_ARRAY_BUFFER, m_uiIndexCount * sizeof(GLuint), &m_auiIndexBuffer[0], GL_STATIC_DRAW);
}

void Mesh::CreateTextureData()
{
	//Set the UV count
	m_uiUVCount = m_v2UVs.size();

	//Generate the UV buffer
	glGenBuffersARB(1, &m_uiUVBufferID);
	glBindBufferARB(GL_ARRAY_BUFFER, m_uiUVBufferID);
	//Bind our UV data to the actual buffer
	glBufferDataARB(GL_ARRAY_BUFFER, m_uiUVCount * sizeof(glm::vec2), &m_v2UVs[0], GL_STATIC_DRAW);
}

void Mesh::CreateNormalData()
{
	CalculateTangentData();

	//Set the normal count
	m_uiNormalCount = m_v3Normals.size();
	m_uiTangentCount = m_v3Tangents.size();
	m_uiBiTangentCount = m_v3BiTangents.size();

	//Generate the normal buffer
	glGenBuffersARB(1, &m_uiNormalBufferID);
	glBindBufferARB(GL_ARRAY_BUFFER, m_uiNormalBufferID);
	//Bind our UV data to the actual buffer
	glBufferDataARB(GL_ARRAY_BUFFER, m_uiNormalCount * sizeof(glm::vec3), &m_v3Normals[0], GL_STATIC_DRAW);

	//Generate the tangent buffer
	glGenBuffersARB(1, &m_uiTangentBufferID);
	glBindBufferARB(GL_ARRAY_BUFFER, m_uiTangentBufferID);
	glBufferDataARB(GL_ARRAY_BUFFER, m_uiTangentCount * sizeof(glm::vec3), &m_v3Tangents[0], GL_STATIC_DRAW);

	//Generate the bitangent buffer
	glGenBuffersARB(1, &m_uiBiTangentBufferID);
	glBindBufferARB(GL_ARRAY_BUFFER, m_uiBiTangentBufferID);
	glBufferDataARB(GL_ARRAY_BUFFER, m_uiBiTangentCount * sizeof(glm::vec3), &m_v3BiTangents[0], GL_STATIC_DRAW);
	
}

void Mesh::CalculateTangentData()
{
	if ((m_uiIndexCount % 3) != 0)
	{
		fprintf(stderr, "CalculatingTangents will break. Indices[] not divisible by 3");
	}

	m_v3Tangents = std::vector<glm::vec3>(m_v3Verts.size());
	m_v3BiTangents = std::vector<glm::vec3>(m_v3Verts.size());

	for (unsigned int i = 0; i < m_uiIndexCount; i += 3)
	{
		GLuint indices[] = {
			m_auiIndexBuffer[i],
			m_auiIndexBuffer[i + 1],
			m_auiIndexBuffer[i + 2],
		};

		//Face verts
		glm::vec3 faceVerts[] =
		{
			m_v3Verts[indices[0]],
			m_v3Verts[indices[1]],
			m_v3Verts[indices[2]]
		};

		//Face tex coords
		glm::vec2 faceUvs[] =
		{
			m_v2UVs[indices[0]],
			m_v2UVs[indices[1]],
			m_v2UVs[indices[2]],
		};

		//Face normals
		glm::vec3 faceNormals[] =
		{
			m_v3Normals[indices[0]],
			m_v3Normals[indices[1]],
			m_v3Normals[indices[2]]
		};

		//Edges of the tri : position delta
		glm::vec3 edge0 = faceVerts[1] - faceVerts[0];
		glm::vec3 edge1 = faceVerts[2] - faceVerts[0];

		//UV delta
		glm::vec2 uvDelta0 = faceUvs[1] - faceUvs[0];
		glm::vec2 uvDelta1 = faceUvs[2] - faceUvs[0];

		float r = 1.0f / (uvDelta0.x * uvDelta1.y - uvDelta0.y * uvDelta1.x);
		glm::vec3 tangent = (edge0 * uvDelta1.y - edge1 * uvDelta0.y) * r;
		glm::vec3 bitangent = (edge1 * uvDelta0.x - edge0 * uvDelta1.x) * r;

		m_v3Tangents[indices[0]] = glm::normalize(tangent - faceNormals[0] * glm::dot(faceNormals[0], tangent));
		m_v3Tangents[indices[1]] = glm::normalize(tangent - faceNormals[1] * glm::dot(faceNormals[1], tangent));
		m_v3Tangents[indices[2]] = glm::normalize(tangent - faceNormals[2] * glm::dot(faceNormals[2], tangent));

		m_v3BiTangents[indices[0]] = glm::normalize(bitangent);
		m_v3BiTangents[indices[1]] = glm::normalize(bitangent);
		m_v3BiTangents[indices[2]] = glm::normalize(bitangent);

		m_v3Tangents[indices[0]] = ValidateTangent(m_v3Tangents[indices[0]], m_v3BiTangents[indices[0]], faceNormals[0]);
		m_v3Tangents[indices[1]] = ValidateTangent(m_v3Tangents[indices[1]], m_v3BiTangents[indices[1]], faceNormals[1]);
		m_v3Tangents[indices[2]] = ValidateTangent(m_v3Tangents[indices[2]], m_v3BiTangents[indices[2]], faceNormals[2]);
	}
}

glm::vec3 Mesh::ValidateTangent(glm::vec3 tangent, glm::vec3 bitangent, glm::vec3 normal)
{
	if (glm::dot(glm::cross(normal, tangent), bitangent) < 0.0f)
		return tangent * -1.0f;

	return tangent;
}

void Mesh::Update(float deltaTime)
{

}

void Mesh::Render()
{
#ifdef _SHADERDEBUG
	glColor3f(0, 0, 1);
	glBegin(GL_LINES);

	for (unsigned int i = 0; i<m_auiIndexBuffer.size(); i++)
	{
		glm::vec3 p = m_v3Verts[m_auiIndexBuffer[i]];
		glVertex3fv(&p.x);
		glm::vec3 o = glm::normalize(m_v3Normals[m_auiIndexBuffer[i]]);
		p += o*0.1f;
		glVertex3fv(&p.x);
	}

	glEnd();
#else

	//1st attribute buffer : verts
	glEnableVertexAttribArray(m_uiVertexAttribID);
	glBindBuffer(GL_ARRAY_BUFFER, m_uiVertBufferID);
	glVertexAttribPointer(m_uiVertexAttribID, 3, GL_FLOAT, GL_FALSE, 0, 0);

	//2nd attribute buffer : texture
	glEnableVertexAttribArray(m_uiUVAttribID);
	glBindBuffer(GL_ARRAY_BUFFER, m_uiUVBufferID);
	glVertexAttribPointer(m_uiUVAttribID, 2, GL_FLOAT, GL_FALSE, 0, 0);

	//3rd attribute buffer : normal
	glEnableVertexAttribArray(m_uiNormalAttribID);
	glBindBuffer(GL_ARRAY_BUFFER, m_uiNormalBufferID);
	glVertexAttribPointer(m_uiNormalAttribID, 3, GL_FLOAT, GL_FALSE, 0, 0);
	
	//4th attribute buffer : tangents
	glEnableVertexAttribArray(m_uiTangentAttribID);
	glBindBuffer(GL_ARRAY_BUFFER, m_uiTangentBufferID);
	glVertexAttribPointer(m_uiTangentAttribID, 3, GL_FLOAT, GL_FALSE, 0, 0);

	//5th attribute buffer : bitangents
	glEnableVertexAttribArray(m_uiBiTangentAttribID);
	glBindBuffer(GL_ARRAY_BUFFER, m_uiBiTangentBufferID);
	glVertexAttribPointer(m_uiBiTangentAttribID, 3, GL_FLOAT, GL_FALSE, 0, 0);

	//Render the mesh
	glBindBufferARB(GL_ELEMENT_ARRAY_BUFFER, m_uiIndexBufferID);
	glDrawElements(GL_TRIANGLES, m_uiIndexCount, GL_UNSIGNED_INT,0);

	//Disable the vertex attrib arrays
	glDisableVertexAttribArray(m_uiBiTangentAttribID);
	glDisableVertexAttribArray(m_uiTangentAttribID);
	glDisableVertexAttribArray(m_uiNormalAttribID);
	glDisableVertexAttribArray(m_uiUVAttribID);
	glDisableVertexAttribArray(m_uiVertexAttribID);
#endif
}
