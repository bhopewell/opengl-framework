#include "Graphics/ShaderTechnique/FontTechnique.h"

FontTechnique::FontTechnique()
{

}

FontTechnique::~FontTechnique()
{

}

bool FontTechnique::Initialize()
{
	if (!ITechnique::Initialize())
		return false;

	if (!AddShader("Resources/Shaders/Text/TextShader.vert", GL_VERTEX_SHADER))
		return false;

	if (!AddShader("Resources/Shaders/Text/TextShader.frag", GL_FRAGMENT_SHADER))
		return false;

	if (!Finalize())
		return false;

	return true;
}