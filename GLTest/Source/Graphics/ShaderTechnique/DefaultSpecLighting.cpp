#include "Graphics/ShaderTechnique/DefaultSpecLighting.h"

DefaultSpecLighting::DefaultSpecLighting()
{
}

DefaultSpecLighting::~DefaultSpecLighting()
{
}

bool DefaultSpecLighting::Initialize()
{
	if (!ITechnique::Initialize())
		return false;

	if (!AddShader("Resources/Shaders/ForwardRendering/SpecLighting.vert", GL_VERTEX_SHADER))
		return false;

	if (!AddShader("Resources/Shaders/ForwardRendering/SpecLighting.frag", GL_FRAGMENT_SHADER))
		return false;

	if (!Finalize())
		return false;

	return true;
}