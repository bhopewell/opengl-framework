#include "Graphics/ShaderTechnique/SpecBumpLighting.h"

SpecBumpLighting::SpecBumpLighting()
{
}

SpecBumpLighting::~SpecBumpLighting()
{
}

bool SpecBumpLighting::Initialize()
{
	if (!ITechnique::Initialize())
		return false;

	if (!AddShader("Resources/Shaders/ForwardRendering/SpecBumpLighting.vert", GL_VERTEX_SHADER))
		return false;

	if (!AddShader("Resources/Shaders/ForwardRendering/SpecBumpLighting.frag", GL_FRAGMENT_SHADER))
		return false;

	if (!Finalize())
		return false;

	return true;
}