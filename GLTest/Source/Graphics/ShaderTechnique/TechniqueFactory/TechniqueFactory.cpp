#include "Graphics/ShaderTechnique/TechniqueFactory/TechniqueFactory.h"
#include "Graphics/ShaderTechnique/TechniqueFactory/ETechnique.h"

#include "Graphics/ShaderTechnique/UnlitTechnique.h"
#include "Graphics/ShaderTechnique/DefaultLighting.h"
#include "Graphics/ShaderTechnique/DefaultSpecLighting.h"

#include "Graphics/ShaderTechnique/SpecMapLighting.h"
#include "Graphics/ShaderTechnique/SpecBumpLighting.h"

#include "Graphics/ShaderTechnique/SkyboxTechnique.h"
#include "Graphics/ShaderTechnique/FontTechnique.h"

#include "Graphics/ShaderTechnique/PreprocessDeferredTechnique.h"
#include "Graphics/ShaderTechnique/DeferredShadingTechnique.h"

#include "Application/Application.h" 

//TODO: Remove the shader Techniques from the solution and throw them in a DLL along with all the other graphics classes.

ITechnique* TechniqueFactory::CreateTechnique(ETechnique eTechnique) 
{
	//If we are using a deferred renderer, ensure that we create the correct shader type
	if (Application::Instance()->IsDeferred())
	{
		switch (eTechnique)
		{
			//Create a skybox technique 
			case ETechnique::ESkybox:
			{
				return new SkyboxTechnique();
			}
			//Create a font technique 
			case ETechnique::EFont:
			{
				return new FontTechnique();
			}
			//Create a deferred shading technique
			case ETechnique::EDeferred:
			{
				return new DeferredShadingTechnique();
			}
			//Create a preprocess technique 
			case ETechnique::EPreprocess:
			default:
			{
				return new PreprocessDeferredTechnique();
			}
		}
	}
	
	switch (eTechnique)
	{
		//Create an unlit technique
		case ETechnique::EUnlit:
		default:
		{
			return new UnlitTechnique();
		}
		//Create a default lighting technique
		case ETechnique::EDefaultLighting:
		{
			return new DefaultLighting();
		}
		//Create a default spec lighting technique 
		case ETechnique::EDefaultSpecLighting:
		{
			return new DefaultSpecLighting();
		}
		//Create a spec map lighting technique 
		case ETechnique::ESpecMapLighting:
		{
			return new SpecMapLighting();
		}
		//Create a spec/bump map lighting technique 
		case ETechnique::ESpecBumpLighting:
		{
			return new SpecBumpLighting();
		}
		//Create a skybox technique 
		case ETechnique::ESkybox:
		{
			return new SkyboxTechnique();
		}
		//Create a font technique 
		case ETechnique::EFont:
		{
			return new FontTechnique();
		}
		//Create a preprocess technique 
		case ETechnique::EPreprocess:
		{
			return new PreprocessDeferredTechnique();
		}
		//Create a deferred shading technique
		case ETechnique::EDeferred:
		{
			return new DeferredShadingTechnique();
		}
	}
}