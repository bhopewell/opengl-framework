#include "Graphics/ShaderTechnique/DefaultLighting.h"

DefaultLighting::DefaultLighting()
{
}

DefaultLighting::~DefaultLighting()
{
}

bool DefaultLighting::Initialize()
{
	if (!ITechnique::Initialize())
		return false;

	if (!AddShader("Resources/Shaders/ForwardRendering/DefaultLighting.vert", GL_VERTEX_SHADER))
		return false;

	if (!AddShader("Resources/Shaders/ForwardRendering/DefaultLighting.frag", GL_FRAGMENT_SHADER))
		return false;

	if (!Finalize())
		return false;

	return true;
}