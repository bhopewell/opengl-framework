#include "Graphics/ShaderTechnique/SkyboxTechnique.h"

SkyboxTechnique::SkyboxTechnique()
{
}

SkyboxTechnique::~SkyboxTechnique()
{
}

bool SkyboxTechnique::Initialize()
{
	if (!ITechnique::Initialize())
		return false;

	if (!AddShader("Resources/Shaders/Skybox/Skybox.vert", GL_VERTEX_SHADER))
		return false;

	if (!AddShader("Resources/Shaders/Skybox/Skybox.frag", GL_FRAGMENT_SHADER))
		return false;

	if (!Finalize())
		return false;

	return true;
}