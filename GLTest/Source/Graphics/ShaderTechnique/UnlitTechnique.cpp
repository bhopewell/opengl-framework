#include "Graphics/ShaderTechnique/UnlitTechnique.h"

UnlitTechnique::UnlitTechnique()
{
}

UnlitTechnique::~UnlitTechnique()
{
}

bool UnlitTechnique::Initialize()
{
	if (!ITechnique::Initialize())
		return false;

	if (!AddShader("Resources/Shaders/Unlit.vert", GL_VERTEX_SHADER))
		return false;

	if (!AddShader("Resources/Shaders/Unlit.frag", GL_FRAGMENT_SHADER))
		return false;

	if (!Finalize())
		return false;

	return true;
}