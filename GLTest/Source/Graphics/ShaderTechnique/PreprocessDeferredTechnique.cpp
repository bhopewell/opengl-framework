#include "Graphics/ShaderTechnique/PreprocessDeferredTechnique.h"

PreprocessDeferredTechnique::PreprocessDeferredTechnique()
{

}

PreprocessDeferredTechnique::~PreprocessDeferredTechnique()
{
}

bool PreprocessDeferredTechnique::Initialize()
{
	if (!ITechnique::Initialize())
		return false;

	if (!AddShader("Resources/Shaders/DeferredShading/Preprocess.vert", GL_VERTEX_SHADER))
		return false;

	if (!AddShader("Resources/Shaders/DeferredShading/Preprocess.frag", GL_FRAGMENT_SHADER))
		return false;

	if (!Finalize())
		return false;

	return true;
}