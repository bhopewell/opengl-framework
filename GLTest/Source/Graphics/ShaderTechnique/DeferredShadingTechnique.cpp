#include "Graphics/ShaderTechnique/DeferredShadingTechnique.h"

DeferredShadingTechnique::DeferredShadingTechnique()
{

}

DeferredShadingTechnique::~DeferredShadingTechnique()
{
}

bool DeferredShadingTechnique::Initialize()
{
	if (!ITechnique::Initialize())
		return false;

	if (!AddShader("Resources/Shaders/DeferredShading/DeferredShading.vert", GL_VERTEX_SHADER))
		return false;

	if (!AddShader("Resources/Shaders/DeferredShading/DeferredShading2.frag", GL_FRAGMENT_SHADER))
		return false;

	if (!Finalize())
		return false;

	return true;
}