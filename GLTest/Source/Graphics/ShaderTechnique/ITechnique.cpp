#include "Graphics/ShaderTechnique/ITechnique.h"
#include "AssetManagement/Shader/ShaderManager.h"

#include "Util/Logger/Logger.h"

#include <vector>

ITechnique::ITechnique()
{
	m_uiProgramID = 0;
}

ITechnique::~ITechnique()
{
	//Detach all the shaders
	for (auto it = m_lShaderList.begin(); it != m_lShaderList.end(); ++it)
	{
		glDetachShader(m_uiProgramID, it._Ptr->_Myval);
	}

	m_lShaderList.clear();

	//Delete the shader program
	if (m_uiProgramID != 0)
	{
		glDeleteProgram(m_uiProgramID);
		m_uiProgramID = 0;
	}
}

bool ITechnique::Initialize()
{
	//Create the shader program
	m_uiProgramID = glCreateProgram();

	//Ensure the shader program was created correctly
	if (m_uiProgramID == 0)
	{
		Logger::LogError("Error creating shader program");
		return false;
	}

	return true;
}

bool ITechnique::AddShader(const char* shaderName, GLenum shaderType)
{
	//Load the shader using our shader manager
	GLuint shaderID = ShaderManager::Instance()->LoadShader(shaderName, shaderType);

	//If our shader manager returns 0, something has gone wrong. Return false
	if (shaderID == 0)
		return false;

	//Push the shader ID to the shader list
	m_lShaderList.push_back(shaderID);

	return true;
}

bool ITechnique::Finalize()
{
	GLint Result = GL_FALSE;
	int InfoLogLength;

	//Attach all the shaders to the program
	for (auto it = m_lShaderList.begin(); it != m_lShaderList.end(); ++it)
	{
		glAttachShader(m_uiProgramID, it._Ptr->_Myval);
	}

	//Link the shader program
	glLinkProgram(m_uiProgramID);
	glGetProgramiv(m_uiProgramID, GL_LINK_STATUS, &Result);
	if (Result == GL_FALSE)
	{
		glGetProgramiv(m_uiProgramID, GL_INFO_LOG_LENGTH, &InfoLogLength);
		std::vector<char> ProgramErrorMessage(InfoLogLength > 1 ? InfoLogLength : 1);
		glGetProgramInfoLog(m_uiProgramID, InfoLogLength, NULL, &ProgramErrorMessage[0]);
		
		//Print the error
		std::string s = std::string("Failed to link shader program! Error Message: " + std::string(&ProgramErrorMessage[0]));
		Logger::LogError(s.c_str());
		
		return false;
	}

	//Validate the Shader Program
	glValidateProgram(m_uiProgramID);
	glGetProgramiv(m_uiProgramID, GL_VALIDATE_STATUS, &Result);
	if (Result == GL_FALSE)
	{
		glGetProgramiv(m_uiProgramID, GL_INFO_LOG_LENGTH, &InfoLogLength);
		std::vector<char> ProgramErrorMessage(InfoLogLength > 1 ? InfoLogLength : 1);
		glGetProgramInfoLog(m_uiProgramID, InfoLogLength, NULL, &ProgramErrorMessage[0]);
		
		//Print the error
		std::string s = std::string("Failed to validate shader program! Error Message: " + std::string(&ProgramErrorMessage[0]));
		Logger::LogError(s.c_str());

		return false;
	}

	return true;
}

void ITechnique::Activate()
{
	glUseProgram(m_uiProgramID);
}

void ITechnique::Deactivate()
{
	glUseProgram(0);
}

GLint ITechnique::GetUniformLocation(const char* uniformName)
{
	return glGetUniformLocation(m_uiProgramID, uniformName);
}

GLint ITechnique::GetAttribLocation(const char* attribName)
{
	return glGetAttribLocation(m_uiProgramID, attribName);
}