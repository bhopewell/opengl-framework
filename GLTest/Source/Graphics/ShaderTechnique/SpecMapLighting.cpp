#include "Graphics/ShaderTechnique/SpecMapLighting.h"

SpecMapLighting::SpecMapLighting()
{
}

SpecMapLighting::~SpecMapLighting()
{
}

bool SpecMapLighting::Initialize()
{
	if (!ITechnique::Initialize())
		return false;

	if (!AddShader("Resources/Shaders/ForwardRendering/SpecMapLighting.vert", GL_VERTEX_SHADER))
		return false;

	if (!AddShader("Resources/Shaders/ForwardRendering/SpecMapLighting.frag", GL_FRAGMENT_SHADER))
		return false;

	if (!Finalize())
		return false;

	return true;
}