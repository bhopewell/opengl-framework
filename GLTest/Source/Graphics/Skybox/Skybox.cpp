#include "Graphics/Skybox/Skybox.h"
#include "Graphics/Mesh/Mesh.h"
#include "AssetManagement/Texture/TextureManager.h"
#include "AssetManagement/Shader/ShaderManager.h"

#include "Application\Application.h"
#include "Object/Camera/ICamera.h"

#include "Graphics/ShaderTechnique/TechniqueFactory/TechniqueFactory.h"
#include "Graphics/ShaderTechnique/TechniqueFactory/ETechnique.h"

#include <glm/glm.hpp>

#include <vector>

Skybox::Skybox(const char* forwardTexture, const char* backTexture, const char* leftTexture,
				const char* rightTexture, const char* bottomTexture, const char* topTexture)
{
	//Load the cubemap textures
	LoadTextures(forwardTexture, backTexture, leftTexture, rightTexture, topTexture, bottomTexture);

	//Create the mesh object
	m_poCubeMesh = new Mesh("Resources/Models/cube.obj");
}

Skybox::~Skybox()
{
	if (m_poCubeMesh != nullptr)
	{
		delete m_poCubeMesh;
		m_poCubeMesh = nullptr;
	}

	//Destroy the shader technique
	if (m_poShaderTechnique != nullptr)
	{
		delete m_poShaderTechnique;
		m_poShaderTechnique = nullptr;
	}
}

void Skybox::LoadTextures(const char* forwardTexture, const char* backTexture, const char* leftTexture,
							const char* rightTexture, const char* bottomTexture, const char* topTexture)
{
	//Load the cubemap
	m_uiCubeMapID = TextureManager::Instance()->LoadTextureCubeMap(forwardTexture, backTexture,	
																	leftTexture, rightTexture, 
																	bottomTexture, topTexture);
}

bool Skybox::AttachShader()
{
	m_poShaderTechnique = TechniqueFactory::CreateTechnique(ETechnique::ESkybox);

	//If the intialization of the technique fails, don't finish creating the object.
	if (!m_poShaderTechnique->Initialize())
		return false;

	//Attach the shader to the mesh
	m_poCubeMesh->AttachShader(m_poShaderTechnique);

	return true;
}

void Skybox::BindTexture()
{
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_CUBE_MAP, m_uiCubeMapID);
}

void Skybox::UnbindTexture()
{
	glBindTexture(GL_TEXTURE_CUBE_MAP, 0);
}

void Skybox::Render()
{
	//Disable the depth testing and face culling because it's the skybox. It's meant to be at the back of everything and always visible
	glDisable(GL_DEPTH_TEST);
	glCullFace(GL_FRONT);

	//Use the skybox shaders
	m_poShaderTechnique->Activate();
	
	//Get a handle to our camera
	ICamera* camera = Application::Instance()->GetActiveCamera();

	//Get a handle for our "VP" uniform
	GLuint viewID = m_poShaderTechnique->GetUniformLocation("V");
	GLuint projID = m_poShaderTechnique->GetUniformLocation("P");

	//Pass the view and projection matrices to the shader
	glUniformMatrix4fv(viewID, 1, GL_FALSE, &camera->GetViewMatrix()[0][0]);
	glUniformMatrix4fv(projID, 1, GL_FALSE, &camera->GetProjectionMatrix()[0][0]);

	//Pass our camPos to the shader
	glUniform3fv(m_poShaderTechnique->GetUniformLocation("CameraPosition"), 1, &camera->GetPosition()[0]);

	//Bind the skybox textures
	BindTexture();
	
	//Actual render code here
	m_poCubeMesh->Render();

	UnbindTexture();
	
	//Stop using the skybox shaders
	m_poShaderTechnique->Deactivate();

	//Re-enable the depth test and face culling before we leave the render method
	glEnable(GL_DEPTH_TEST);
	glCullFace(GL_BACK);
}