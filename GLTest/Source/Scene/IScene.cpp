#include "Scene/IScene.h"

#include "Debug.h"

IScene::IScene()
{
	//Null out the camera pointer seeing as we don't know what projection the camera should be here.
	m_poCamera = nullptr;

	//Create the light manager
	m_poLightManager = new LightManager();

	m_szSceneName = "SceneName";
}

IScene::~IScene()
{
	//Destroy the light manager and therefore the lights
	if (m_poLightManager != nullptr)
	{
		delete m_poLightManager;
		m_poLightManager = nullptr;
	}
	
	//Destroy the camera
	if (m_poCamera != nullptr)
	{
		delete m_poCamera;
		m_poCamera = nullptr;
	}
}
