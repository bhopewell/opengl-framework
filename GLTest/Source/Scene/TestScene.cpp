#include "Scene/TestScene.h"
#include "Application/Application.h"

#include "Object/Camera/FreeCam.h"

#include "Object\DrawableObject\DrawableObject3D.h"
#include "Object\DrawableObject\DrawableObject2D.h"
#include "Graphics\Skybox\Skybox.h"

#include "Object/Light/PointLight.h"
#include "Object/Light/SpotLight.h"
#include "Object/Light/DirectionalLight.h"

#include "Graphics/Font/FontManager.h"

#include "Graphics/ShaderTechnique/TechniqueFactory/ETechnique.h"

#include "Graphics/Mesh/Mesh.h"
#include "Graphics/ShaderTechnique/TechniqueFactory/TechniqueFactory.h"

#include "GameTime\GameTime.h"

#include <GLFW\glfw3.h>

#include <string>

#include "Debug.h"

TestScene::TestScene() : IScene()
{
	m_szSceneName = "Test Scene";

	//Create the camera
	m_poCamera = new FreeCam();

	//Initialize the camera
	m_poCamera->Init(60.0f, Application::Instance()->GetScreenSize().x / Application::Instance()->GetScreenSize().y, 0.1f, 250.0f);

	//Create the drawable 3D objects
	m_poCube = new DrawableObject3D("Resources/Models/suzanne.obj");
	m_poCube->SetTextures("Resources/Textures/uvmap.dds");
	m_poCube->AttachShader(ETechnique::EDefaultLighting);
	m_poCube->SetPosition(glm::vec3(2, 0.5f, 0));
	m_poCube->SetCanRotate(false);

	m_poCube2 = new DrawableObject3D("Resources/Models/jeep.obj");
	m_poCube2->SetTextures("Resources/Textures/jeep_army.jpg");
	m_poCube2->AttachShader(ETechnique::EDefaultLighting);
	m_poCube2->SetPosition(glm::vec3(-5, -0.5f, 0));
	m_poCube2->SetScale(glm::vec3(0.01f, 0.01f, 0.01f));
	m_poCube2->SetRotation(glm::vec3(0, glm::radians(-90.0f), 0));
	m_poCube2->SetCanRotate(false);

	m_poPlane = new DrawableObject3D("Resources/Models/quad.dae");
	m_poPlane->SetTextures("Resources/Textures/diffuse.dds", "Resources/Textures/normal.bmp", "Resources/Textures/specular.dds");
	m_poPlane->AttachShader(ETechnique::ESpecBumpLighting);
	m_poPlane->SetScale(glm::vec3(20, 1, 20));
	m_poPlane->SetPosition(glm::vec3(0, -0.5f, 0));
	m_poPlane->SetCanRotate(false);

	//Create the lights
	m_poLightManager->AddLight(new SpotLight(10.0f, 60.0f, 10.0f, glm::vec3(1.0f, 1.0f, 1.0f), 
												glm::vec3(-5.0f, 5.0f, 2.5f), glm::vec3(0, -1, -1), 
												Attenuation(0.05f, 0.1f, 0.0125f)));
	m_poLightManager->AddLight(new DirectionalLight(glm::vec3(1.0f, 0.5f, 0.0f), glm::vec3(1.0f, 0.0f, 0.0f)));


	m_poPointLight = new PointLight(10.0f, 10.0f, glm::vec3(0.0f, 0.5f, 1.0f), glm::vec3(0.0f, 1.0f, 0.0f), Attenuation(0.05f, 0.1f, 0.0125f));
	m_poLightManager->AddLight(m_poPointLight);
	
	//Create the 2D object
	m_poSprite = new DrawableObject2D(glm::vec3(0, 2, 2), glm::vec3(1.0f));
	m_poSprite->SetTextures("Resources/Textures/AlphaTest.png");
	m_poSprite->AttachShader(ETechnique::EUnlit);
	m_poSprite->SetRotation(glm::vec3(glm::radians(90.0f), 0.0f, 0.0f));
	m_poSprite->SetCanOrbit(true);
	
	m_poCube->AddChild(m_poSprite);

	//Create the skybox
	m_poSkybox = new Skybox("Resources/Textures/Skybox/Front.png", "Resources/Textures/Skybox/Back.png", "Resources/Textures/Skybox/Left.png",
		"Resources/Textures/Skybox/Right.png", "Resources/Textures/Skybox/Bottom.png", "Resources/Textures/Skybox/Top.png");
	m_poSkybox->AttachShader();
}

TestScene::~TestScene()
{
	if (m_poSkybox != nullptr)
	{
		delete m_poSkybox;
		m_poSkybox = nullptr;
	}

	if (m_poSprite != nullptr)
	{
		delete m_poSprite;
		m_poSprite = nullptr;
	}

	if (m_poPlane != nullptr)
	{
		delete m_poPlane;
		m_poPlane = nullptr;
	}

	if (m_poCube2 != nullptr)
	{
		delete m_poCube2;
		m_poCube2 = nullptr;
	}

	if (m_poCube != nullptr)
	{
		delete m_poCube;
		m_poCube = nullptr;
	}
}

void TestScene::Update(float deltaTime)
{
	//Update the camera
	m_poCamera->Update(deltaTime);

	if (m_bUpdateLights)
	{
		//Update the light manager
		m_poLightManager->Update(deltaTime);
	}

	//Update the sprite object
	m_poSprite->Update(deltaTime);
	m_poPointLight->SetPosition(glm::vec3(m_poSprite->GetPosition()));

	//Update the 3D objects
	m_poCube->Update(deltaTime);
	m_poCube2->Update(deltaTime);
	m_poPlane->Update(deltaTime);
}

void TestScene::Render()
{
	//Enable depth testing
	glDisable(GL_DEPTH_TEST);
	//Enable backface culling
	glDisable(GL_CULL_FACE);

	//Render the skybox because that's a thing we need to do
	m_poSkybox->Render();

	//Enable depth testing
	glEnable(GL_DEPTH_TEST);
	//Enable backface culling
	glEnable(GL_CULL_FACE);

	m_poCube->Render();
	m_poCube2->Render();
	m_poPlane->Render();

	//Render the remainder of the objects. Anything with alpha goes last!
	m_poSprite->Render();
}

void TestScene::RenderGUI()
{
	//Render the font
	FontManager::Instance()->RenderText("W: Move forward", glm::vec2(10, 850), 20.f);
	FontManager::Instance()->RenderText("S: Move backward", glm::vec2(10, 825), 20.f);
	FontManager::Instance()->RenderText("A: Move left", glm::vec2(10, 800), 20.f);
	FontManager::Instance()->RenderText("D: Move right", glm::vec2(10, 775), 20.f);
	FontManager::Instance()->RenderText("Mouse: Rotate Camera", glm::vec2(10, 750), 20.f);
}

void TestScene::OnKeyPressed(int key)
{

}

void TestScene::OnKeyReleased(int key)
{
	//Handle the key press
	switch (key)
	{
		//If the escape key has been pressed, quit the application
		case GLFW_KEY_ESCAPE:
		{
			Application::Instance()->Quit();
		}
		break;
	}
}