#include "Scene/TestScene2.h"
#include "Application/Application.h"

#include "Object/Camera/FreeCam.h"

#include "Object\DrawableObject\DrawableObject3D.h"
#include "Object\DrawableObject\DrawableObject2D.h"
#include "Graphics\Skybox\Skybox.h"

#include "Graphics/ShaderTechnique/TechniqueFactory/ETechnique.h"

#include "Object/Light/AmbientLight.h"

#include "Graphics/Font/FontManager.h"

#include "AssetManagement/Texture/TextureManager.h"

#include <GLFW\glfw3.h>

#include <string>
#include <time.h>

#include "Debug.h"

TestScene2::TestScene2() : IScene()
{
	m_szSceneName = "Test Scene 2";

	//Create the camera
	m_poCamera = new FreeCam();

	//Initialize the camera
	m_poCamera->Init(60.0f, Application::Instance()->GetScreenSize().x / Application::Instance()->GetScreenSize().y, 0.1f, 1000.0f);

	m_poLightManager->GetAmbientLight()->SetColor(glm::vec3(1.0f, 1.0f, 1.0f));

	m_poPlane = new DrawableObject3D("Resources/Models/plane.obj");
	m_poPlane->SetTextures("Resources/Textures/diffuse.dds", "Resources/Textures/normal.bmp", "Resources/Textures/specular.dds");
	m_poPlane->AttachShader(ETechnique::ESpecBumpLighting);
	m_poPlane->SetScale(glm::vec3(1, 1, 1));
	m_poPlane->SetPosition(glm::vec3(-5, -0.5f, 0));
	m_poPlane->SetRotation(glm::vec3(glm::radians(90.0f), 0, 0));
	m_poPlane->SetCanRotate(false);

	//Create the skybox
	m_poSkybox = new Skybox("Resources/Textures/Skybox/Front.png", "Resources/Textures/Skybox/Back.png", "Resources/Textures/Skybox/Left.png",
		"Resources/Textures/Skybox/Right.png", "Resources/Textures/Skybox/Bottom.png", "Resources/Textures/Skybox/Top.png");
	m_poSkybox->AttachShader();
}

TestScene2::~TestScene2()
{
	if (m_poSkybox != nullptr)
	{
		delete m_poSkybox;
		m_poSkybox = nullptr;
	}

	if (m_poPlane != nullptr)
	{
		delete m_poPlane;
		m_poPlane = nullptr;
	}
}

void TestScene2::Update(float deltaTime)
{
	//Update the camera
	m_poCamera->Update(deltaTime);

	if (m_bUpdateLights)
	{
		//Update the light manager
		m_poLightManager->Update(deltaTime);
	}

	m_poPlane->Update(deltaTime);
}

void TestScene2::Render()
{
	//Render the skybox because that's a thing we need to do
	m_poSkybox->Render();

	//Enable depth testing
	glEnable(GL_DEPTH_TEST);
	//Enable backface culling
	glEnable(GL_CULL_FACE);

	m_poPlane->Render();
}

void TestScene2::OnKeyPressed(int key)
{

}

void TestScene2::OnKeyReleased(int key)
{
	//Handle the key press
	switch (key)
	{
		//If the escape key has been pressed, quit the application
		case GLFW_KEY_ESCAPE:
		{
			Application::Instance()->Quit();
		}
		break;
		case GLFW_KEY_L:
		{
			m_bUpdateLights = !m_bUpdateLights;
		}
		break;
	}
}