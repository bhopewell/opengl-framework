//////////////////////////////////////////////////////////////////////
//	Author: Benjamin J Hopewell
//	Date: 24/08/2013
//	File: InputManager.cpp
//	Brief: Implementation of the InputManager Class. 
//			Handles any input received from the user
//////////////////////////////////////////////////////////////////////
#include <string.h>
#include <GLFW/glfw3.h>

#include "Input/InputManager.h"
#include "Application/Application.h"

#include "Util/Logger/Logger.h"

#include "Debug.h"

InputManager::InputManager()
{
}

InputManager::~InputManager()
{
}

void InputManager::Init()
{
	glfwSetScrollCallback(Application::Instance()->GetWindow(), InputManager::OnScrollEvent);

	//Init all the keys as not being pressed
	for (int i = 0; i < MAX_KEYS; ++i)
	{
		m_abCurrentKeys[i] = false;
		m_abPrevKeys[i] = false;
	}

	Logger::Log("Input Manager Initialized.");
}

void InputManager::Update()
{
	for (int i = 0; i < MAX_KEYS; ++i)
	{
		m_abCurrentKeys[i] = glfwGetKey(Application::Instance()->GetWindow(), i) != GLFW_RELEASE;

		//If the current key press isn't the same as the previous key press. Raise the appropriate event
		if (m_abCurrentKeys[i] != m_abPrevKeys[i])
		{
			//If the current key state is true, the key was pressed, otherwise it was released
			if (m_abCurrentKeys[i] == true)
			{
				__raise OnKeyPressed(i);
			}
			else
			{
				__raise OnKeyReleased(i);
			}
		}
	}

	memcpy(m_abPrevKeys, m_abCurrentKeys, sizeof(m_abCurrentKeys));

	//Poll for events
	glfwPollEvents();
}

void InputManager::OnScrollEvent(GLFWwindow* window, double xOffset, double yOffset)
{
	__raise (Application::Instance()->GetInputManager()->OnScroll(xOffset, yOffset));
}

bool InputManager::IsKeyDown(unsigned int a_uiKey)
{
	return m_abCurrentKeys[a_uiKey];
}