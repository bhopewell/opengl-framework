#ifndef __OBJECT_INL__
#define __OBJECT_INL__

#include "glm/gtc/matrix_transform.hpp"

inline void Object::SetPosition(glm::vec3& newPosition)
{
	m_v3LocalPosition = newPosition;
}

inline void Object::SetScale(glm::vec3& newScale)
{
	m_v3Scale = newScale;
}

inline void Object::SetRotation(glm::vec3& newRotationEuler)
{
	m_v3Rotation = newRotationEuler;
}

inline const glm::vec3& Object::GetLocalPosition() const
{
	return m_v3LocalPosition;
}

inline const glm::vec4& Object::GetPosition() const
{
	return m_m4Transform[3];
}

inline const glm::vec3& Object::GetScale() const
{
	return m_v3Scale;
}

inline const glm::vec3& Object::GetRotationEuler() const
{
	return m_v3Rotation;
}

inline const glm::mat4& Object::GetTransform() const
{
	return m_m4Transform;
}

inline const Object* Object::GetParent() const
{
	return m_poParent;
}

inline void Object::SetParent(Object* parent)
{
	if (parent == this)
		return;

	m_poParent = parent;
}

inline void Object::AddChild(Object* child)
{
	//Add the child to the children list and set the child's parent to this
	m_lChildren.push_back(child);
	child->SetParent(this);
}

inline void Object::RemoveChild(Object* child)
{
	std::vector<Object*> tempChildren;

	for (auto it = m_lChildren.begin(); it != m_lChildren.end(); ++it)
	{
		if (*(it._Ptr) != child)
		{
			tempChildren.push_back(*(it._Ptr));
		}
	}

	m_lChildren = tempChildren;
	//Remove the childs parent
	child->SetParent(nullptr);
}

inline const std::vector<Object*>& Object::GetChildren() const
{
	return m_lChildren;
}

#endif __OBJECT_INL__