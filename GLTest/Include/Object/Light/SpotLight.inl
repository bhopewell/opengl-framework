#ifndef __SPOTLIGHT_INL__
#define __SPOTLIGHT_INL__

inline void SpotLight::SetOn(bool isOn)
{
	m_bIsOn = isOn;
}

inline void SpotLight::SetPower(float power)
{
	m_fPower = power;
}

inline void SpotLight::SetCutoff(float cutoff)
{
	m_fCutoff = cutoff;
}

inline void SpotLight::SetAttenuation(Attenuation& attenuation)
{
	m_oAttenuation = attenuation;
}

inline void SpotLight::SetColor(glm::vec3& color)
{
	m_v3Color = color;
}

inline void SpotLight::SetPosition(glm::vec3& position)
{
	//Set the position with the w value as 1.0f as this is not a directional light
	m_v3Position = glm::vec4(position, 1.0f);
}

inline void SpotLight::SetDirection(glm::vec3& direction)
{
	m_v3Direction = direction;
}

#endif __SPOTLIGHT_INL__