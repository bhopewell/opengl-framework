#ifndef __ILIGHT_INL__
#define __ILIGHT_INL__

inline const bool& ILight::IsOn() const
{
	return m_bIsOn;
}

inline const float& ILight::GetExponent() const
{
	return m_fExponent;
}

inline const float& ILight::GetCutoff() const
{
	return m_fCutoff;
}

inline const float& ILight::GetRange() const
{
	return m_fRange;
}

inline const glm::vec3& ILight::GetColor() const
{
	return m_v3Color;
}

inline const glm::vec3& ILight::GetDirection() const
{
	return glm::normalize(m_v3Direction);
}

inline const glm::vec4& ILight::GetPosition() const
{
	return m_v4Position;
}

inline const Attenuation& ILight::GetAttenuation() const
{
	return m_oAttenuation;
}

inline void ILight::SetOn(bool isOn)
{
	m_bIsOn = isOn;
}

inline void ILight::SetExponent(float exponent)
{
	m_fExponent = exponent;
}

inline void ILight::SetCutoff(float cutoff)
{
	if (cutoff > 180.0f)
	{
		cutoff = 180.0f;
	}
	else if (cutoff <= 0.0f)
	{
		cutoff = 1.0f;
	}

	m_fCutoff = cutoff;
}

inline void ILight::SetRange(float range)
{
	m_fRange = range;
}

inline void ILight::SetAttenuation(Attenuation& attenuation)
{
	m_oAttenuation = attenuation;
}

inline void ILight::SetColor(glm::vec3& color)
{
	m_v3Color = color;
}

inline void ILight::SetPosition(glm::vec3& position)
{
	//Set the position with the w value as 1.0f as this is not a directional light
	m_v4Position = glm::vec4(position, 1.0f);
}

inline void ILight::SetDirection(glm::vec3& direction)
{
	m_v3Direction = direction;
}

#endif __ILIGHT_INL__