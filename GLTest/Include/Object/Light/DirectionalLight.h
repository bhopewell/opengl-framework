#ifndef __DIRECTIONALLIGHT_H__
#define __DIRECTIONALLIGHT_H__

#include "ILight.h"
#include <glm/glm.hpp>

class DirectionalLight : public ILight
{
public:
	DirectionalLight();
	DirectionalLight(glm::vec3& color);
	DirectionalLight(glm::vec3& color, glm::vec3& direction, bool isOn = true);

	~DirectionalLight();

	virtual void Update(float deltaTime) override;
};

#endif __DIRECTIONALLIGHT_H__