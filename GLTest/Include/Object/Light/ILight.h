#ifndef __ILIGHT_H__
#define __ILIGHT_H__

#include "../Object.h"
#include <glm/glm.hpp>

struct Attenuation
{
	Attenuation()
	{}

	Attenuation(float constant, float linear, float quadratic)
		: m_fConstant(constant), m_fLinear(linear), m_fQuadratic(quadratic)
	{}

	float m_fConstant;
	float m_fLinear;
	float m_fQuadratic;
};

class ILight : public Object
{
public:
	virtual ~ILight() = 0
	{
	};

	virtual void Update(float deltaTime) = 0;

	//Get methods
	virtual inline const bool& IsOn() const;
	virtual inline const float& GetExponent() const;
	virtual inline const float& GetCutoff() const;
	virtual inline const float& GetRange() const;
	virtual inline const Attenuation& GetAttenuation() const;
	virtual inline const glm::vec3& GetColor() const;
	virtual inline const glm::vec4& GetPosition() const;
	virtual inline const glm::vec3& GetDirection() const;

	//Set methods
	virtual inline void SetOn(bool isOn);
	virtual inline void SetExponent(float exponent);
	virtual inline void SetCutoff(float cutoff);
	virtual inline void SetRange(float range);
	virtual inline void SetAttenuation(Attenuation& attenuation);
	virtual inline void SetColor(glm::vec3& color);
	virtual inline void SetPosition(glm::vec3& position);
	virtual inline void SetDirection(glm::vec3& direction);

protected:
	bool m_bIsOn;

	float m_fCutoff;
	float m_fRange;
	
	Attenuation m_oAttenuation;

	float m_fExponent;
	glm::vec3 m_v3Color;

	glm::vec4 m_v4Position;
	glm::vec3 m_v3Direction;
};

#include "ILight.inl"

#endif __ILIGHT_H__