#ifndef __SPOTLIGHT_H__
#define __SPOTLIGHT_H__

#include "ILight.h"

class SpotLight : public ILight
{
public:
	SpotLight();
	SpotLight(float exponent);
	SpotLight(float exponent, float cutoffDegrees);
	SpotLight(float exponent, float cutoffDegrees, float range);
	SpotLight(float exponent, float cutoffDegrees, float range, glm::vec3& color);
	SpotLight(float exponent, float cutoffDegrees, float range, glm::vec3& color, glm::vec3& position);
	SpotLight(float exponent, float cutoffDegrees, float range, glm::vec3& color, glm::vec3& position, glm::vec3& direction);
	SpotLight(float exponent, float cutoffDegrees, float range, glm::vec3& color, glm::vec3& position, glm::vec3& direction, Attenuation& attenuation, bool isOn = true);
	
	~SpotLight();

	virtual void Update(float deltaTime) override;
};

#endif __SPOTLIGHT_H__