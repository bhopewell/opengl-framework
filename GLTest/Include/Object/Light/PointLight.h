#ifndef __POINTLIGHT_H__
#define __POINTLIGHT_H__

#include "ILight.h"

class PointLight : public ILight
{
public:
	PointLight();
	PointLight(float exponent);
	PointLight(float exponent, float range);
	PointLight(float exponent, float range, glm::vec3& color);
	PointLight(float exponent, float range, glm::vec3& color, glm::vec3& position);
	PointLight(float exponent, float range, glm::vec3& color, glm::vec3& position, Attenuation& attenuation, bool isOn = true);

	~PointLight();

	virtual void Update(float deltaTime) override;
};
#endif __POINTLIGHT_H__