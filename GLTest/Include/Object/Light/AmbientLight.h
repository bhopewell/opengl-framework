#ifndef __AMBIENTLIGHT_H__
#define __AMBIENTLIGHT_H__

#include "ILight.h"
#include <glm/glm.hpp>

class AmbientLight : public ILight
{
public:
	AmbientLight();
	AmbientLight(glm::vec3& color);

	~AmbientLight();

	virtual void Update(float deltaTime) override;
};

#endif __AMBIENTLIGHT_H__