#ifndef __DRAWABLEOBJECT3D_H__
#define __DRAWABLEOBJECT3D_H__

#include "../Object.h"
#include <gl/glew.h>

class Mesh;
enum ETechnique;
class ITechnique;

class DrawableObject3D : public Object
{
public:
	DrawableObject3D();
	DrawableObject3D(const char* meshFilePath);
	virtual ~DrawableObject3D() override;

	void SetTextures(GLint diffuseTexID, GLint bumpTexID = -1, GLint specTexID = -1);
	void SetTextures(const char* diffuseTexturePath, const char* bumpMapPath = nullptr, const char* specMapPath = nullptr);

	bool AttachShader(ETechnique shaderTechnique);

	virtual void Update(float deltaTime) override;

	virtual void Render();

	inline void SetCanRotate(bool canRotate)
	{
		m_bRotate = canRotate;
	}

protected:
	void BindTextures();
	void UnbindTextures();

protected:

	bool m_bRotate = true;

	Mesh* m_poMesh;

	ITechnique* m_poShaderTechnique;

	GLint m_iDiffuseTextureID;
	GLint m_iNormalTextureID;
	GLint m_iSpecTextureID;

	GLint m_iDiffuseTextureAttribID;
	GLint m_iNormalTextureAttribID;
	GLint m_iSpecTextureAttribID;
};

#endif __DRAWABLEOBJECT3D_H__