#ifndef __DRAWABLEOBJECT2D_H__
#define __DRAWABLEOBJECT2D_H__

#include "../Object.h"

#include <GL/glew.h>
#include <vector>

class Quad;

enum ETechnique;
class ITechnique;

class DrawableObject2D : public Object
{
public:
	DrawableObject2D();
	DrawableObject2D(const glm::vec3& position);
	DrawableObject2D(const glm::vec3& position, const glm::vec3& scale);
	DrawableObject2D(const glm::vec3& position, const glm::vec3& scale, const glm::vec3& rotationRadians);
	virtual ~DrawableObject2D() override;

	void SetTextures(GLint diffuseTexID, GLint bumpTexID = -1, GLint specTexID = -1);
	void SetTextures(const char* diffuseTexturePath, const char* bumpMapPath = nullptr, const char* specMapPath = nullptr);

	bool AttachShader(ETechnique shaderTechnique);

	inline void SetCanOrbit(bool canOrbit)
	{
		m_bOrbit = canOrbit;
	}

	virtual void Update(float deltaTime) override;

	virtual void Render();

protected:
	virtual void BindTextures();
	virtual void UnbindTextures();

protected:
	bool m_bOrbit;

	Quad* m_poQuad;

	ITechnique* m_poShaderTechnique;

	GLint m_iDiffuseTextureID;
	GLint m_iNormalTextureID;
	GLint m_iSpecTextureID;

	GLint m_iDiffuseTextureAttribID;
	GLint m_iNormalTextureAttribID;
	GLint m_iSpecTextureAttribID;
};

#endif __DRAWABLEOBJECT2D_H__