#ifndef __ICAMERA_H__
#define __ICAMERA_H__

#define GLM_FORCE_RADIANS

#include "glm/glm.hpp"
#include "../Object.h"

class ICamera : public Object
{
public:
	ICamera();
	virtual ~ICamera() = 0;

	virtual void Init();
	virtual void Init(float fov, float aspectRatio, float nearPlane, float farPlane);

	inline const float& GetNearPlane() const;
	inline const float& GetFarPlane() const;
	inline const float& GetAspect() const;
	inline const float& GetFOV() const;

	inline const glm::mat4& GetProjectionMatrix() const;
	inline const glm::mat4& GetViewMatrix() const;

	inline void SetProjection(float fov, float aspectRatio, float zNear, float zFar);

protected:
	float m_fFOV;
	float m_fAspectRatio;
	float m_fNearPlane;
	float m_fFarPlane;

	glm::mat4 m_m4View;
	glm::mat4 m_m4Projection;
};

#include "ICamera.inl"

#endif __ICAMERA_H__