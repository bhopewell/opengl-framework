#ifndef __ICAMERA_INL__
#define __ICAMERA_INL__

#include "glm/gtc/matrix_transform.hpp"

inline const glm::mat4& ICamera::GetProjectionMatrix() const
{
	return m_m4Projection;
}

inline const glm::mat4& ICamera::GetViewMatrix() const
{
	return m_m4View;
}

inline void ICamera::SetProjection(float fov, float aspectRatio, float zNear, float zFar)
{
	m_m4Projection = glm::perspective(fov, aspectRatio, zNear, zFar);
}

inline const float& ICamera::GetNearPlane() const
{
	return m_fNearPlane;
}

inline const float& ICamera::GetFarPlane() const
{
	return m_fFarPlane;
}

inline const float& ICamera::GetAspect() const
{
	return m_fAspectRatio;
}

inline const float& ICamera::GetFOV() const
{
	return m_fFOV;
}

#endif __ICAMERA_INL__