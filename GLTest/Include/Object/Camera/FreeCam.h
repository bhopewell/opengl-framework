#ifndef __FREECAM_H__
#define __FREECAM_H__

#include "ICamera.h"

class FreeCam : public ICamera
{
public:
	FreeCam();
	virtual ~FreeCam();

	void Update(float deltaTime) override;

private:
	float m_fSpeedMod = 1.0f;
	const float m_fSpeed = 5.0f;
	const float m_fMouseSpeed = 0.05f;

	float m_fHorizontalAngle = 3.14f;
	float m_fVerticalAngle = 0.0f;
};

#endif __FREECAM_H__