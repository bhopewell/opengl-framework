#ifndef __OBJECT_H__
#define __OBJECT_H__

#include <glm/glm.hpp>
#include <vector>

#include "Debug.h"

class Object
{
public:
	Object();
	Object(const glm::vec3& position);
	Object(const glm::vec3& position, const glm::vec3& scale);
	Object(const glm::vec3& position, const glm::vec3& scale, const glm::vec3& rotationRadians);
	virtual ~Object() = 0;

	virtual void Update(float deltaTime);

	inline void SetPosition(glm::vec3& newPosition);
	inline void SetLocalPosition(glm::vec3& newPosition);
	inline void SetScale(glm::vec3& newScale);
	inline void SetRotation(glm::vec3& newRotationEuler);

	inline const glm::vec4& GetPosition() const;
	inline const glm::vec3& GetLocalPosition() const;
	inline const glm::vec3& GetScale() const;
	inline const glm::vec3& GetRotationEuler() const;

	inline const glm::mat4& GetTransform() const;

	inline const Object* GetParent() const;

	inline void AddChild(Object* child);
	inline void RemoveChild(Object* child);
	inline const std::vector<Object*>& GetChildren() const;

private:
	inline void SetParent(Object* parent);

protected:

	//Parent object
	Object* m_poParent;
	std::vector<Object*> m_lChildren;

	glm::vec3 m_v3LocalPosition;
	glm::vec3 m_v3Scale;
	glm::vec3 m_v3Rotation;

	glm::mat4 m_m4Transform;
};

#include "Object.inl"

#endif __OBJECT_H__