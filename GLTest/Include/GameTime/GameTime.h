#ifndef __GAMETIME_H__
#define __GAMETIME_H__

class GameTime
{
public:
	static inline double DeltaTime();

	static inline double TotalElapsedTime();

	static void Destroy();

	static void Initialize();

	static void Update();

protected:
	static inline GameTime* Instance();
	static GameTime* m_poInstance;

	GameTime();
	~GameTime();

	double m_fCurrentTime;
	double m_fPrevTime;
	double m_fDeltaTime;

	double m_fTotalElapsedTime;
};

#include "GameTime.inl"

#endif __GAMETIME_H__