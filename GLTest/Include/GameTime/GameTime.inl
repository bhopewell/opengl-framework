#ifndef __GAMETIME_INL__
#define __GAMETIME_INL__

inline double GameTime::DeltaTime()
{
	return Instance()->m_fDeltaTime;
}

inline double GameTime::TotalElapsedTime()
{
	return Instance()->m_fTotalElapsedTime;
}

inline GameTime* GameTime::Instance()
{
	if (m_poInstance == nullptr)
	{
		m_poInstance = new GameTime();
	}
	return m_poInstance;
}

#endif __GAMETIME_INL__