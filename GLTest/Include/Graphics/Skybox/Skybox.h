#ifndef __SKYBOX_H__
#define __SKYBOX_H__

#include <GL\glew.h>

class Mesh;
class ITechnique;

class Skybox
{
public:
	Skybox(const char* forwardTexture, const char* backTexture, const char* leftTexture,
			const char* rightTexture, const char* bottomTexture, const char* topTexture);
	~Skybox();

	bool AttachShader();

	void Render();

protected:
	void LoadTextures(const char* forwardTexture, const char* backTexture, const char* leftTexture,
						const char* rightTexture, const char* bottomTexture, const char* topTexture);

	void BindTexture();
	void UnbindTexture();

private:
	GLuint m_uiCubeMapID;

	ITechnique* m_poShaderTechnique;

	Mesh* m_poCubeMesh;
};

#endif __SKYBOX_H__