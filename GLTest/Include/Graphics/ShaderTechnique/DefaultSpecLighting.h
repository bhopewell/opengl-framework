#ifndef __DEFAULTSPECLIGHTING_H__
#define __DEFAULTSPECLIGHTING_H__

#include "ITechnique.h"

class DefaultSpecLighting : public ITechnique
{
public:
	DefaultSpecLighting();
	~DefaultSpecLighting();

	virtual bool Initialize() override;
};

#endif __DEFAULTSPECLIGHTING_H__