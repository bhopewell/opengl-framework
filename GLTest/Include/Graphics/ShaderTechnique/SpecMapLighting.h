#ifndef __SPECMAPLIGHTING_H__
#define __SPECMAPLIGHTING_H__

#include "ITechnique.h"

class SpecMapLighting : public ITechnique
{
public:
	SpecMapLighting();
	~SpecMapLighting();

	virtual bool Initialize() override;
};

#endif __SPECMAPLIGHTING_H__