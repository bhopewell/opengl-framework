#ifndef __DEFAULTLIGHTING_H__
#define __DEFAULTLIGHTING_H__

#include "ITechnique.h"

class DefaultLighting : public ITechnique
{
public:
	DefaultLighting();
	~DefaultLighting();

	virtual bool Initialize() override;
};

#endif __DEFAULTLIGHTING_H__