#ifndef __SPECBUMPLIGHTING_H__
#define __SPECBUMPLIGHTING_H__

#include "ITechnique.h"

class SpecBumpLighting : public ITechnique
{
public:
	SpecBumpLighting();
	~SpecBumpLighting();

	virtual bool Initialize() override;
};

#endif __SPECBUMPLIGHTING_H__