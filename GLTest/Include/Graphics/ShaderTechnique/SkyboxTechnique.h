#ifndef __SKYBOXTECHNIQUE_H__
#define __SKYBOXTECHNIQUE_H__

#include "ITechnique.h"

class SkyboxTechnique : public ITechnique
{
public:
	SkyboxTechnique();
	~SkyboxTechnique();

	virtual bool Initialize() override;
};

#endif __SKYBOXTECHNIQUE_H__