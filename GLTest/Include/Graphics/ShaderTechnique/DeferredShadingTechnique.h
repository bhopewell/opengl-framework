#ifndef __DEFERREDSHADINGTECHNIQUE_H__
#define __DEFERREDSHADINGTECHNIQUE_H__

#include "ITechnique.h"

class DeferredShadingTechnique : public ITechnique
{
public:
	DeferredShadingTechnique();
	~DeferredShadingTechnique();

	virtual bool Initialize() override;
};

#endif //__DEFERREDSHADINGTECHNIQUE_H__