#ifndef __UNLITTECHNIQUE_H__
#define __UNLITTECHNIQUE_H__

#include "ITechnique.h"

class UnlitTechnique : public ITechnique
{
public:
	UnlitTechnique();
	~UnlitTechnique();

	virtual bool Initialize() override;
};

#endif __UNLITTECHNIQUE_H__