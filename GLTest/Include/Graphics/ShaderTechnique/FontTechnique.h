#ifndef __FONTTECHNIQUE_H__
#define __FONTTECHNIQUE_H__

#include "ITechnique.h"

class FontTechnique : public ITechnique
{
public:
	FontTechnique();
	~FontTechnique();

	virtual bool Initialize() override;
};

#endif __FONTTECHNIQUE_H__