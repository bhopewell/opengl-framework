#ifndef __PREPROCESSDEFERREDTECHNIQUE_H__
#define __PREPROCESSDEFERREDTECHNIQUE_H__

#include "ITechnique.h"

class PreprocessDeferredTechnique : public ITechnique
{
public:
	PreprocessDeferredTechnique();
	~PreprocessDeferredTechnique();

	virtual bool Initialize() override;
};

#endif __PREPROCESSDEFERREDTECHNIQUE_H__