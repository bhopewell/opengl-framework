#ifndef __ITECHNIQUE_H__
#define __ITECHNIQUE_H__

#include <list>
#include <GL/glew.h>

class ITechnique
{
public:
	ITechnique();
	~ITechnique();

	virtual bool Initialize();

	//Start using the technique
	void Activate();
	//Stop using the technique
	void Deactivate();

	//Get the uniform location
	GLint GetUniformLocation(const char* uniformName);

	//Get the attrib location
	GLint GetAttribLocation(const char* attribName);

protected:
	
	//Add a shader to the technique
	bool AddShader(const char* filepath, GLenum shaderType);

	//Prepare the program for use
	bool Finalize();

	GLuint m_uiProgramID;

private:
	std::list<GLuint> m_lShaderList;
};

#endif __ITECHNIQUE_H__