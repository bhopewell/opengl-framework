#ifndef __ETECHNIQUE_H__
#define __ETECHNIQUE_H__

enum ETechnique
{
	EUnlit,

	EDefaultLighting,
	EDefaultSpecLighting,

	ESpecMapLighting,
	ESpecBumpLighting,

	ESkybox,
	EFont,

	EPreprocess,
	EDeferred,
};

#endif __ETECHNIQUE_H__