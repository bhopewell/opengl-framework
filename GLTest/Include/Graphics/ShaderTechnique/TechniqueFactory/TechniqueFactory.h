#ifndef __TECHNIQUEFACTORY_H__
#define __TECHNIQUEFACTORY_H__

enum ETechnique;
class ITechnique;

class TechniqueFactory
{
public:
	static ITechnique* CreateTechnique(ETechnique eTechnique);
};

#endif __TECHNIQUEFACTORY_H__