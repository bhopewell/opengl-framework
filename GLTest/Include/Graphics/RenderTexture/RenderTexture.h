#ifndef __RENDERTEXTURE_H__
#define __RENDERTEXTURE_H__

#include <GL/glew.h>
#include "Object/Object.h"

class RenderTexture : public Object
{
public:
	RenderTexture(bool useDepthBuffer = true);
	~RenderTexture();

	virtual void Update(float deltaTime);

	inline GLint GetTexture()
	{
		return m_uiColorTextureID;
	}

	inline GLint GetDepthTexture()
	{
		return m_uiDepthTextureID;
	}

	void StartRender();
	void EndRender();

protected:
	void CreateFrameBuffer();
	void CreateTexture(GLuint& textureID, bool isDepthTex = false);

private:
	bool m_bUseDepthBuffer;

	GLuint m_uiFrameBufferID;

	GLuint m_uiColorTextureID;

	GLuint m_uiDepthTextureID;
};

#endif __RENDERTEXTURE_H__