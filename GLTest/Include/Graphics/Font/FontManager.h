#ifndef __FONTMANAGER_H__
#define __FONTMANAGER_H__

#include <GL/glew.h>
#include "glm/glm.hpp"

#include "Debug.h"

class ITechnique;

class FontManager
{
public:
	static inline FontManager* Instance()
	{
		if (m_poInstance == NULL)
		{
			m_poInstance = new FontManager();
		}

		return m_poInstance;
	}

	void Destroy();

	void InitFontManager(const char* texturePath);
	void RenderText(const char* text, glm::vec2& pos, float size);

protected:
	FontManager();
	~FontManager();

	static FontManager* m_poInstance;

private:
	GLuint m_uiText2DTextureID;
	GLuint m_uiText2DVertexBufferID;
	GLuint m_uiText2DUVBufferID;

	GLuint m_uiTextUniformID;
	GLuint m_uiScreenSizeUniformID;
	GLuint m_uiVertAttribID;
	GLuint m_uiUVAttribID;

	ITechnique* m_poShaderTechnique;
};

#endif __FONTMANAGER_H__