#ifndef __MESH_H__
#define __MESH_H__

#include "glm/gtc/matrix_transform.hpp"
#include <GL/glew.h>
#include "GLFW/glfw3.h"

#include <vector>

class ITechnique;

class Mesh
{
public:
	Mesh(const char* filepath);
	~Mesh();
	
	virtual void Update(float deltaTime);

	virtual void Render();

	inline void AttachShader(ITechnique* technique);

	inline void AddVert(glm::vec3 newVert);
	inline void AddUV(glm::vec2 newUV);
	inline void AddNormal(glm::vec3 newNorm);
	inline void AddIndices(glm::vec3 newIndices);

protected:

	virtual void LoadMesh(const char* filepath);

	virtual void CreateVertexData();
	virtual void CreateIndexData();
	virtual void CreateTextureData();
	virtual void CreateNormalData();

	void CalculateTangentData();
	glm::vec3 ValidateTangent(glm::vec3 tangent, glm::vec3 bitangent, glm::vec3 normal);

private:

	GLuint m_uiVertexAttribID;
	GLuint m_uiUVAttribID;
	GLuint m_uiNormalAttribID;
	GLuint m_uiTangentAttribID;
	GLuint m_uiBiTangentAttribID;

	GLuint m_uiVertBufferID;
	GLuint m_uiIndexBufferID;
	GLuint m_uiUVBufferID;
	GLuint m_uiNormalBufferID;
	GLuint m_uiTangentBufferID;
	GLuint m_uiBiTangentBufferID;

	std::vector<glm::vec3> m_v3Verts;
	std::vector<glm::vec2> m_v2UVs;
	std::vector<glm::vec3> m_v3Normals;
	std::vector<glm::vec3> m_v3Tangents;
	std::vector<glm::vec3> m_v3BiTangents;

	std::vector<GLuint> m_auiIndexBuffer;

	GLuint m_uiVertCount;
	GLuint m_uiIndexCount;
	GLuint m_uiUVCount;
	GLuint m_uiNormalCount;
	GLuint m_uiTangentCount;
	GLuint m_uiBiTangentCount;
};

#include "Mesh.inl"

#endif __MESH_H__