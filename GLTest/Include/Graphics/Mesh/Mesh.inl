#ifndef __MESH_INL__
#define __MESH_INL__

#include "Graphics/ShaderTechnique/ITechnique.h"

inline void Mesh::AddVert(glm::vec3 newVert)
{
	m_v3Verts.push_back(newVert);
}

inline void Mesh::AddUV(glm::vec2 newUV)
{
	m_v2UVs.push_back(newUV);
}

inline void Mesh::AddNormal(glm::vec3 newNorm)
{
	m_v3Normals.push_back(newNorm);
}

inline void Mesh::AddIndices(glm::vec3 newIndices)
{
	m_auiIndexBuffer.push_back((unsigned int)newIndices.x);
	m_auiIndexBuffer.push_back((unsigned int)newIndices.y);
	m_auiIndexBuffer.push_back((unsigned int)newIndices.z);
}

void Mesh::AttachShader(ITechnique* shaderTechnique)
{
	//Get the location of the vertex attribute according to the vert shader
	m_uiVertexAttribID = shaderTechnique->GetAttribLocation("vertPosIn");
	m_uiUVAttribID = shaderTechnique->GetAttribLocation("vertUVIn");
	m_uiNormalAttribID = shaderTechnique->GetAttribLocation("normalIn");
	m_uiTangentAttribID = shaderTechnique->GetAttribLocation("tangentIn");
	m_uiBiTangentAttribID = shaderTechnique->GetAttribLocation("bitangentIn");
}

#endif __MESH_INL__