#ifndef __BILLBOARDSPRITE_H__
#define __BILLBOARDSPRITE_H__

#include "Quad.h"

enum BillboardType
{
	Cylindrical,
	Spherical
};

class BillboardSprite : public Quad
{
public:
	BillboardSprite(BillboardType billboardType);
	~BillboardSprite();

	virtual void Update(float deltaTime) override;

private:
	BillboardType m_eBillboardType;
};

#endif __BILLBOARDSPRITE_H__