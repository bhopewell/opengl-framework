#ifndef __SPRITE_H__
#define __SPRITE_H__

#include "glm/gtc/matrix_transform.hpp"
#include <GL/glew.h>
#include "GLFW/glfw3.h"

#include <vector>

#include "../Mesh/Mesh.h"

class ITechnique;

class Quad : public Mesh
{
public:
	Quad();
	~Quad();

	virtual void Update(float deltaTime);
};

#endif __SPRITE_H__
