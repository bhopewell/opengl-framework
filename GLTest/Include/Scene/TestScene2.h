#ifndef __TESTSCENE2_H__
#define __TESTSCENE2_H__

#include "IScene.h"

#include "Debug.h"

class DrawableObject3D;
class DrawableObject2D;
class RenderTexture;
class Skybox;

class TestScene2 : public IScene
{
public:
	TestScene2();
	~TestScene2();

	virtual void Update(float deltaTime) override;

	virtual void Render() override;

	virtual void OnKeyPressed(int key) override;
	virtual void OnKeyReleased(int key) override;

protected:
	std::vector<DrawableObject3D*> m_poDrawableObjects;
	DrawableObject3D* m_poPlane;

	Skybox* m_poSkybox;

	bool m_bUpdateLights;
};

#endif __TESTSCENE2_H__