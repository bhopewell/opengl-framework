#ifndef __ISCENE_H__
#define __ISCENE_H__

#include <string>

#include "Debug.h"

#include "Object/Camera/ICamera.h"
#include "AssetManagement/Light/LightManager.h"

class IScene
{
public:
	IScene();
	virtual ~IScene() = 0;

	virtual void Update(float deltaTime) = 0;

	virtual void Render() = 0;
	virtual void RenderGUI() = 0;

	virtual void OnKeyPressed(int key) = 0;
	virtual void OnKeyReleased(int key) = 0;

	inline ICamera* GetCamera() const;
	inline LightManager* GetLightManager() const;

	inline const std::string& GetName() const;

protected:
	ICamera* m_poCamera;
	LightManager* m_poLightManager;

	std::string m_szSceneName;
};

#include "IScene.inl"

#endif __ISCENE_H__