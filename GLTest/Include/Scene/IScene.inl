#ifndef __ISCENE_INL__
#define __ISCENE_INL__

inline ICamera* IScene::GetCamera() const
{
	return m_poCamera;
}

inline LightManager* IScene::GetLightManager() const
{
	return m_poLightManager;
}

inline const std::string& IScene::GetName() const
{
	return m_szSceneName;
}

#endif __ISCENE_INL__