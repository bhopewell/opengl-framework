#ifndef __TESTSCENE_H__
#define __TESTSCENE_H__

#include "IScene.h"

#include "Debug.h"

class DrawableObject3D;
class DrawableObject2D;
class RenderTexture;
class Skybox;

class TestScene : public IScene
{
public:
	TestScene();
	~TestScene();

	virtual void Update(float deltaTime) override;

	virtual void Render() override;
	virtual void RenderGUI() override;

	virtual void OnKeyPressed(int key) override;
	virtual void OnKeyReleased(int key) override;	

protected:
	DrawableObject3D* m_poPlane;
	DrawableObject3D* m_poCube;
	DrawableObject3D* m_poCube2;

	DrawableObject2D* m_poSprite;

	ILight* m_poPointLight;

	Skybox* m_poSkybox;

	bool m_bUpdateLights;
};

#endif __TESTSCENE_H__