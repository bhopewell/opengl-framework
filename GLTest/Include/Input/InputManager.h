//////////////////////////////////////////////////////////////////////
//	Author: Benjamin J Hopewell
//	Date: 24/08/2013
//	File: InputManager.h
//	Brief: InputManager Class. Handles any input received from the user
//////////////////////////////////////////////////////////////////////

#ifndef __INPUT_MANAGER_H__
#define __INPUT_MANAGER_H__

//MAX_KEYS is 512 because GLFW defines custom int values for its keys. Eg. Escape = 256 instead of 27.
#define MAX_KEYS 512

//Forward declare the GLFWwindow struct
struct GLFWwindow;

[event_source(native)]
class InputManager
{
public:
	InputManager();
	~InputManager();

	__event void OnKeyPressed(int key);
	__event void OnKeyReleased(int key);
	__event void OnScroll(double xOffset, double yOffset);

	void Init();

	void Update();

	bool IsKeyDown(unsigned int a_uiKey);

protected:
	static void OnScrollEvent(GLFWwindow* window, double xOffset, double yOffset);

private:
	bool m_abCurrentKeys[MAX_KEYS];
	bool m_abPrevKeys[MAX_KEYS];
};

#endif __INPUT_MANAGER_H__