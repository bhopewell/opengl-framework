#ifndef __MESHLOADER_H__
#define __MESHLOADER_H__

#include "glm/glm.hpp"
#include "GLFW/glfw3.h"
#include <vector>

class Mesh;

class MeshLoader
{
public:
	static bool LoadMeshAssimp(const char* filepath, std::vector<GLuint>& indices, std::vector<glm::vec3>& vertices,
														std::vector<glm::vec2>& uvs, std::vector<glm::vec3>& normals);

	static bool LoadMeshAssimp(const char* filepath, Mesh** mesh);
};

#endif __MESHLOADER_H__