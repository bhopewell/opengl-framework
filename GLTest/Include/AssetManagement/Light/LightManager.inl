#ifndef __LIGHTMANAGER_INL__
#define __LIGHTMANAGER_INL__

inline ILight* LightManager::GetAmbientLight() const
{
	return m_poAmbientLight;
}

inline const std::vector<ILight*>& LightManager::GetLights() const
{
	return m_lLights;
}

#endif __LIGHTMANAGER_INL__