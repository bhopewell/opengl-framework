#ifndef __LIGHTMANAGER_H__
#define __LIGHTMANAGER_H__

#include <vector>

class ILight;
class ITechnique;

class LightManager
{
public:
	LightManager();
	~LightManager();

	inline ILight* GetAmbientLight() const;
	inline const std::vector<ILight*>& GetLights() const;

	void AddLight(ILight* newLight);

	void Update(float deltaTime);

	void RenderLights(ITechnique* shaderTechnique);

private:
	ILight* m_poAmbientLight;

	std::vector<ILight*> m_lLights;
};

#include "LightManager.inl"

#endif __LIGHTMANAGER_H__