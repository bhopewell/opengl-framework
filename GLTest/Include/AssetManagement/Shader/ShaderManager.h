#ifndef __SHADERMANAGER_H__
#define __SHADERMANAGER_H__

#include <GL/glew.h>
#include <string>
#include <map>

#include "Debug.h"

class ShaderManager
{
public:
	static inline ShaderManager* Instance()
	{
		if (m_poInstance == nullptr)
			m_poInstance = new ShaderManager();

		return m_poInstance;
	}

	GLuint LoadShader(const char* filepath, GLenum shaderType);

	void Destroy();

private:
	static ShaderManager* m_poInstance;

	ShaderManager();
	~ShaderManager();

	void PrintShaderLog(GLuint shader, const char* filepath);

	std::string m_sHashString;

	std::map<unsigned long, GLuint> m_oProgramMap;

	std::map<const char*, GLuint> m_oShaderMap;
};

#endif __SHADERMANAGER_H__