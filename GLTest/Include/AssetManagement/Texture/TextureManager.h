#ifndef __TEXTUREMANAGER_H__
#define __TEXTUREMANAGER_H__

#include <map>
#include <GL/glew.h>

#include <glm/glm.hpp>

#include "Debug.h"

class TextureManager
{
public:
	static inline TextureManager* Instance()
	{
		if (m_poInstance == nullptr)
		{
			m_poInstance = new TextureManager();
		}

		return m_poInstance;
	}

	inline GLuint DefaultTexture()
	{
		return m_uiDefaultTexID;
	}

	void Destroy();

	void Initialize();

	GLint LoadTexture(const char* filepath);
	GLint LoadTextureCubeMap(const char* forwardTexture, const char* backTexture, const char* leftTexture,
								const char* rightTexture, const char* bottomTexture, const char* topTexture);

	//bool SaveTextureSOIL(GLint textureID, const char* filename);

protected:
	static TextureManager* m_poInstance;

private:
	TextureManager();
	~TextureManager();

	std::map<const char*, GLuint> m_oTextureMap;

	GLuint m_uiDefaultTexID;
};

#endif __TEXTUREMANAGER_H__