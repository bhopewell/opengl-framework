#ifndef __SCENEMANAGER_INL__
#define __SCENEMANAGER_INL__

inline const IScene* SceneManager::ActiveScene() const
{
	if (m_stSceneStack.empty())
	{
		return nullptr;
	}

	return m_stSceneStack.top();
}

#endif __SCENEMANAGER_INL__