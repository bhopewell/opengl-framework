#ifndef __SCENEMANAGER_H__
#define __SCENEMANAGER_H__

#include <stack>
#include "Debug.h"

#include "Scene/IScene.h"

class SceneManager
{
public:
	SceneManager();
	~SceneManager();

	void Init();

	inline const IScene* ActiveScene() const;

	void Update(float deltaTime);

	void Render();
	void RenderGUI();

	void PushScene(IScene* newScene);
	void ReplaceCurrentScene(IScene* newScene);
	void PopScene();

protected:
	void OnKeyPressed(int key);
	void OnKeyReleased(int key);

private:
	std::stack<IScene*> m_stSceneStack;
};

#include "SceneManager.inl"

#endif __SCENEMANAGER_H__