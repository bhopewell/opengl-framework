#ifndef __APPLICATION_H__
#define __APPLICATION_H__

#include "glm/glm.hpp"

#include "Renderer\IRenderer.h"
#include "AssetManagement/Scene/SceneManager.h"

struct GLFWwindow;

class ICamera;
class InputManager;
class SceneManager;

class Application
{
public:
	~Application();

	bool Init();
	void FrameBufferResize(int newX, int newY);

	void Run();

	void Destroy();

	void Quit();

	static inline Application* Instance();
	inline GLFWwindow* GetWindow() const;
	inline ICamera* GetActiveCamera() const;
	inline InputManager* GetInputManager() const;
	inline SceneManager* GetSceneManager() const;

	inline const glm::vec2& GetScreenSize() const;

	inline const bool IsDeferred() const;
	inline void SetIsDeferred(bool isDeferred);

protected:
	static Application* m_poInstance;
	Application();

	void Render();

private:
	bool m_bIsRunning;
	bool m_bIsDeferred;

	//Renderer
	IRenderer* m_poRenderer;

	//Input Manager
	InputManager* m_poInputManager;

	//Scene Manager
	SceneManager* m_poSceneManager;
};

#include "Application.inl"

#endif __APPLICATION_H__