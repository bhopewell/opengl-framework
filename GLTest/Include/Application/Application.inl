#ifndef __APPLICATION_INL__
#define __APPLICATION_INL__

#include "Debug.h"

inline Application* Application::Instance()
{
	if (m_poInstance == NULL)
		m_poInstance = new Application();

	return m_poInstance;
}

inline GLFWwindow* Application::GetWindow() const
{
	return m_poRenderer->GetWindow();
}

inline ICamera* Application::GetActiveCamera() const
{
	return m_poSceneManager->ActiveScene()->GetCamera();
}

inline InputManager* Application::GetInputManager() const
{
	return m_poInputManager;
}

inline SceneManager* Application::GetSceneManager() const
{
	return m_poSceneManager;
}

inline const glm::vec2& Application::GetScreenSize() const
{
	return m_poRenderer->GetScreenSize();
}

inline const bool Application::IsDeferred() const
{
	return m_bIsDeferred;
}

inline void Application::SetIsDeferred(bool isDeferred)
{
	m_bIsDeferred = isDeferred;
}

#endif