#ifndef __LOGGER_H__
#define __LOGGER_H__

#include <cstdio>
#include <string>

enum LogLevel
{
	VERBOSE,
	STANDARD,
	FORCE,
};

class Logger
{
public:
	static void Init();
	static void Close();

	static void Log(const char* log, LogLevel logLevel = VERBOSE);
	static void Log(std::string log, LogLevel logLevel = VERBOSE);
	static void LogWarning(const char* log, LogLevel logLevel = VERBOSE);
	static void LogWarning(std::string log, LogLevel logLevel = VERBOSE);
	static void LogError(const char* log);
	static void LogError(std::string log);

protected:
#ifdef _DEBUG
	static const LogLevel m_eLogLevel = VERBOSE;
#else
	static const LogLevel m_eLogLevel = STANDARD;
#endif

	static FILE* m_pfLogFile;
};

#endif __LOGGER_H__