//If the forward renderer is being used, throw and error because this should not be used when forward rendering
#ifdef __FORWARDRENDERER_H__
#error Cannot use DeferredQuad when rendering with the Forward Renderer
#endif __FORWARDRENDERER_H__

#ifndef __DEFERREDQUAD_H__
#define __DEFERREDQUAD_H__

#include <GL/glew.h>
#include <glm/glm.hpp>

#include "Debug.h"

class ITechnique;

class DeferredQuad
{
public:
	DeferredQuad(glm::vec2& screenSize);
	~DeferredQuad();

	void SetTextures(GLint diffuse, GLint position = -1, GLint normal = -1, GLint spec = -1, GLint tangent = -1);

	void Render();

protected:
	void InitBuffers(glm::vec2& screenSize);
	void CreateVerts(glm::vec2& screenSize);
	void CreateUVs();

	void InitShaders();

	void BindTextures();
	void UnbindTextures();

private:

	//Texture buffers IDs
	GLint m_iDiffuseBuffer;
	GLint m_iDepthBuffer;
	GLint m_iNormalBuffer;
	GLint m_iSpecBuffer;
	GLint m_iTangentBuffer;

	//Texture uniform IDs
	GLint m_iDiffuseUniform;
	GLint m_iDepthUniform;
	GLint m_iNormalUniform;
	GLint m_iSpecUniform;
	GLint m_iTangentUniform;

	GLuint m_uiVertCount;

	//Vert and UV buffer IDs
	GLuint m_uiVertexBufferID;
	GLuint m_uiUVBufferID;

	//Vert and UV buffer attribute IDs
	GLuint m_uiVertAttribID;
	GLuint m_uiUVAttribID;

	ITechnique* m_poShaderTechnique;
};

#endif __DEFERREDQUAD_H__