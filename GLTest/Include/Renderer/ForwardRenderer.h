#ifndef __FORWARDRENDERER_H__
#define __FORWARDRENDERER_H__

#include "IRenderer.h"

class ForwardRenderer : public IRenderer
{
public:
	ForwardRenderer();
	virtual ~ForwardRenderer() override;

	virtual bool Initialize(glm::vec2& screenSize, int openGLVersionMajor, int openGLVersionMinor, int antiAliasLevel) override;

	virtual void FrameBufferResize(int newX, int newY) override;

	virtual void StartRender() override;
	virtual void StartRenderGUI() override;
	virtual void EndRender() override;

protected:
};

#endif __FORWARDRENDERER_H__