#ifndef __DEFERREDRENDERER_H__
#define __DEFERREDRENDERER_H__

#include "IRenderer.h"

#include <GL/glew.h>

class DeferredQuad;
class ITechnique;

class DeferredRenderer : public IRenderer
{
public:
	DeferredRenderer();

	virtual ~DeferredRenderer() override;

	virtual bool Initialize(glm::vec2& screenSize, int openGLVersionMajor, int openGLVersionMinor, int antiAliasLevel) override;

	virtual void FrameBufferResize(int newX, int newY) override;

	virtual void StartRender() override;
	virtual void StartRenderGUI() override;
	virtual void EndRender() override;

protected:

	virtual bool InitializeOpenGL();
	virtual bool InitializeFrameBuffer();
	virtual void CreateGBufferTextures();
	virtual void CreateBuffer(GLuint& textureID, bool isDepthTex = false);

	//Frame buffer ID
	GLuint m_uiFrameBuffer;

	//Screen Quad
	DeferredQuad* m_poScreenQuad;

	//Buffer IDs
	GLuint m_uiPositionBuffer;
	GLuint m_uiColorBuffer;
	GLuint m_uiNormalBuffer;
	GLuint m_uiSpecBuffer;
	GLuint m_uiUVBuffer;

	GLuint m_uiDepthBuffer;
};

#endif __DEFERREDRENDERER_H__