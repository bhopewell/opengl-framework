#ifndef __IRENDERER_H__
#define __IRENDERER_H__

#include <glm\glm.hpp>

struct GLFWwindow;

class IRenderer
{
public:
	virtual ~IRenderer() 
	{
		//Empty destructor for correct clean-up
	}

	virtual bool Initialize(glm::vec2& screenSize, int openGLVersionMajor, int openGLVersionMinor, int antiAliasLevel) = 0;

	virtual void FrameBufferResize(int newX, int newY) = 0;

	virtual void StartRender() = 0;
	virtual void StartRenderGUI() = 0;
	virtual void EndRender() = 0;

	inline GLFWwindow* GetWindow() const;
	inline const glm::vec2& GetScreenSize() const;
	inline void SetScreenSize(glm::vec2 newScreenSize);

protected:

	GLFWwindow* m_poWindow;

	glm::vec2 m_v2ScreenSize;

	int m_iOpenGLVersionMajor;
	int m_iOpenGLVersionMinor;

	int m_iAntiAliasLevel;
};

#include "IRenderer.inl"

#endif __IRENDERER_H__