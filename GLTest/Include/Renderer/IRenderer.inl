#ifndef __IRENDERER_INL__
#define __IRENDERER_INL__

inline GLFWwindow* IRenderer::GetWindow() const
{
	return m_poWindow;
};

inline const glm::vec2& IRenderer::GetScreenSize() const
{
	return m_v2ScreenSize;
}

inline void IRenderer::SetScreenSize(glm::vec2 newScreenSize)
{
	m_v2ScreenSize = newScreenSize;
}

#endif __IRENDERER_INL__