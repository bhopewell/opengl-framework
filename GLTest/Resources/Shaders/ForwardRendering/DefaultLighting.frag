#version 420 core

const int MAXLIGHTS = 16;

in vec2 UV;
in vec3 VertPos;	//Vert pos in world space
in vec3 Normal;		//Normal in world space

uniform mat4 V;

struct Light
{
	float Exponent;
	float Cutoff;
	float Range;
	float ConstantAttenuation;
	float LinearAttenuation;
	float QuadraticAttenuation;

	vec4 Position;
	vec3 Direction;
	vec3 Color;
};
uniform Light lights[MAXLIGHTS];

struct AmbientLight
{
	vec3 AmbientColor;
};
uniform AmbientLight ambientLight;

uniform sampler2D diffuseTexture;
 
layout (location = 0) out vec4 DiffuseOut;
layout (location = 1) out vec4 WorldPosOut;
layout (location = 2) out vec4 NormalOut;

//Calculate the diffuse intensity
vec3 CalculateDiffuseIntensity(int i, float attenuation, vec3 normalDir, vec3 lightDirection)
{
	return attenuation * lights[i].Color * max(0.0, dot(normalDir, lightDirection));
}

void main() 
{
	vec4 MaterialDiffuseColor = texture(diffuseTexture, vec2(UV.x, 1.0 - UV.y)).rgba;
	vec3 MaterialAmbientColor = ambientLight.AmbientColor * vec3(MaterialDiffuseColor);

	vec3 finalDiffuse = vec3(0,0,0);

	//Calculate the View Direction using the inverse ViewMat
	vec3 viewDir = normalize(vec3(inverse(V) * vec4(0,0,0,1) - vec4(VertPos, 1)));
	//Calculate the Normal direction
	vec3 normalDir = normalize(Normal);
	vec3 lightDirection;
	float attenuation;

	for(int i = 0; i < MAXLIGHTS; ++i)
	{
		//Directional light
		if(lights[i].Position.w == 0.0f)
		{
			attenuation = 1.0f; //No attenuation
			lightDirection = normalize(vec3(lights[i].Position));
		}
		else
		{
			vec3 positionToLightSource = vec3(lights[i].Position) - VertPos;
			float distance = length(positionToLightSource);
			lightDirection = normalize(positionToLightSource);
			
			//Don't calculate this light for this fragment as it's too far away
			if(distance > lights[i].Range)
			{
				continue;
			}

			//Calculate the attenuation value
			attenuation = 1.0f / (lights[i].ConstantAttenuation + (lights[i].LinearAttenuation * distance) + (lights[i].QuadraticAttenuation * distance * distance));

			//Spotlight
			if(lights[i].Cutoff <= 90.0f)
			{
				float clampedCos = max(0.0, dot(-lightDirection, normalize(lights[i].Direction)));
				//Fragment outside of spot cone?
				if(clampedCos < cos(radians(lights[i].Cutoff)))
				{
					attenuation = 0.0f;
				}
				else
				{
					attenuation = attenuation * pow(clampedCos, lights[i].Exponent);
				}
			}
		}

		finalDiffuse += vec3(MaterialDiffuseColor) * CalculateDiffuseIntensity(i, attenuation, normalDir, lightDirection);
	}

	vec3 finalColor = MaterialAmbientColor + finalDiffuse;

	DiffuseOut = vec4(finalColor, MaterialDiffuseColor.a);
	WorldPosOut = vec4(VertPos, 1);
	NormalOut = vec4(normalize(normalDir), 1);
}