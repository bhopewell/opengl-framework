#version 420 core

const int MAXLIGHTS = 16;

in vec2 UV;
in vec3 VertPos;
in mat3 TBN;

struct Light
{
	float Exponent;
	float Cutoff;
	float Range;
	float ConstantAttenuation;
	float LinearAttenuation;
	float QuadraticAttenuation;

	vec4 Position;
	vec3 Direction;
	vec3 Color;
};
uniform Light lights[MAXLIGHTS];

struct AmbientLight
{
	vec3 AmbientColor;
};
uniform AmbientLight ambientLight;

uniform sampler2D diffuseTexture;
uniform sampler2D normalTexture;
uniform sampler2D specTexture;

uniform mat4 V;
 
layout (location = 0) out vec4 DiffuseOut;
layout (location = 1) out vec4 WorldPosOut;
layout (location = 2) out vec4 NormalOut;

//Calculate the diffuse intensity
vec3 CalculateDiffuseIntensity(int i, float attenuation, vec3 normalDir, vec3 lightDirection)
{
	return attenuation * lights[i].Color * max(0.0, dot(normalDir, lightDirection));
}

//Calculate the specularity intensity
vec3 CalculateSpecIntensity(int i, float attenuation, vec3 normalDir, vec3 lightDirection, vec3 viewDir)
{
	vec3 specReflection = vec3(0,0,0);
	//Check if the light source is on the correct side. If so, apply spec
	if(dot(normalDir, lightDirection) > 0.0f)
	{
		float specIntensity = pow(max(0.0, dot(reflect(-lightDirection, normalDir), viewDir)), 10);
		specReflection = attenuation * lights[i].Color * specIntensity;
	}
	return specReflection;
}

void main() 
{
	//Local Normal, tangent space.
	vec3 encodedNormals = normalize(texture(normalTexture, vec2(UV.x, UV.y)).rgb * 2.0 - 1.0);

	//Material properties
	vec4 MaterialDiffuseColor = texture(diffuseTexture, vec2(UV.x, 1.0 - UV.y)).rgba;
	vec3 MaterialAmbientColor = ambientLight.AmbientColor * vec3(MaterialDiffuseColor);
	vec3 MaterialSpecColor = texture(specTexture, vec2(UV.x, 1.0 - UV.y)).rgb * 0.1; //0.3 could be moved to a 'shininess' uniform

	vec3 finalDiffuse = vec3(0,0,0);
	vec3 finalSpec = vec3(0,0,0);
	
	vec3 viewDir = normalize(vec3(inverse(V) * vec4(0,0,0,1) - vec4(VertPos, 1)));
	vec3 normalDir = normalize(encodedNormals);
	vec3 lightDirection = vec3(0,0,0);
	float attenuation = 1.0f;

	for(int i = 0; i < MAXLIGHTS; ++i)
	{
		//Directional light
		if(lights[i].Position.w == 0.0f)
		{
			attenuation = 1.0f; //No attenuation
			lightDirection = normalize(TBN * vec3(V * lights[i].Position));
		}
		else
		{
			//Calculate the light direction
			vec3 positionToLightSource = vec3(lights[i].Position) - VertPos;
			//Calculate the distance to the light
			float distance = length(positionToLightSource);
			//Normalize the light direction
			lightDirection = normalize(positionToLightSource);

			//Don't calculate this light for this fragment as it's too far away
			if(distance > lights[i].Range)
			{
				continue;
			}

			//Calculate the attenuation value
			attenuation = 1.0f / (lights[i].ConstantAttenuation + (lights[i].LinearAttenuation * distance) + (lights[i].QuadraticAttenuation * distance * distance));

			//Spotlight
			if(lights[i].Cutoff <= 90.0f)
			{
				float clampedCos = max(0.0, dot(-lightDirection, normalize(lights[i].Direction)));
				//Fragment outside of spot cone?
				if(clampedCos < cos(radians(lights[i].Cutoff)))
				{
					attenuation = 0.0f;
				}
				else
				{
					attenuation = attenuation * pow(clampedCos, lights[i].Exponent);
				}
			}
			else
			{
				//Transform the light direction into tangent space
				lightDirection = normalize(TBN * lightDirection);
			}
		}

		finalDiffuse += vec3(MaterialDiffuseColor) * CalculateDiffuseIntensity(i, attenuation, normalDir, lightDirection);
		finalSpec += MaterialSpecColor * CalculateSpecIntensity(i, attenuation, normalDir, lightDirection, viewDir);
	}

	vec3 finalColor = MaterialAmbientColor + finalDiffuse + finalSpec;
	
	DiffuseOut = vec4(finalColor, MaterialDiffuseColor.a);
	WorldPosOut = vec4(VertPos, 1);
	NormalOut = vec4(normalize(normalDir), 1);
}