#version 420 core

in vec3 vertPosIn;
in vec2 vertUVIn;
in vec3 normalIn;

out vec2 UV;
out vec3 VertPos;
out vec3 Normal;

uniform mat4 M;		//Model matrix
uniform mat4 V;		//View matrix
uniform mat4 P;		//Projection matrix

void main()
{
	mat4 MVP = P * V * M;
	// Output position of the vertex, in clip space : MVP * position
    gl_Position =  MVP * vec4(vertPosIn, 1);

	// Position of the vertex, in worldspace : M * position
	VertPos = (M * vec4(vertPosIn,1)).xyz;

	// Normal of the the vertex, in world space
	Normal = vec4(transpose(inverse(mat3(M))) * normalIn,0).xyz;

	//UV of the vertex.
	UV = vertUVIn;
}