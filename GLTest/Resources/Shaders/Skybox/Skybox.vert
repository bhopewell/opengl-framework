#version 420 core

uniform vec3 CameraPosition;

uniform mat4 V;		//View matrix
uniform mat4 P;		//Projection matrix

in vec3 vertPosIn;

// Output data will be interpolated for each fragment.
out vec3 UV;

void main()
{
	mat4 VP = P * V;

	gl_Position = VP * vec4(vertPosIn + CameraPosition, 1);

	// UV of the vertex. No special space for this one.
	UV = vertPosIn;
}