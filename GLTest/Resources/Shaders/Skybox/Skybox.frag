#version 420 core

uniform samplerCube diffuseTexture;

// Interpolated values from the vertex shaders
in vec3 UV;

// Ouput data
layout (location = 0) out vec4 DiffuseOut;

void main()
{
	DiffuseOut = texture( diffuseTexture, UV );
}