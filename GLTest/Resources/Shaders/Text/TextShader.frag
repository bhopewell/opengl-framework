#version 420 core

// Interpolated values from the vertex shaders
in vec2 UV;

// Ouput data
layout (location = 0) out vec4 color;

// Values that stay constant for the whole mesh.
uniform sampler2D diffuseTex;

void main()
{
	color = texture( diffuseTex, UV ).rgba;
}