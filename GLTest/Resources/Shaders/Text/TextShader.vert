#version 420 core

// Input vertex data, different for all executions of this shader.
in vec2 vertPosIn;
in vec2 vertUVIn;

uniform vec2 screenSize;

// Output data ; will be interpolated for each fragment.
out vec2 UV;

void main()
{
	// Output position of the vertex, in clip space
	// map [0..800][0..600] to [-1..1][-1..1]
	vec2 vertexPosition_homoneneousspace = vertPosIn - (screenSize*0.5);
	vertexPosition_homoneneousspace /= (screenSize*0.5);
	gl_Position = vec4(vertexPosition_homoneneousspace,0,1);
	
	// UV of the vertex. No special space for this one.
	UV = vertUVIn;
}