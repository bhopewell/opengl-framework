#version 420 core

in vec2 UV;
in vec3 VertPos;	//Vert pos in world space
in vec3 Normal;		//Normal in world space

layout (location = 0) out vec4 DiffuseOut;
layout (location = 1) out vec4 WorldPosOut;
layout (location = 2) out vec4 NormalOut;

uniform sampler2D diffuseTexture;
 
void main() 
{
	//output color is the color of the texure at the appropriate UV
	DiffuseOut = texture( diffuseTexture, UV ).rgba;
	WorldPosOut = vec4(VertPos, 1);
	NormalOut = vec4(normalize(Normal), 1);
}