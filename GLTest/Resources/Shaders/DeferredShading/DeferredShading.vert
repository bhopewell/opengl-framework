#version 420 core

in vec3 vertPosIn;
in vec2 vertUVIn;

out vec2 UV;

void main()
{
	// Output position of the vertex, in clip space
    gl_Position = vec4(vertPosIn, 1);

	//UV of the vertex.
	UV = vertUVIn;
}