#version 420 core

const int MAXLIGHTS = 16;

in vec2 UV;

layout (location = 0) out vec4 DiffuseOut;

//Input textures.
uniform sampler2D diffuseTexture;
uniform sampler2D depthTexture;
uniform sampler2D normalTexture;
uniform sampler2D specTexture;

//Camera position
uniform vec3 CameraPosition;
//Near and far planes
uniform float zNearPlane;
uniform float zFarPlane;

//View matrix
uniform mat4 V;

struct Light
{
	float Exponent;
	float Cutoff;
	float Range;
	float ConstantAttenuation;
	float LinearAttenuation;
	float QuadraticAttenuation;

	vec4 Position;
	vec3 Direction;
	vec3 Color;
};
uniform Light lights[MAXLIGHTS];

//Ambient light
struct AmbientLight
{
	vec3 AmbientColor;
};
uniform AmbientLight ambientLight;

//Calculate the diffuse intensity
vec3 CalculateDiffuseIntensity(int i, float attenuation, vec3 normalDir, vec3 lightDirection)
{
	return attenuation * lights[i].Color * max(0.0, dot(normalDir, lightDirection));
}

//Calculate the specularity intensity
vec3 CalculateSpecIntensity(int i, float attenuation, vec3 normalDir, vec3 lightDirection, vec3 viewDir)
{
	vec3 specReflection = vec3(0,0,0);
	//Check if the light source is on the correct side. If so, apply spec
	if(dot(normalDir, lightDirection) > 0.0f)
	{
		float specIntensity = pow(max(0.0, dot(reflect(-lightDirection, normalDir), viewDir)), 10);
		specReflection = attenuation * lights[i].Color * specIntensity;
	}
	return specReflection;
}

float GetLinearizedDepth()
{
	return 1 - (2 * zNearPlane) / (zFarPlane  + zNearPlane - texture( depthTexture, vec2(UV.x, 1 - UV.y)).x * (zFarPlane - zNearPlane));
}

void main() 
{
	vec3 diff = texture( diffuseTexture, vec2(UV.x, 1 - UV.y)).rgb;
	float depth = texture( depthTexture, vec2(UV.x, 1 - UV.y)).r;
	vec3 norm = texture( normalTexture, vec2(UV.x, 1 - UV.y)).rgb;
	vec3 spec = texture( specTexture, vec2(UV.x, 1 - UV.y)).rgb;
	
	//Calculate the final ambient color
	vec3 MaterialAmbientColor = ambientLight.AmbientColor * vec3(diff);
	
	vec3 finalDiffuse = vec3(0,0,0);
	vec3 finalSpec = vec3(0,0,0);

	vec3 viewDir = normalize(CameraPosition - depth);
	vec3 normalDir = normalize(norm);
	vec3 lightDirection = vec3(0,0,0);
	float attenuation = 1.0f;

	for(int i = 0; i < MAXLIGHTS; ++i)
	{
		//Directional light
		if(lights[i].Position.w == 0.0f)
		{
			attenuation = 1.0f; //No attenuation
			lightDirection = normalize(vec3(lights[i].Position));
		}
		else
		{
			//Calculate the light direction
			vec3 positionToLightSource = vec3(lights[i].Position) - depth;
			//Calculate the distance to the light
			float distance = length(positionToLightSource);
			//Normalize the light direction
			lightDirection = vec3(normalize(V * vec4(positionToLightSource,1)));

			//Don't calculate this light for this fragment as it's too far away
			if(distance > lights[i].Range)
			{
				continue;
			}

			//Calculate the attenuation value
			attenuation = 1.0f / (lights[i].ConstantAttenuation + (lights[i].LinearAttenuation * distance) + (lights[i].QuadraticAttenuation * distance * distance));

			//Spotlight
			if(lights[i].Cutoff <= 90.0f)
			{
				float clampedCos = max(0.0, dot(-lightDirection, normalize(lights[i].Direction)));
				//Fragment outside of spot cone?
				if(clampedCos < cos(radians(lights[i].Cutoff)))
				{
					attenuation = 0.0f;
				}
				else
				{
					attenuation = attenuation * pow(clampedCos, lights[i].Exponent);
				}
			}
			else
			{
				//Transform the light direction into tangent space
				lightDirection = normalize(lightDirection);
			}
		}

		finalDiffuse += vec3(diff) * CalculateDiffuseIntensity(i, attenuation, normalDir, lightDirection);
		finalSpec += spec * CalculateSpecIntensity(i, attenuation, normalDir, lightDirection, viewDir);
	}

	//Set the final color and output it from the shader
	vec3 finalColor = MaterialAmbientColor + finalDiffuse + finalSpec;
	DiffuseOut = vec4(finalColor, 1);
}