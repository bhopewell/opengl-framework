#version 420 core

in vec3 VertPos;
in vec2 UV;
in vec3 Normal;
in mat3 TBN;

layout (location = 0) out vec4 DiffuseOut;
layout (location = 1) out vec4 WorldPosOut;
layout (location = 2) out vec4 NormalOut;
layout (location = 3) out vec4 SpecOut;
layout (location = 4) out vec4 UVOut;

uniform sampler2D diffuseTexture;
uniform sampler2D normalTexture;
uniform sampler2D specTexture;

uniform bool NormalTexIsValid;
uniform bool SpecTexIsValid;

vec4 GetNormal()
{
	if(NormalTexIsValid)
	{
		vec3 encodedNormals = normalize(texture(normalTexture, vec2(UV.x, UV.y)).rgb * 2.0 - 1.0);
		return vec4(normalize(transpose(TBN) * encodedNormals), 1); //Return the normals in cam space
	}

	return vec4(normalize(Normal), 1);
}

vec4 GetSpec(float shininess)
{
	if(SpecTexIsValid)
	{
		vec3 specColor = texture(specTexture, vec2(UV.x, 1.0 - UV.y)).rgb * shininess;
		return vec4(specColor, 1);
	}

	return vec4(0.1f, 0.1f, 0.1f, 1);
}

void main() 
{
	float shininess = 0.1f;

	//Set the world out to the vert pos
	WorldPosOut = vec4(VertPos, 1);
	
	//Calculate the normal
	NormalOut = GetNormal();

	//Calculate the spec
	SpecOut = GetSpec(shininess);
	
	//DiffuseOut is the color of the texure at the appropriate UV
	DiffuseOut = vec4(texture(diffuseTexture, vec2(UV.x, 1.0 - UV.y)).rgb, 1);

	//Set the UV out to the UV coords
	UVOut = vec4(UV.x, UV.y, 1, 1);
}