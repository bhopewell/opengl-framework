#version 420 core

in vec3 vertPosIn;
in vec2 vertUVIn;
in vec3 normalIn;
in vec3 tangentIn;
in vec3 bitangentIn;

out vec2 UV;
out vec3 VertPos;
out vec3 Normal;
out mat3 TBN;

uniform mat4 M;		//Model matrix
uniform mat4 V;		//View matrix
uniform mat4 P;		//Projection matrix

void main()
{
	mat4 MVP = P * V * M;
	mat3 MV3x3 = mat3(V * M);

	// Output position of the vertex, in clip space : MVP * position
    gl_Position =  MVP * vec4(vertPosIn, 1);
	
	// Position of the vertex, in worldspace : M * position
	VertPos = (M * vec4(vertPosIn,1)).xyz;

	//UV of the vertex.
	UV = vertUVIn;

	// Normal of the vertex, in world space
	Normal = vec4(transpose(inverse(MV3x3)) * normalIn,0).xyz;

	//Model to camera = modelview
	vec3 vertTangent_camSpace = MV3x3 * tangentIn;
	vec3 vertBitangent_camSpace = MV3x3 * bitangentIn;
	vec3 vertNormal_camSpace = MV3x3 * normalIn;

	TBN = transpose(mat3(
						vertTangent_camSpace,
						vertBitangent_camSpace,
						vertNormal_camSpace));
}